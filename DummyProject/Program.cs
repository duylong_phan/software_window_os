﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using LongWpfUI.SystemHelper;
using LongModel.Helpers;
using MicroControllerApp.Models.DataClasses.Settings.Others;
using System.Xml.Linq;
using MicroControllerApp.Models.DataClasses.Settings;
using Microsoft.Win32;

namespace DummyProject
{
    public enum NetPositions
    {
        Client,
        Server
    }
    
    class Program
    {
        public const string Res_Ok = "Ok";
        public const string Res_NotAvailable = "NotAvailable";
        public const string Res_Invalid = "Invalid";

        private static SerialPort port;
        private static int index;

        static void Main(string[] args)
        {
            index = 0;
            
            try
            {
                /*
                port = new SerialPort("COM3", 115200);
                port.Open();
                port.DataReceived += Port_DataReceived;
                SendStart();
                while (true)
                {
                    var input = Console.ReadLine();
                    if (input == string.Empty)
                    {
                        break;
                    }

                    var type = (MotorCtrTypes)Enum.Parse(typeof(MotorCtrTypes), input);
                    switch (type)
                    {
                        case MotorCtrTypes.GetStatus:
                            MotorCtr_GetDeviceStatus();
                            break;

                        case MotorCtrTypes.StartTrans:
                            MotorCtr_StartTrans();
                            break;

                        case MotorCtrTypes.Action:
                            MotorCtr_SendAction();
                            break;

                        case MotorCtrTypes.EndTrans:
                            MotorCtr_EndTrans();
                            break;

                        case MotorCtrTypes.StartOper:
                            MotorCtr_StartOper();
                            break;

                        case MotorCtrTypes.StopOper:
                            MotorCtr_StopOper();
                            break;

                        case MotorCtrTypes.RealAction:
                            MotorCtr_RealAction();
                            break;

                        case MotorCtrTypes.ToggleSample:
                            MotorCtr_ToggleSample();
                            break;

                        case MotorCtrTypes.ReadPwm:
                            MotorCtr_ReadPwm();
                            break;

                        case MotorCtrTypes.WritePwm:
                            MotorCtr_WritePwm();
                            break;

                        default:
                            break;
                    }
                }

                SendStop();
                */

                var item = Registry.CurrentUser.OpenSubKey("Software").OpenSubKey("Classes").OpenSubKey("Harting/UsbBackup");

                Console.WriteLine("Done");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                Console.WriteLine(ex.StackTrace);
            }
            Console.ReadKey();
        }

        

        public const string WebAddress = "134.102.90.217";
        public const int WebPort = 12345;

        #region HW Protocol
        private static void SendStart()
        {
            var payLoad = new byte[] { 1, 0 };
            WritePayload(payLoad);
        }

        private static void SendStop()
        {
            var payLoad = new byte[] { 2, 0 };
            WritePayload(payLoad);
        }

        private static void MotorCtr_GetDeviceStatus()
        {
            //byte[] payLoad = null;
            //using (var stream = new MemoryStream())
            //using (var writer = new BinaryWriter(stream))
            //{
            //    writer.Write((byte)3);
            //    writer.Write((byte)2);
            //    writer.Write((byte)MicroTasks.MotorCtr);
            //    writer.Write(MotorCtrAction.GetStatus.GetBytes());
            //    payLoad = stream.ToArray();
            //}
            //WritePayload(payLoad);
        }

        private static void MotorCtr_StartTrans()
        {
            //byte[] payLoad = null;
            //using (var stream = new MemoryStream())
            //using (var writer = new BinaryWriter(stream))
            //{
            //    writer.Write((byte)3);
            //    writer.Write((byte)2);
            //    writer.Write((byte)MicroTasks.MotorCtr);
            //    writer.Write(MotorCtrAction.StartTrans.GetBytes());
            //    payLoad = stream.ToArray();
            //}
            //WritePayload(payLoad);
        }

        private static void MotorCtr_SendAction()
        {
            //var action1 = new MotorCtrMotorAction()
            //{
            //    Control1 = (byte)(MotorControl1Flags.Motor1 | MotorControl1Flags.Trigger | MotorControl1Flags.Counter_Clk),
            //    Control2 = 0,
            //    Amount = 10,
            //    DelayAmount = 0,
            //    TriggerAmount = 32000                
            //};

            //var action2 = new MotorCtrMotorAction()
            //{
            //    Control1 = (byte)(MotorControl1Flags.Motor2 | MotorControl1Flags.Trigger | MotorControl1Flags.Counter_Clk),
            //    Control2 = 0,
            //    Amount = 10,
            //    DelayAmount = 0,
            //    TriggerAmount = 32000
            //};

            //byte[] payLoad = null;
            //using (var stream = new MemoryStream())
            //using (var writer = new BinaryWriter(stream))
            //{
            //    writer.Write((byte)3);
            //    writer.Write((byte)0);
            //    writer.Write((byte)MicroTasks.MotorCtr);
            //    writer.Write((byte)MotorCtrTypes.Action);
            //    writer.Write(action1.GetBytes());
            //    writer.Write(action2.GetBytes());
            //    payLoad = stream.ToArray();
            //}
            //payLoad[1] = (byte)(payLoad.Length - 2);
            //WritePayload(payLoad);
        }

        private static void MotorCtr_EndTrans()
        {
            var action = new MotorCtrEndTransAction(2, 0,
                                                    2000);
            byte[] payLoad = null;
            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write((byte)3);
                writer.Write((byte)0);
                writer.Write((byte)MicroTasks.MotorCtr);
                writer.Write(action.GetBytes());
                payLoad = stream.ToArray();
            }
            payLoad[1] = (byte)(payLoad.Length - 2);
            WritePayload(payLoad);
        }

        private static void MotorCtr_StartOper()
        {
            //byte[] payLoad = null;
            //using (var stream = new MemoryStream())
            //using (var writer = new BinaryWriter(stream))
            //{
            //    writer.Write((byte)3);
            //    writer.Write((byte)2);
            //    writer.Write((byte)MicroTasks.MotorCtr);
            //    writer.Write(MotorCtrAction.StartOper.GetBytes());
            //    payLoad = stream.ToArray();
            //}
            //WritePayload(payLoad);
        }        

        private static void MotorCtr_StopOper()
        {
            //byte[] payLoad = null;
            //using (var stream = new MemoryStream())
            //using (var writer = new BinaryWriter(stream))
            //{
            //    writer.Write((byte)3);
            //    writer.Write((byte)2);
            //    writer.Write((byte)MicroTasks.MotorCtr);
            //    writer.Write(MotorCtrAction.StopOper.GetBytes());
            //    payLoad = stream.ToArray();
            //}
            //WritePayload(payLoad);
        }

        private static void MotorCtr_RealAction()
        {
            //var action1 = new MotorCtrMotorAction()
            //{
            //    Control1 = (byte)(MotorControl1Flags.Motor2 | MotorControl1Flags.Trigger | MotorControl1Flags.EnabledDelay),
            //    Control2 = 0,
            //    Amount = 10000,
            //    DelayAmount = 1,
            //    TriggerAmount = 1
            //};
            //byte[] payLoad = null;
            //using (var stream = new MemoryStream())
            //using (var writer = new BinaryWriter(stream))
            //{
            //    writer.Write((byte)3);
            //    writer.Write((byte)2);
            //    writer.Write((byte)MicroTasks.MotorCtr);
            //    writer.Write((byte)MotorCtrTypes.RealAction);
            //    writer.Write(action1.GetBytes());
            //    payLoad = stream.ToArray();
            //}
            //payLoad[1] = (byte)(payLoad.Length - 2);
            //WritePayload(payLoad);
        }

        

        private static void MotorCtr_ToggleSample()
        {
            //uint flag = (uint)(MotorCtrSampleTypes.Battery | MotorCtrSampleTypes.Pulse | MotorCtrSampleTypes.PwmFreq);
            //var toggle = new MotorCtrToggleSample(flag, 2);

            //byte[] payLoad = null;
            //using (var stream = new MemoryStream())
            //using (var writer = new BinaryWriter(stream))
            //{
            //    writer.Write((byte)3);
            //    writer.Write((byte)2);
            //    writer.Write((byte)MicroTasks.MotorCtr);
            //    writer.Write(toggle.GetBytes());
            //    payLoad = stream.ToArray();
            //}
            //payLoad[1] = (byte)(payLoad.Length - 2);
            //WritePayload(payLoad);
        }

        private static void MotorCtr_ReadPwm()
        {
            
        }

        private static void MotorCtr_WritePwm()
        {
            
        }

        private static void WritePayload(byte[] payload)
        {
            var header = new byte[] { (byte)'#', (byte)'#', (byte)'#', (byte)'#' };
            port.Write(header, 0, header.Length);
            port.Write(payload, 0, payload.Length);
        }

        private static void Port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            Task.Delay(5).Wait();
            var data = new byte[port.BytesToRead];
            port.Read(data, 0, data.Length);
            foreach (var item in data)
            {
                Console.Write(item.ToString("X2") + " ");
            }
            Console.WriteLine();
        }

        private static void Send_Ping()
        {
            var buffer = new byte[] {(byte)MicroTasks.RfTrans, (byte)RfTransTypes.Status, (byte)RfTransStatus.Ping };            
        }

        #endregion

    }

}
