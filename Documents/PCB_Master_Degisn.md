# The PCB is designed by using Circuitmaker Software, from Altium Limited. In order to use the PCB Project, there are some requirements: #

1. A user account from [Circuitmarker](http://circuitmaker.com)
1. Circuitmarker Software, which can be downloaded after the user account is created


# After installing the software, the user has to request the membership of the project, please following these steps #
## 1. Start the software, search for **Project** option, and put **PSOC_MASTER_THESIS** in the search box ##

![PCB_1.png](https://bitbucket.org/repo/5z4x5A/images/3678938130-PCB_1.png)

## 2. Select the project, after the website gives back the result ##

![PCB_2.png](https://bitbucket.org/repo/5z4x5A/images/697172005-PCB_2.png)

## 3. Select **Team** option, and select **Add New Team Request** ##

![PCB_3.png](https://bitbucket.org/repo/5z4x5A/images/3845511816-PCB_3.png)

## 4. Select **Edit** option, write something in the textbox, and click **Send** ##

![PCB_4.png](https://bitbucket.org/repo/5z4x5A/images/264863983-PCB_4.png)

## 5. After that, please send an email to me at [phanduylong@gmail.com](Link URL), I will add you in the member list as soon as possible ##
## 6. After you are in the member list, you can open design and edit the Project ##

![PCB_5.png](https://bitbucket.org/repo/5z4x5A/images/3144298295-PCB_5.png)

In order to use the Circuitmarker effectively, you can see tutorial video at [Youtube](https://www.youtube.com/watch?v=DYqIRv14d7M&list=PLxKPkAQqaBHcQ_cmCinzr7BwZc63znytE) or simply google with **Circuit maker tutorial**

# Support #
For more information, and support please contact [phanduylong@gmail.com](Link URL)