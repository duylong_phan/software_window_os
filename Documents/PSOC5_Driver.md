# Before the device is connected to computer's USB port, please make sure #
* the computer has the latest Windows's update
* the internet connection is available

## Connect PSOC5 to PC ##

![PSOC5_Driver_1.png](https://bitbucket.org/repo/5z4x5A/images/1469194191-PSOC5_Driver_1.png)

## Automatic installation PSOC5's driver ##

![PSOC5_Driver_2.png](https://bitbucket.org/repo/5z4x5A/images/1167799430-PSOC5_Driver_2.png)

## Open Device manager to check if driver is correctly installed ##

![DeviceManager.png](https://bitbucket.org/repo/5z4x5A/images/684765062-DeviceManager.png)

## Find Communication Port ##

![PSOC5_Driver_3.png](https://bitbucket.org/repo/5z4x5A/images/2687502171-PSOC5_Driver_3.png)

## Important ##
* The drive is only required to install, when the device is connected to Computer on the first time.


# Support #
For more information, and support please contact [phanduylong@gmail.com](Link URL)