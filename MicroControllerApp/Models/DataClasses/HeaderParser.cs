﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Models.DataClasses
{
    public class HeaderParser
    {
        #region Const
        public const char Indicator = '#';
        public const int IndicatorLength = 4;
        #endregion

        #region Getter
        public Queue<byte> Queue { get; private set; }
        #endregion

        #region Getter, setter
        public bool HasHeader { get; set; }
        public bool HasLength { get; set; }
        public byte Type { get; set; }
        public byte Length { get; set; }
        public int HeaderCount { get; set; }
        #endregion


        #region Constructor
        public HeaderParser()
        {
            this.Queue = new Queue<byte>();
            this.HasHeader = this.HasLength = false;
            this.Type = this.Length = 0;
            this.HeaderCount = 0;
        }
        #endregion
    }
}
