﻿using MicroControllerApp.Models.Base;
using MicroControllerApp.Models.DataClasses.Settings;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Models.DataClasses.Packages
{
    public class CapSamplePackage : DataPackage
    {
        #region Const
        public const int CapAmount = 4;
        public const int SampleIndex = 2;
        #endregion

        #region Getter
        public int[] Durations { get; private set; }
        public double[] Capacities { get; private set; }
        #endregion

        #region Constructor
        public CapSamplePackage(byte type, byte[] buffer)
            : base(type, buffer)
        {
            int index = SampleIndex;            
            this.Durations = new int[CapAmount];
            this.Capacities = new double[CapAmount];

            for (int i = 0; i < CapAmount; i++)
            {
                this.Durations[i] = BitConverter.ToInt32(buffer, index);
                index += 4;
            }
        }
        #endregion

        #region Public Method
        public override byte[] GetBytes()
        {
            byte[] data = null;

            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(this.Type);
                writer.Write((byte)0);
                for (int i = 0; i < CapAmount; i++)
                {
                    writer.Write(this.Durations[i]);
                }
                
                data = stream.ToArray();
                data[1] = (byte)data.Length;
            }

            return data;
        }
        #endregion
    }
}
