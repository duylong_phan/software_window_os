﻿using MicroControllerApp.Models.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Models.DataClasses.Packages
{
    public enum MotorCtrStatusTypes
    {
        Available = 1 << 0,
        TransDone = 1 << 1,
        TransError = 1 << 2,
        OperDone = 1 << 3,
        OperError = 1 << 4,
        HasActions = 1 << 5,
        NoActions = 1 << 6,
        OperWorking = 1 << 7,
        NoOperWorking = 1 << 8,
        SampleOn = 1 << 9,
        SampleOff = 1 << 10,
    }

    public class MotorCtrStatusPackage : DataPackage
    {
        #region Const
        public const int PcActionIndex = 2;
        public const int StatusIndex = 6;
        #endregion

        #region Getter
        public uint PcAction { get; private set; }
        public uint Status { get; private set; }
        #endregion

        #region Constructor
        public MotorCtrStatusPackage(byte type, byte[] buffer)
            : base(type, buffer)
        {
            this.PcAction = BitConverter.ToUInt32(buffer, PcActionIndex);
            this.Status = BitConverter.ToUInt32(buffer, StatusIndex);
        }
        #endregion
    }
}
