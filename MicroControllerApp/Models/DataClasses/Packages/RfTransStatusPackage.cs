﻿using MicroControllerApp.Models.Base;
using MicroControllerApp.Models.DataClasses.Settings;
using MicroControllerApp.Models.DataClasses.Settings.Others;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Models.DataClasses.Packages
{
    public class RfTransStatusPackage : DataPackage
    {
        #region Const
        public const int StatusIndex = 2;
        #endregion

        #region Getter
        public RfTransStatus Status { get; private set; }         
        #endregion

        #region Constructor
        public RfTransStatusPackage(byte type, byte[] buffer)
            : base(type, buffer)
        {
            try
            {
                this.Status = (RfTransStatus)buffer[StatusIndex];
            }
            catch (Exception)
            {
                this.Status = RfTransStatus.Ping;
            }
        }
        #endregion

        #region Public Method
        public override byte[] GetBytes()
        {
            byte[] data = null;
            using (var stream = new MemoryStream())
            using(var writer = new BinaryWriter(stream))
            {
                writer.Write(DataPackage.Header);
                writer.Write(this.Type);
                writer.Write((byte)1);
                writer.Write((byte)this.Status);
                data = stream.ToArray();
            }
            return data;
        }
        #endregion
    }
}
