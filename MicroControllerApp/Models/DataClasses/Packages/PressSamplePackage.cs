﻿using MicroControllerApp.Models.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using MicroControllerApp.Models.DataClasses.Settings;

namespace MicroControllerApp.Models.DataClasses.Packages
{
    public class PressSamplePackage : DataPackage
    {
        #region Const
        public const int ArrayLength = 5;
        public const int SampleIndex = 2;
        #endregion

        #region Getter
        public short[] MagD { get; private set; }
        public short[] Force { get; private set; }
        #endregion

        #region Constructor
        public PressSamplePackage(byte type, byte[] buffer)
            : base(type, buffer)
        {
            this.MagD = new short[ArrayLength];
            this.Force = new short[ArrayLength];
            int index = SampleIndex;
            for (int i = 0; i < ArrayLength; i++)
            {
                this.MagD[i] = BitConverter.ToInt16(buffer, index);
                index += 2;
                this.Force[i] = BitConverter.ToInt16(buffer, index);
                index += 2;
            }
        }
        #endregion

        #region Public Method
        public override byte[] GetBytes()
        { 
            byte[] data = null;
            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(this.Type);
                writer.Write((byte)0);
                for (int i = 0; i < ArrayLength; i++)
                {
                    writer.Write(this.MagD[i]);
                    writer.Write(this.Force[i]);
                }

                data = stream.ToArray();
                data[LengthIndex] = (byte)(data.Length - 2);
            }

            return data;
        }
        #endregion
    }
}
