﻿using MicroControllerApp.Models.Base;
using MicroControllerApp.Models.DataClasses.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Models.DataClasses.Packages
{
    public class RfPackage : RawPackage
    {
        #region Const
        public const int Index_TaskID = 0;
        public const int Index_TaskType = 1;
        public const int Index_Payload = 2;
        public const int Size_PayLoad = 30;
        #endregion

        #region Getter
        public byte TaskID { get; private set; }
        public byte TaskType { get; private set; }
        public byte[] PayLoad { get; private set; }
        #endregion

        #region Constructor
        public RfPackage(byte taskID, byte taskType)
            : this(taskID, taskType, new byte[Size_PayLoad])
        {

        }

        public RfPackage(byte taskID, byte taskType, byte[] payload)
            : base((byte)PcCommands.RF_Transmit)        
        {
            this.TaskID = taskID;
            this.TaskType = taskType;
            this.PayLoad = payload;
        }
        #endregion

        #region Public Method
        public override byte[] GetBytes()
        {
            Buffer = new byte[this.PayLoad.Length + 2];
            Buffer[Index_TaskID] = this.TaskID;
            Buffer[Index_TaskType] = this.TaskType;
            System.Buffer.BlockCopy(this.PayLoad, 0, Buffer, 2, this.PayLoad.Length);
            return base.GetBytes();
        }
        #endregion
    }
}
