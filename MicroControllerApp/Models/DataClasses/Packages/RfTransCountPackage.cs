﻿using MicroControllerApp.Models.Base;
using MicroControllerApp.Models.DataClasses.Settings;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Models.DataClasses.Packages
{
    public class RfTransCountPackage : DataPackage
    {
        #region Const
        public const int CountIndex = 2;
        #endregion

        #region Getter
        public ushort Count { get; private set; }
        #endregion

        #region Constructor
        public RfTransCountPackage(byte type, byte[] buffer)
            : base(type, buffer)
        {
            this.Count = BitConverter.ToUInt16(buffer, CountIndex);
        }
        #endregion

        #region Public Method
        public override byte[] GetBytes()
        {
            byte[] data = null;
            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(DataPackage.Header);
                writer.Write(this.Type);
                writer.Write((byte)2);
                writer.Write(this.Count);
                data = stream.ToArray();
            }

            return data;
        }
        #endregion
    }
}
