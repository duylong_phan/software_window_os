﻿using MicroControllerApp.Models.Base;
using MicroControllerApp.Models.DataClasses.Settings;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Models.DataClasses.Packages
{
    public class RawPackage : DataPackage
    {
        #region Read Package
        public static RawPackage Start
        {
            get
            {
                return new RawPackage((byte)PcCommands.Start);
            }
        }

        public static RawPackage Stop
        {
            get
            {
                return new RawPackage((byte)PcCommands.Stop);
            }
        }        

        public static RawPackage ReadMasterSetting
        {
            get
            {
                return new RawPackage((byte)PcCommands.Read_MasterSetting);
            }
        }
        #endregion
                
        #region Constructor
        public RawPackage(byte type)
            : this(type, null)
        {

        }

        public RawPackage(byte type, byte[] buffer)
            : base(type, buffer)
        {
            
        }
        #endregion

        #region Public Method
        public override byte[] GetBytes()
        {
            byte[] data = null;

            using (var stream = new MemoryStream())
            using(var writer = new BinaryWriter(stream))
            {
                writer.Write(DataPackage.Header);
                writer.Write(this.Type);
                if (this.Buffer != null)
                {
                    writer.Write((byte)Buffer.Length);
                    writer.Write(this.Buffer);
                }
                else
                {
                    writer.Write((byte)0);
                }

                data = stream.ToArray();
            }

            return data;
        }
        #endregion
    }
}
