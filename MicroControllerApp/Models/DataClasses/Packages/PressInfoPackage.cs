﻿using MicroControllerApp.Models.Base;
using MicroControllerApp.Models.DataClasses.Settings;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace MicroControllerApp.Models.DataClasses.Packages
{
    public class PressInfoPackage : DataPackage
    {
        #region Const
        public const string InfoTag = "PressInfo";
        public const string IdTag = "ID";
        public const string AmountTag = "Amount";
        public const int IdIndex = 2;
        public const int AmountINdex = 3;
        #endregion

        #region Getter
        public byte ID { get; set; }
        public byte Amount { get; set; }
        #endregion

        #region Constructor

        public PressInfoPackage(string text)
             : base((byte)PcCommands.Press_Pressed, new byte[0])
        {
            var element = XElement.Parse(text);
            this.ID = byte.Parse(element.Attribute(PressInfoPackage.IdTag).Value);
            this.Amount = byte.Parse(element.Attribute(PressInfoPackage.AmountTag).Value);
        }

        public PressInfoPackage(byte type,  byte[] buffer)
            : base(type, buffer)
        {
            this.ID = buffer[IdIndex];
            this.Amount = buffer[AmountINdex];
        }
        #endregion

        #region Public Method
        public override byte[] GetBytes()
        {
            byte[] data = null;

            using (var stream = new MemoryStream())
            using(var writer = new BinaryWriter(stream))
            {
                writer.Write((byte)this.Type);
                writer.Write((byte)2);
                writer.Write(this.ID);
                writer.Write(this.Amount);
                data = stream.ToArray();
            }

            return data;
        }

        public override string ToString()
        {
            var element = new XElement(PressInfoPackage.InfoTag);
            element.Add(new XAttribute(PressInfoPackage.IdTag, this.ID));
            element.Add(new XAttribute(PressInfoPackage.AmountTag, this.Amount));
            return element.ToString();
        }
        #endregion
    }
}
