﻿using MicroControllerApp.Models.Base;
using MicroControllerApp.Models.DataClasses.Settings;
using MicroControllerApp.Models.DataClasses.Settings.Others;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Models.DataClasses.Packages
{
    public class uBalanceStatusPackage : DataPackage
    {
        #region Const
        public const int TaskTypeIndex = 2;
        #endregion

        #region Getter
        public byte TaskType { get; private set; }
        public uBalanceTypes Status { get; private set; }
        #endregion

        #region Constructor
        public uBalanceStatusPackage(byte type, byte[] buffer)
            : base(type, buffer)
        {
            this.TaskType = buffer[TaskTypeIndex];
            try
            {
                this.Status = (uBalanceTypes)Enum.ToObject(typeof(uBalanceTypes), this.TaskType);
            }
            catch (Exception)
            {
                this.Status = uBalanceTypes.Ping;
            }            
        }
        #endregion

        #region Public Method
        public override byte[] GetBytes()
        {
            byte[] data = null;
            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(DataPackage.Header);
                writer.Write(this.Type);
                writer.Write((byte)1);
                writer.Write(this.TaskType);
                data = stream.ToArray();
            }
            return data;
        }
        #endregion
    }
}
