﻿using MicroControllerApp.Models.Base;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Models.DataClasses.Packages
{
    public abstract class SlaveSettingPackage : DataPackage
    {
        #region Getter
        public int Amount { get; private set; }
        public byte TaskID { get; private set; }
        public byte TaskType { get; private set; }
        #endregion

        #region Constructor
        public SlaveSettingPackage(byte type, List<SubSlaveSettingPackage> collection, byte taskID, byte taskType)
            : base(type, null)
        {
            this.Amount = collection.Count;
            this.TaskID = taskID;
            this.TaskType = taskType;
            this.Buffer = new byte[collection.Count * SubSlaveSettingPackage.SettingSize];

            for (int i = 0; i < collection.Count; i++)
            {
                System.Buffer.BlockCopy(collection[i].Buffer, SubSlaveSettingPackage.SettingIndex, 
                                        this.Buffer, i* SubSlaveSettingPackage.SettingSize, SubSlaveSettingPackage.SettingSize);                
            }            

            //Set Property from Buffer
            AssignProperties();
        }
        #endregion

        #region Abstract Method
        protected abstract void AssignProperties();
        protected abstract void AssignBuffer();
        #endregion

        #region Public Method
        public List<RfPackage> GetTransmitPackages()
        {
            var collection = new List<RfPackage>();

            //Set buffer from Properties
            AssignBuffer();

            for (int i = 0; i < this.Amount; i++)
            {
                var payload = new byte[SubSlaveSettingPackage.SettingSize + 1];
                //important => Setting Package Index
                payload[0] = (byte)i;
                System.Buffer.BlockCopy(this.Buffer, i * SubSlaveSettingPackage.SettingSize,
                                        payload, 1, SubSlaveSettingPackage.SettingSize);

                collection.Add(new RfPackage(this.TaskID, this.TaskType, payload));
            }

            return collection;
        }
        #endregion
    }
}
