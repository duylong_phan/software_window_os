﻿using MicroControllerApp.Interfaces;
using MicroControllerApp.Models.DataClasses.Settings;
using MicroControllerApp.Models.DataClasses.Settings.Others;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Models.DataClasses.Packages
{
    public enum PressMethods
    {
        PressedDuration = 0x01,
        TriggerCurve = 0x02
    }

    public enum PressBeepTypes
    {
        Start = 0x01,
        Closed = 0x02,
        Done = 0x04,
        Received = 0x08,
    }

    public class PressSlaveSettingPackage : SlaveSettingPackage, IRfSetting
    {
        #region Const
        public const int ID_Index =             0;
        public const int Method_Index =         1;
        public const int Frequency_Index =      2;
        public const int WindowSize_Index =     3;
        public const int PointAmount_Index =    4;
        public const int Mag_Beta_Index =       5;
        public const int Force_Beta_Index =     6;
        public const int Min_MagD_Index =       7;
        public const int Max_MagD_Index =       9;
        public const int Min_Force_Index =      11;
        public const int Max_Force_Index =      13;
        public const int Release_Force_Index =  15;
        public const int Enable_Beep_Index =    17;
        public const int Short_Beep_Index =     18;
        public const int Long_Beep_Index =      19;
        public const int Beep_On_Start_Index =  20;
        public const int Beep_On_Closed_Index = 21;
        public const int Beep_On_Done_Index =   22;
        public const int Beep_On_Received_Index = 23;
        public const int DoneAmount_Index =     28;
        public const int RF_Channel_Index =     29;
        public const int RF_Rx_Address_Index =  30;
        public const int RF_Tx_Address_Index =  35;
        public const int AbsForceDeltaSum_Index = 40;
        #endregion

        #region Getter, Setter
        private byte id;        
        private byte frequency;
        private byte windowSize;
        private byte pointAmount;
        private byte magBeta;
        private byte forceBeta;
        private byte shortBeep;
        private byte longBeep;
        private byte beepOnStartAmount;
        private byte beepOnClosedAmount;
        private byte beepOnDoneAmount;
        private byte beepOnReceivedAmount;
        private byte doneAmount;
        private byte rfChannel;
        private byte absForceDeltaSum;
        private ushort minMagD;
        private ushort maxMagD;
        private ushort minForce;
        private ushort maxForce;
        private ushort releaseForce;
        private bool usePressedDuration;
        private bool useTriggerCurve;
        private bool beepOnStart;
        private bool beepOnClosed;
        private bool beepOnDone;
        private bool beepOnReceived;
        private string rfRxAddress;
        private string rfTxAddress;

        public byte ID
        {
            get { return this.id; }
            set
            {
                this.SetProperty(ref this.id, value);
            }
        }
        public bool UsePressedDuration
        {
            get { return this.usePressedDuration; }
            set
            {
                this.SetProperty(ref this.usePressedDuration, value);
            }
        }
        public bool UseTriggerCurve
        {
            get { return this.useTriggerCurve; }
            set
            {
                this.SetProperty(ref this.useTriggerCurve, value);
            }
        }
        public byte Frequency
        {
            get { return this.frequency; }
            set
            {
                this.SetProperty(ref this.frequency, value);
            }
        }
        public byte WindowSize
        {
            get { return this.windowSize; }
            set
            {
                this.SetProperty(ref this.windowSize, value);
            }
        }
        public byte PointAmount
        {
            get { return this.pointAmount; }
            set
            {
                this.SetProperty(ref this.pointAmount, value);
            }
        }
        public byte MagBeta
        {
            get { return this.magBeta; }
            set
            {
                this.SetProperty(ref this.magBeta, value);
            }
        }
        public byte ForceBeta
        {
            get { return this.forceBeta; }
            set
            {
                this.SetProperty(ref this.forceBeta, value);
            }
        }
        public ushort MinMagD
        {
            get { return this.minMagD; }
            set
            {
                this.SetProperty(ref this.minMagD, value);
            }
        }
        public ushort MaxMagD
        {
            get { return this.maxMagD; }
            set
            {
                this.SetProperty(ref this.maxMagD, value);
            }
        }
        public ushort MinForce
        {
            get { return this.minForce; }
            set
            {
                this.SetProperty(ref this.minForce, value);
            }
        }
        public ushort MaxForce
        {
            get { return this.maxForce; }
            set
            {
                this.SetProperty(ref this.maxForce, value);
            }
        }
        public ushort ReleaseForce
        {
            get { return this.releaseForce; }
            set
            {
                this.SetProperty(ref this.releaseForce, value);
            }
        }        
        public bool BeepOnStart
        {
            get { return this.beepOnStart; }
            set
            {
                this.SetProperty(ref this.beepOnStart, value);
            }
        }
        public bool BeepOnClosed
        {
            get { return this.beepOnClosed; }
            set
            {
                this.SetProperty(ref this.beepOnClosed, value);
            }
        }
        public bool BeepOnDone
        {
            get { return this.beepOnDone; }
            set
            {
                this.SetProperty(ref this.beepOnDone, value);
            }
        }
        public bool BeepOnReceived
        {
            get { return this.beepOnReceived; }
            set
            {
                this.SetProperty(ref this.beepOnReceived, value);
            }
        }
        public byte ShortBeep
        {
            get { return this.shortBeep; }
            set
            {
                this.SetProperty(ref this.shortBeep, value);
            }
        }
        public byte LongBeep
        {
            get { return this.longBeep; }
            set
            {
                this.SetProperty(ref this.longBeep, value);
            }
        }
        public byte BeepOnStartAmount
        {
            get { return this.beepOnStartAmount; }
            set
            {
                this.SetProperty(ref this.beepOnStartAmount, value);
            }
        }
        public byte BeepOnClosedAmount
        {
            get { return this.beepOnClosedAmount; }
            set
            {
                this.SetProperty(ref this.beepOnClosedAmount, value);
            }
        }
        public byte BeepOnDoneAmount
        {
            get { return this.beepOnDoneAmount; }
            set
            {
                this.SetProperty(ref this.beepOnDoneAmount, value);
            }
        }
        public byte BeepOnReceivedAmount
        {
            get { return this.beepOnReceivedAmount; }
            set
            {
                this.SetProperty(ref this.beepOnReceivedAmount, value);
            }
        }        
        public byte DoneAmount
        {
            get { return this.doneAmount; }
            set
            {
                this.SetProperty(ref this.doneAmount, value);
            }
        }
        public byte RfChannel
        {
            get { return this.rfChannel; }
            set
            {
                this.SetProperty(ref this.rfChannel, value);
            }
        }
        public string RfRxAddress
        {
            get { return this.rfRxAddress; }
            set
            {
                this.SetProperty(ref this.rfRxAddress, value);
            }
        }
        public string RfTxAddress
        {
            get { return this.rfTxAddress; }
            set
            {
                this.SetProperty(ref this.rfTxAddress, value);
            }
        }
        public byte AbsForceDeltaSum
        {
            get { return this.absForceDeltaSum; }
            set
            {
                this.SetProperty(ref this.absForceDeltaSum, value);
            }
        }

        public object ByteRule { get; set; }
        public object UshortRule { get; set; }
        public object RfAddressRule { get; set; }
        #endregion

        #region Constructor
        public PressSlaveSettingPackage(byte type, List<SubSlaveSettingPackage> collection)
            : base(type, collection, (byte)MicroTasks.Press, (byte)PressTypes.WriteSetting)
        {

        }
        #endregion

        #region Protected method
        protected override void AssignProperties()
        {
            this.id = this.Buffer[ID_Index];
            this.frequency = this.Buffer[Frequency_Index];
            this.windowSize = this.Buffer[WindowSize_Index];
            this.pointAmount = this.Buffer[PointAmount_Index];
            this.magBeta = this.Buffer[Mag_Beta_Index];
            this.forceBeta = this.Buffer[Force_Beta_Index];
            this.shortBeep = this.Buffer[Short_Beep_Index];
            this.longBeep = this.Buffer[Long_Beep_Index];
            this.beepOnStartAmount = this.Buffer[Beep_On_Start_Index];
            this.beepOnClosedAmount = this.Buffer[Beep_On_Closed_Index];
            this.beepOnDoneAmount = this.Buffer[Beep_On_Done_Index];
            this.beepOnReceivedAmount = this.Buffer[Beep_On_Received_Index];
            this.doneAmount = this.Buffer[DoneAmount_Index];
            this.rfChannel = this.Buffer[RF_Channel_Index];
            this.absForceDeltaSum = this.Buffer[AbsForceDeltaSum_Index];

            this.minMagD = BitConverter.ToUInt16(this.Buffer, Min_MagD_Index);
            this.maxMagD = BitConverter.ToUInt16(this.Buffer, Max_MagD_Index);
            this.minForce = BitConverter.ToUInt16(this.Buffer, Min_Force_Index);
            this.maxForce = BitConverter.ToUInt16(this.Buffer, Max_Force_Index);
            this.releaseForce = BitConverter.ToUInt16(this.Buffer, Release_Force_Index);

            this.usePressedDuration = ((this.Buffer[Method_Index] & (byte)PressMethods.PressedDuration) != 0);
            this.useTriggerCurve = ((this.Buffer[Method_Index] & (byte)PressMethods.TriggerCurve) != 0);
            this.beepOnStart = ((this.Buffer[Enable_Beep_Index] & (byte)PressBeepTypes.Start) != 0);
            this.beepOnClosed = ((this.Buffer[Enable_Beep_Index] & (byte)PressBeepTypes.Closed) != 0);
            this.beepOnDone = ((this.Buffer[Enable_Beep_Index] & (byte)PressBeepTypes.Done) != 0);
            this.beepOnReceived = ((this.Buffer[Enable_Beep_Index] & (byte)PressBeepTypes.Received) != 0);

            this.rfRxAddress = ViewClasses.ValidationRules.RfAddressRule.GetAddressText(this.Buffer, RF_Rx_Address_Index);
            this.rfTxAddress = ViewClasses.ValidationRules.RfAddressRule.GetAddressText(this.Buffer, RF_Tx_Address_Index);
        }

        protected override void AssignBuffer()
        {
            this.Buffer[ID_Index] = this.id;
            this.Buffer[Frequency_Index] = this.frequency;
            this.Buffer[WindowSize_Index] = this.windowSize;
            this.Buffer[PointAmount_Index] = this.pointAmount;
            this.Buffer[Mag_Beta_Index] = this.magBeta;
            this.Buffer[Force_Beta_Index] = this.forceBeta;
            this.Buffer[Short_Beep_Index] = this.shortBeep;
            this.Buffer[Long_Beep_Index] = this.longBeep;
            this.Buffer[Beep_On_Start_Index] = this.beepOnStartAmount;
            this.Buffer[Beep_On_Closed_Index] = this.beepOnClosedAmount;
            this.Buffer[Beep_On_Done_Index] = this.beepOnDoneAmount;
            this.Buffer[Beep_On_Received_Index] = this.beepOnReceivedAmount;
            this.Buffer[DoneAmount_Index] = this.doneAmount;
            this.Buffer[RF_Channel_Index] = this.rfChannel;
            this.Buffer[AbsForceDeltaSum_Index] = this.absForceDeltaSum;

            WriteUshort(this.Buffer, Min_MagD_Index, this.minMagD);
            WriteUshort(this.Buffer, Max_MagD_Index, this.maxMagD);
            WriteUshort(this.Buffer, Min_Force_Index, this.minForce);
            WriteUshort(this.Buffer, Max_Force_Index, this.maxForce);
            WriteUshort(this.Buffer, Release_Force_Index, this.releaseForce);

            this.Buffer[Method_Index] = 0;
            WriteBit(this.Buffer, Method_Index, this.usePressedDuration, (byte)PressMethods.PressedDuration);
            WriteBit(this.Buffer, Method_Index, this.useTriggerCurve, (byte)PressMethods.TriggerCurve);

            this.Buffer[Enable_Beep_Index] = 0;
            WriteBit(this.Buffer, Enable_Beep_Index, this.beepOnStart, (byte)PressBeepTypes.Start);
            WriteBit(this.Buffer, Enable_Beep_Index, this.beepOnClosed, (byte)PressBeepTypes.Closed);
            WriteBit(this.Buffer, Enable_Beep_Index, this.beepOnDone, (byte)PressBeepTypes.Done);
            WriteBit(this.Buffer, Enable_Beep_Index, this.beepOnReceived, (byte)PressBeepTypes.Received);

            WriteRfAddress(this.Buffer, RF_Rx_Address_Index, this.rfRxAddress);
            WriteRfAddress(this.Buffer, RF_Tx_Address_Index, this.rfTxAddress);
        }
        #endregion

        #region Private Method
        private void WriteUshort(byte[] buffer, int index, ushort value)
        {
            var data = BitConverter.GetBytes(value);
            System.Buffer.BlockCopy(data, 0, buffer, index, data.Length);
        }

        private void WriteBit(byte[] buffer, int index, bool isSet, byte bitValue)
        {
            if (isSet)
            {
                buffer[index] |= bitValue;
            }
        }

        private void WriteRfAddress(byte[] buffer, int index, string address)
        {
            var data = ViewClasses.ValidationRules.RfAddressRule.GetAddressBytes(address);
            System.Buffer.BlockCopy(data, 0, buffer, index, data.Length);
        }
        #endregion
    }
}
