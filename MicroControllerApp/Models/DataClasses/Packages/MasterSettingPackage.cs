﻿using MicroControllerApp.Interfaces;
using MicroControllerApp.Models.Base;
using MicroControllerApp.Models.DataClasses.Settings;
using MicroControllerApp.Models.ViewClasses.ValidationRules;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Models.DataClasses.Packages
{
    public class MasterSettingPackage : DataPackage, ISharedPackage, IRfSetting
    {
        #region Const
        public const int SettingSize = 58;        
        public const int OffsetIndex = 2;
        public const int RF_Channel_Index = 0;
        public const int RF_Rx_Address_Index = 1;
        public const int RF_Tx_Address_Index = 6;
        #endregion

        #region Getter, setter
        private byte rfChannel;
        private string rfRxAddress;
        private string rfTxAddress;

        public byte RfChannel
        {
            get { return this.rfChannel; }
            set
            {
                this.SetProperty(ref this.rfChannel, value);
            }
        }
        public string RfRxAddress
        {
            get { return this.rfRxAddress; }
            set
            {
                this.SetProperty(ref this.rfRxAddress, value);
            }
        }
        public string RfTxAddress
        {
            get { return this.rfTxAddress; }
            set
            {
                this.SetProperty(ref this.rfTxAddress, value);
            }
        }

        public object ByteRule { get; set; }
        public object RfAddressRule { get; set; }
        #endregion

        #region Constructor
        public MasterSettingPackage(byte type, byte[] buffer)
            : base(type, buffer)
        {
            this.rfChannel = this.Buffer[OffsetIndex + RF_Channel_Index];
            this.rfRxAddress = ViewClasses.ValidationRules.RfAddressRule.GetAddressText(this.Buffer, RF_Rx_Address_Index + OffsetIndex);
            this.rfTxAddress = ViewClasses.ValidationRules.RfAddressRule.GetAddressText(this.Buffer, RF_Tx_Address_Index + OffsetIndex);
        }
        #endregion

        #region Public Method
        public byte[] GetWriteBytes()
        {
            var data = new byte[SettingSize + OffsetIndex + Header.Length];

            using (var stream = new MemoryStream(data))
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(Header);
                writer.Write((byte)PcCommands.Write_MasterSetting);
                writer.Write((byte)SettingSize);

                writer.Write(this.rfChannel);
                writer.Write(ViewClasses.ValidationRules.RfAddressRule.GetAddressBytes(this.rfRxAddress));
                writer.Write(ViewClasses.ValidationRules.RfAddressRule.GetAddressBytes(this.rfTxAddress));                
            }

            return data;
        }
        #endregion
    }
}
