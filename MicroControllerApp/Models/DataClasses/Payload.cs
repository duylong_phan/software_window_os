﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Models.DataClasses
{
    public class Payload
    {
        #region Getter
        public byte Type { get; private set; }
        public byte[] Data { get; private set; }
        #endregion

        #region Constructor
        public Payload(byte type, byte[] data)
        {
            this.Type = type;
            this.Data = data;
        }
        #endregion
    }
}
