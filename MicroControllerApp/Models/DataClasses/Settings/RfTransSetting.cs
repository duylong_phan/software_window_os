﻿using MicroControllerApp.Models.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MicroControllerApp.Interfaces;
using LongModel.Models.IO.Files;
using MicroControllerApp.Models.DataClasses.Settings.Sub;
using System.Xml.Serialization;
using OxyPlot;
using OxyPlot.Axes;
using System.Windows.Media;
using MicroControllerApp.Models.DataClasses.Packages;
using MicroControllerApp.Models.DataClasses.Settings.Others;

namespace MicroControllerApp.Models.DataClasses.Settings
{
    public class RfTransSetting : SettingBase, IMatFile, IGraphic
    {
        #region Getter, setter     
        private bool syncMatFile;

        [XmlAttribute("SyncMatFile")]
        public bool SyncMatFile
        {
            get { return this.syncMatFile; }
            set
            {
                this.SetProperty(ref this.syncMatFile, value);
            }
        }
        [XmlElement("GraphicSetting")]
        public GraphicSetting GraphicSetting { get; set; }        
        [XmlElement("MatFileSetting")]
        public FileSingleSetting MatFileSetting { get; set; }
        [XmlElement("OperSetting")]
        public RfTransOperSetting OperSetting { get; set; }
        [XmlIgnore]
        public SampleInfo SampleInfo { get; set; }
        #endregion

        #region Constructor
        public RfTransSetting()
            : base(MicroTasks.RfTrans, new RemoteSetting(), new ServerSetting())
        {
            this.SyncMatFile = false;
            this.MatFileSetting = new FileSingleSetting(App.DataPath, "Trans_Data", ".mat", 10000, 1, true, 60, false, true, false);
            this.OperSetting = new RfTransOperSetting();

            #region Graphic
            this.GraphicSetting = new GraphicSetting();
            this.GraphicSetting.AxisCollection.Add(new AxisInfo("Time", "s", false, false, LineStyle.LongDash, AxisPosition.Bottom));
            this.GraphicSetting.AxisCollection.Add(new AxisInfo("Package Amount", string.Empty, false, false, LineStyle.LongDash, AxisPosition.Left));
            this.GraphicSetting.CurveCollection.Add(new CurveInfo("Received Amount", Colors.Blue, LineStyle.Solid));
            #endregion

            #region SampleInfo
            var columns = new List<ColumnInfo>();
            columns.Add(new ColumnInfo("Time", 1, "Sample Time", "s"));
            columns.Add(new ColumnInfo("Amount", 2, "Received Package amount", string.Empty));
            this.SampleInfo = new SampleInfo(10, string.Empty, columns);
            #endregion
        }
        #endregion

        #region Public Method
        public override DataPackage ParseBuffer(byte[] buffer)
        {
            DataPackage package = null;

            switch (buffer[0])
            {
                case (byte)PcCommands.RfTrans_Status:
                    package = new RfTransStatusPackage(buffer[0], buffer);
                    break;

                case (byte)PcCommands.RfTrans_Count:
                    package = new RfTransCountPackage(buffer[0], buffer);
                    break;

                default:
                    //ignore
                    break;
            }

            return package;
        }
        #endregion
    }
}
