﻿using MicroControllerApp.Models.DataClasses.Settings.Sub;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LongModel.Models.IO.Files;
using System.Xml.Serialization;
using OxyPlot;
using System.Windows.Media;
using OxyPlot.Axes;
using MicroControllerApp.Models.Base;
using MicroControllerApp.Interfaces;
using MicroControllerApp.Models.DataClasses.Packages;

namespace MicroControllerApp.Models.DataClasses.Settings
{
    public class PressSetting : SettingBase, IGraphic, IMatFile
    {
        #region Const, static
        public const string PressNameFormat = "dd_MM_yyyy";
        public const string PressExtension = ".press";
        public static string PressFilter { get; private set; }

        static PressSetting()
        {
            PressFilter = string.Format("Press Pliers File (*{0})|*{0}", PressExtension);
        }
        #endregion

        #region Getter, setter       
        [XmlElement("GraphicSetting")]
        public GraphicSetting GraphicSetting { get; set; }
        [XmlElement("PressFileSetting")]
        public PressFileSetting PressFileSetting { get; set; }
        [XmlElement("MatFileSetting")]
        public FileSingleSetting MatFileSetting { get; set; }
        [XmlIgnore]
        public SampleInfo SampleInfo { get; set; }
        #endregion

        #region Constructor
        public PressSetting()
            : base(MicroTasks.Press, new RemoteSetting(4321, "Client1", "127.0.0.1"), new ServerSetting(4321, false))
        {
            this.PressFileSetting = new PressFileSetting(App.DataPath, 2, true, true, true);
            this.MatFileSetting = new FileSingleSetting(App.DataPath, "Press_Data", ".mat", 10000, 1, true, 60, false, true, false);

            #region Graphic
            this.GraphicSetting = new GraphicSetting();
            this.GraphicSetting.AxisCollection.Add(new AxisInfo("Time", "s", false, false, LineStyle.LongDash, AxisPosition.Bottom));
            this.GraphicSetting.AxisCollection.Add(new AxisInfo("Amplitude", "mV", false, false, LineStyle.LongDash, AxisPosition.Left));
            this.GraphicSetting.CurveCollection.Add(new CurveInfo("Mag_D", Colors.Blue, LineStyle.Solid));
            this.GraphicSetting.CurveCollection.Add(new CurveInfo("Force", Colors.Red, LineStyle.Solid));
            #endregion

            #region SampleInfo
            var columns = new List<ColumnInfo>();
            columns.Add(new ColumnInfo("Time", 1, "Sample Time", "s"));
            columns.Add(new ColumnInfo("MagD", 2,"Deviration of Magnetic field", "mV"));
            columns.Add(new ColumnInfo("Force", 3,"Pressing Force", "mV"));
            this.SampleInfo = new SampleInfo(80, string.Empty, columns);
            #endregion
        }
        #endregion

        #region Public Method
        public override DataPackage ParseBuffer(byte[] buffer)
        {
            DataPackage package = null;

            switch (buffer[0])
            {
                case (byte)PcCommands.Press_Status:
                    package = new PressStatusPackage(buffer[0], buffer);
                    break;

                case (byte)PcCommands.Press_Pressed:
                    package = new PressInfoPackage(buffer[0], buffer);
                    break;

                case (byte)PcCommands.Press_Sample:
                    package = new PressSamplePackage(buffer[0], buffer);
                    break;

                default:
                    //do nothing
                    break;
            }
            return package;
        }
        #endregion
    }
}
