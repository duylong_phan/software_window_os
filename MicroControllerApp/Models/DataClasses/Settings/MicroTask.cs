﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Models.DataClasses.Settings
{
    public enum MicroTasks
    {
        Non = 0,
        Press = 1,
        Capacitor = 2,
        Acceleration = 3,
        uBalance = 4,
        RfTrans = 5,   
        MotorCtr = 6     
    }
    
    public enum AppPositions
    {
        Local,
        Remote
    }

    public enum AppViews
    {
        Non,
        Graphic,
        OutPinCtr,
        InPinCtr,
        PressTable,
        PressLocalControl,
        PressRemoteControl,
        CapControl,
        uBalanceEditor,
        uBalanceControl,
        RfTransControl,   
        MotorCtrContol,        
        MotorCtrEditor,
        AccControl
    }

    public enum PhysicalPorts
    {
        Serial,
        USB,
        WebServer,
        WebClient,
        Native
    }

    [Flags]
    public enum PcActions
    {
        AppStatus = 1 << 0,
        InfoMessage = 1 << 1,
        WarningMessage = 1 << 2,
        ErrorMessage = 1 << 3,
    }

    public enum PcCommands : byte
    {
        Error = 0,
        Start = 1,
        Stop = 2,
        RF_Transmit = 3,
        Command_Done = 4,
        Read_MasterSetting = 5,
        Write_MasterSetting = 6,
        Read_SlaveSetting = 7,
        Write_SlaveSetting = 8,
        PinWrite = 9,
        PinRead = 10,
        Press_Pressed = 50,
        Press_Sample = 51,
        Press_Status = 52,        
        Cap_Done = 60,
        uBalance_Status = 61,
        RfTrans_Status = 62,
        RfTrans_Count = 63,
        MotorCtr_Status = 64,
        MotorCtr_DataSample = 65,
        Accelerometer_Done = 66
    }
}
