﻿using LongInterface.Models;
using LongModel.BaseClasses;
using LongModel.Helpers;
using MicroControllerApp.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MicroControllerApp.Models.DataClasses.Settings.Sub
{
    public interface IGraphicSetting : IPlot, ICopyable<IGraphicSetting>
    {
        int PointAmount { get; set; }        
    }

    public class GraphicSetting : ObservableObject, IGraphicSetting
    {
        #region Getter, setter
        private bool isLegendVisible;
        private int pointAmount;

        [XmlAttribute("IsLegendVisible")]
        public bool IsLegendVisible
        {
            get { return this.isLegendVisible; }
            set
            {
                this.SetProperty(ref this.isLegendVisible, value);
            }
        }
        [XmlAttribute("PointAmount")]
        public int PointAmount
        {
            get { return this.pointAmount; }
            set
            {
                this.SetProperty(ref this.pointAmount, value);
            }
        }

        [XmlArray("CurveCollection"), XmlArrayItem("CurveInfo", typeof(CurveInfo))]
        public List<CurveInfo> CurveCollection { get; set; }
        [XmlArray("AxisCollection"), XmlArrayItem("AxisInfo", typeof(AxisInfo))]
        public List<AxisInfo> AxisCollection { get; set; }

        [XmlIgnore]
        public object PointAmountRule { get; set; }
        #endregion

        #region Constructor
        public GraphicSetting()
            : this(2500, false)
        {

        }

        public GraphicSetting(int pointAmount, bool isLegendVisible)
        {
            this.pointAmount = pointAmount;
            this.isLegendVisible = isLegendVisible;
            this.CurveCollection = new List<CurveInfo>();
            this.AxisCollection = new List<AxisInfo>();
        }
        #endregion

        #region Public Method
        public void CopyProperties(IGraphicSetting item)
        {
            if (item != null)
            {
                ModelHelper.CopyProperties(item, this);
            }
        }

        public void CopyProperties(IPlot item)
        {
            if (item != null)
            {
                ModelHelper.CopyProperties(item, this);
            }
        }
        #endregion
    }
}
