﻿using LongInterface.Models;
using LongInterface.Models.IO.Files;
using LongModel.BaseClasses;
using MicroControllerApp.Interfaces;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using LongModel.Helpers;
using System.Windows.Input;
using MicroControllerApp.Models.IO;

namespace MicroControllerApp.Models.DataClasses.Settings.Sub
{
    public class UsbSetting : ObservableObject, IUsbSetting, ICopyable<IUsbSetting>, ICloneModel
    {
        #region Getter, Setter
        [XmlIgnore]
        public string VendorIdText
        {
            get { return this.VendorID.ToString("X"); }
            set
            {
                int tmpValue = 0;
                if (int.TryParse(value, NumberStyles.HexNumber, CultureInfo.InvariantCulture, out tmpValue))
                {
                    if(tmpValue != this.VendorID)
                    {
                        this.VendorID = tmpValue;
                        OnPropertyChanged("VendorIdText");
                    }
                }
            }
        }
        [XmlIgnore]
        public string ProductIdText
        {
            get { return this.ProductID.ToString("X"); }
            set
            {
                int tmpValue = 0;
                if (int.TryParse(value, NumberStyles.HexNumber, CultureInfo.InvariantCulture, out tmpValue))
                {
                    if (tmpValue != this.ProductID)
                    {
                        this.ProductID = tmpValue;
                        OnPropertyChanged("ProductIdText");
                    }
                }
            }
        }
        
        [XmlAttribute("VendorID")]
        public int VendorID { get; set; }
        [XmlAttribute("ProductID")]
        public int ProductID { get; set; }

        [XmlIgnore]
        public object IdRule { get; set; }
        [XmlIgnore]
        public ICommand EnableDeviceSignature { get; set; }
        [XmlIgnore]
        public ICommand DisableDeviceSignature { get; set; }
        #endregion

        #region Constructor
        public UsbSetting()
            :this(UsbPort.DefaultVendorID, UsbPort.DefaultProductID)
        {

        }
        public UsbSetting(int vendorID, int productID)
        {
            this.VendorID = vendorID;
            this.ProductID = productID;
        }
        #endregion

        #region Public Method
        public void CopyProperties(IUsbSetting item)
        {
            if (item != null)
            {
                ModelHelper.CopyProperties(item, this);
            }
        }

        public object CloneModel()
        {
            var setting = new UsbSetting();
            setting.CopyProperties(this);
            return setting;
        }
        #endregion
    }
}
