﻿using LongInterface.Models;
using LongModel.BaseClasses;
using LongModel.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Xml.Serialization;

namespace MicroControllerApp.Models.DataClasses.Settings.Sub
{
    public interface IServerSetting : ICloneModel, ICopyable<IServerSetting>
    {        
        int Port { get; set; }
    }

    public class ServerSetting : ObservableObject, IServerSetting
    {
        #region Getter, setter
        private int port;
        private bool isEnabled;

        [XmlAttribute("Port")]
        public int Port
        {
            get { return this.port; }
            set
            {
                this.SetProperty(ref this.port, value);
            }
        }
        [XmlAttribute("IsEnabled")]
        public bool IsEnabled
        {
            get { return this.isEnabled; }
            set
            {
                this.SetProperty(ref this.isEnabled, value);
            }
        }
        [XmlIgnore]
        public object PortRule { get; set; }
        [XmlIgnore]
        public ICommand GetIpAddress { get; set; }
        [XmlIgnore]
        public ICommand RegisterPort { get; set; }
        [XmlIgnore]
        public ICommand RegisterPrefix { get; set; }
        #endregion

        #region Constructor
        public ServerSetting()
            : this(1234, false)
        {

        }

        public ServerSetting(int port, bool isEnabled)
        {
            this.port = port;
            this.isEnabled = isEnabled;
        }
        #endregion

        #region Public Method
        public object CloneModel()
        {
            var setting = new ServerSetting();
            setting.CopyProperties(this);
            return setting;
        }

        public void CopyProperties(IServerSetting item)
        {
            if (item != null)
            {
                ModelHelper.CopyProperties(item, this);
            }
        }
        #endregion
    }
}
