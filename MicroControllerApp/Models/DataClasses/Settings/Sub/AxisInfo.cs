﻿using LongModel.BaseClasses;
using LongModel.Helpers;
using MicroControllerApp.Interfaces;
using OxyPlot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using OxyPlot.Axes;
using System.Windows.Input;

namespace MicroControllerApp.Models.DataClasses.Settings.Sub
{
    public class AxisInfo : ObservableObject, IAxis
    {
        #region Getter, setter
        private string title;       
        private string unit;
        private bool isZoomEnabled;
        private bool isPanEnabled;
        private LineStyle majorGridlineStyle;
        private AxisPosition position;

        [XmlAttribute("Title")]
        public string Title
        {
            get { return this.title; }
            set
            {
                this.SetProperty(ref this.title, value);
            }
        }
        [XmlAttribute("Unit")]
        public string Unit
        {
            get { return this.unit; }
            set
            {
                this.SetProperty(ref this.unit, value);
            }
        }
        [XmlAttribute("IsZoomEnabled")]
        public bool IsZoomEnabled
        {
            get { return this.isZoomEnabled; }
            set
            {
                this.SetProperty(ref this.isZoomEnabled, value);
            }
        }
        [XmlAttribute("IsPanEnabled")]
        public bool IsPanEnabled
        {
            get { return this.isPanEnabled; }
            set
            {
                this.SetProperty(ref this.isPanEnabled, value);
            }
        }
        [XmlAttribute("LineStyle")]
        public LineStyle MajorGridlineStyle
        {
            get { return this.majorGridlineStyle; }
            set
            {
                this.SetProperty(ref this.majorGridlineStyle, value);
            }
        }
        [XmlAttribute("AxisPosition")]
        public AxisPosition Position
        {
            get { return this.position; }
            set
            {
                this.SetProperty(ref this.position, value);
            }
        }

        [XmlIgnore]
        public object LineStyleCollection { get; set; }
        [XmlIgnore]
        public ICommand OpenSetting { get; set; }
        #endregion

        #region Constructor
        public AxisInfo()
            : this("no title", "no unit", false, false, LineStyle.LongDash, AxisPosition.Bottom)
        {

        }

        public AxisInfo(string title, string unit, bool isPanEnable, bool isZoomEnable, LineStyle majorGridlineStyle, AxisPosition position)
        {
            this.title = title;
            this.unit = unit;
            this.isPanEnabled = isPanEnable;
            this.isZoomEnabled = isZoomEnable;
            this.majorGridlineStyle = majorGridlineStyle;
            this.position = position;
        }
        #endregion

        #region Public Method       
        public void CopyProperties(IAxis item)
        {
            if (item != null)
            {
                ModelHelper.CopyProperties(item, this);
            }
        }

        public object CloneModel()
        {
            AxisInfo info = new AxisInfo();
            info.CopyProperties(this);
            return info;
        }
        #endregion
    }
}
