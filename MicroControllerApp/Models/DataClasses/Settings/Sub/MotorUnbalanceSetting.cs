﻿using LongModel.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MicroControllerApp.Models.DataClasses.Settings.Sub
{
    public class MotorUnbalanceSetting : ObservableObject
    {
        #region Getter, setter
        private double desiredValue;
        private double tolerance;
        private int pulseAmount;

        [XmlAttribute("DesiredValue")]
        public double DesiredValue
        {
            get { return this.desiredValue; }
            set
            {
                this.SetProperty(ref this.desiredValue, value);
            }
        }
        [XmlAttribute("Tolerance")]
        public double Tolerance
        {
            get { return this.tolerance; }
            set
            {
                this.SetProperty(ref this.tolerance, value);
            }
        }
        [XmlAttribute("PulseAmount")]
        public int PulseAmount
        {
            get { return this.pulseAmount; }
            set
            {
                this.SetProperty(ref this.pulseAmount, value);
            }
        }        

        [XmlIgnore]
        public object PosDoubleRule { get; set; }
        [XmlIgnore]
        public object NonZeroUshortRule { get; set; }
        #endregion

        #region Constructor
        public MotorUnbalanceSetting()
        {
            this.desiredValue = 1;
            this.tolerance = 0.05;
            this.pulseAmount = 1000;
        }
        #endregion
    }
}
