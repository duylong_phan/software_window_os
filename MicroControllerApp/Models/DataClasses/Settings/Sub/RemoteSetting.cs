﻿using LongInterface.Models;
using LongInterface.Models.IO.Files;
using LongModel.BaseClasses;
using LongModel.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Xml.Serialization;

namespace MicroControllerApp.Models.DataClasses.Settings.Sub
{
    public class RemoteSetting : ObservableObject, IClientSetting, ICopyable<IClientSetting>, ICloneModel
    {
        #region Getter, setter
        private int timeout;
        private int updateInterval;
        private int port;
        private string name;
        private string address;

        [XmlAttribute("Timeout")]
        public int Timeout
        {
            get { return this.timeout; }
            set
            {
                this.SetProperty(ref this.timeout, value);
            }
        }
        [XmlAttribute("UpdateInterval")]
        public int UpdateInterval
        {
            get { return this.updateInterval; }
            set
            {
                this.SetProperty(ref this.updateInterval, value);
            }
        }
        [XmlAttribute("Port")]
        public int Port
        {
            get { return this.port; }
            set
            {
                this.SetProperty(ref this.port, value);
            }
        }
        [XmlAttribute("Name")]
        public string Name
        {
            get { return this.name; }
            set
            {
                this.SetProperty(ref this.name, value);
            }
        }
        [XmlAttribute("Address")]
        public string Address
        {
            get { return this.address; }
            set
            {
                this.SetProperty(ref this.address, value);
            }
        }        

        [XmlIgnore]
        public object PortRule { get; set; }
        [XmlIgnore]
        public object TimeRule { get; set; }
        [XmlIgnore]
        public object NameRule { get; set; }
        [XmlIgnore]
        public object AddressRule { get; set; }       
        [XmlIgnore]
        public ICommand CheckServer { get; set; }
        #endregion

        #region Constructor
        public RemoteSetting()
            : this(1234, "Client1", "127.0.0.1")
        {

        }

        public RemoteSetting(int port, string name, string address)
        {
            this.port = port;
            this.name = name;
            this.address = address;
            this.timeout = 5000;
            this.updateInterval = 500;
        }
        #endregion

        #region Public Method
        public object CloneModel()
        {
            var setting = new RemoteSetting();
            setting.CopyProperties(this);
            return setting;
        }

        public void CopyProperties(IClientSetting item)
        {
            if (item != null)
            {
                ModelHelper.CopyProperties(item, this);
            }
        }
        #endregion
    }
}
