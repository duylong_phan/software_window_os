﻿using LongModel.BaseClasses;
using MicroControllerApp.Models.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MicroControllerApp.Models.DataClasses.Settings
{
    public class Configuration : ObservableObject
    {
        #region Getter, setter
        private MicroTasks task;

        [XmlAttribute("Task")]
        public MicroTasks Task
        {
            get { return this.task; }
            set
            {
                this.SetProperty(ref this.task, value);
            }
        }

        [XmlElement("PressSetting")]
        public PressSetting PressSetting { get; set; }
        [XmlElement("CapSetting")]
        public CapSetting CapSetting { get; set; }
        [XmlElement("AccSetting")]
        public AccSetting AccSetting { get; set; }
        [XmlElement("uBalanceSetting")]
        public uBalanceSetting uBalanceSetting { get; set; }
        [XmlElement("RfTransSetting")]
        public RfTransSetting RfTransSetting { get; set; }
        [XmlElement("MotorCtrSetting")]
        public MotorCtrSetting MotorCtrSetting { get; set; }
        #endregion

        #region Getter
        [XmlIgnore]
        public SettingBase CurrentSetting
        {
            get
            {
                SettingBase setting = null;

                switch (this.task)
                {
                    case MicroTasks.Press:          setting = this.PressSetting;        break;
                    case MicroTasks.Capacitor:      setting = this.CapSetting;          break;
                    case MicroTasks.uBalance:       setting = this.uBalanceSetting;     break;
                    case MicroTasks.RfTrans:        setting = this.RfTransSetting;      break;
                    case MicroTasks.MotorCtr:       setting = this.MotorCtrSetting;     break;
                    case MicroTasks.Acceleration:   setting = this.AccSetting;          break;         
                    case MicroTasks.Non:
                    default:
                        //nothing
                        break;
                }

                return setting;
            }
        }
        #endregion

        #region Constructor
        public Configuration()
        {
            this.task = MicroTasks.Non;
            this.PressSetting = new PressSetting();
            this.CapSetting = new CapSetting();
            this.AccSetting = new AccSetting();
            this.uBalanceSetting = new uBalanceSetting();
            this.RfTransSetting = new RfTransSetting();
            this.MotorCtrSetting = new MotorCtrSetting();         
        }
        #endregion
        
    }
}
