﻿using LongInterface.Models;
using LongModel.BaseClasses;
using MicroControllerApp.Interfaces;
using MicroControllerApp.Models.Base;
using MicroControllerApp.Models.DataClasses.Packages;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
namespace MicroControllerApp.Models.DataClasses.Settings.Others
{
    public enum RfTransTypes
    {
        Status = 1 << 0,
        Package = 1 << 1
    }

    public enum RfTransStatus
    {
        Ping = 0,
        WriteSetting = 1,
        WriteSettingDone = 2,
        StartOper = 3,
        StopOper = 4,
        OperDone = 5,
        OperError = 6,
        OperWorking = 7
    }

    public class RfTransAction : ObservableObject, IBuffer
    {
        #region Static
        public static RfTransAction Ping
        {
            get
            {
                return new RfTransAction((byte)RfTransStatus.Ping);
            }
        }

        public static RfTransAction StartOper
        {
            get
            {
                return new RfTransAction((byte)RfTransStatus.StartOper);
            }
        }
        #endregion

        #region Getter        
        [XmlIgnore]
        public byte Action { get; private set; }
        #endregion

        #region Constructor
        public RfTransAction(byte action)            
        {            
            this.Action = action;
        }

        #endregion

        #region Public Method
        /// <summary>
        /// Get Buffer to transmit to Slave over RF
        /// </summary>
        /// <returns></returns>
        public virtual byte[] GetBytes()
        {
            return new byte[] { this.Action };
        }
        #endregion
    }
}
