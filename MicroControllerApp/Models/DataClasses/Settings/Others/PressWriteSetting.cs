﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Models.DataClasses.Settings.Others
{
    public class PressWriteSetting : PressAction
    {
        #region Getter
        public byte Index { get; private set; }
        public byte[] Buffer { get; private set; }
        #endregion

        #region Constructor
        public PressWriteSetting(byte index, byte[] buffer)
            : base((byte)PressTypes.WriteSetting)
        {
            this.Index = index;
            this.Buffer = buffer;
        }
        #endregion

        #region Public Method
        public override byte[] GetBytes()
        {
            byte[] data = null;
            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(this.Index);
                writer.Write(this.Buffer);

                data = stream.ToArray();
            }
            return data;
        }
        #endregion
    }
}
