﻿using MicroControllerApp.Models.Base;
using MicroControllerApp.Models.DataClasses.Packages;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LongModel.Helpers;

namespace MicroControllerApp.Models.DataClasses.Settings.Others
{
    public enum uBalanceTypes
    {
        Ping = 0,
        StartTrans = 1,
        Actions = 2,
        EndTrans = 3,
        TransDone = 4,
        TransError = 5,
        StartOper = 6,
        StopOper = 7,
        OperDone = 8,
        OperError = 9,
        NoActions = 10,
        ActionWorking = 11,
    }

    public static class uBalanceHelper
    {
        #region Public Method
        public static List<byte[]> GetActionsPackages(List<uBalanceChannelAction> input)
        {
            int index = 0;
            var payload = new byte[0];
            var output = new List<byte[]>();

            #region StartTrans
            RfPackage package = new RfPackage((byte)MicroTasks.uBalance, (byte)uBalanceTypes.StartTrans, payload);
            output.Add(package.GetBytes());
            #endregion

            #region Actions
            int factor = input.Count / 3;
            if ((input.Count % 3) != 0)
            {
                factor++;
            }
            for (int i = 0; i < factor; i++)
            {
                using (var stream = new MemoryStream())
                using (var writer = new BinaryWriter(stream))
                {
                    for (int k = 0; k < 3; k++)
                    {
                        if (input.Count > index)
                        {
                            writer.Write(input[index].GetBytes());
                        }
                        else
                        {
                            writer.Write(uBalanceChannelAction.GetEmptyBytes());
                        }
                        index++;
                    }
                    payload = stream.ToArray();
                    package = new RfPackage((byte)MicroTasks.uBalance, (byte)uBalanceTypes.Actions, payload);
                    output.Add(package.GetBytes());
                }
            }
            #endregion

            #region EndTrans
            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write((ushort)input.Count);
                payload = stream.ToArray();
            }
            package = new RfPackage((byte)MicroTasks.uBalance, (byte)uBalanceTypes.EndTrans, payload);
            output.Add(package.GetBytes());
            #endregion

            return output;
        }

        public static void SaveFile(Stream stream, List<uBalanceActionContainer> collection)
        {
            using (var writer = new StreamWriter(stream))
            {
                foreach (var item in collection)
                {
                    foreach (var subItem in item)
                    {
                        writer.WriteLine(XmlSerializerHelper.GetXML(subItem));
                    }
                }
            }
        }

        public static void LoadFile(Stream stream, List<uBalanceActionContainer> collection)
        {
            //use dict, to avoid update Collection in Non UI-Thread
            Dictionary<uBalanceActionContainer, List<uBalanceChannelAction>> dict = new Dictionary<uBalanceActionContainer, List<uBalanceChannelAction>>();
            foreach (var item in collection)
            {
                dict.Add(item, new List<uBalanceChannelAction>());
            }

            //Load and store in dict
            using (var reader = new StreamReader(stream))
            {
                string line = reader.ReadLine();
                while (line != null)
                {
                    var action = XmlSerializerHelper.GetData(line, typeof(uBalanceChannelAction)) as uBalanceChannelAction;
                    foreach (var item in collection)
                    {
                        if (item.Channel == action.Channel)
                        {
                            dict[item].Add(action);
                            break;
                        }
                    }
                    line = reader.ReadLine();
                }
            }

            //remove Range and add Range
            foreach (var item in collection)
            {
                item.RemoveRange(0, item.Count);
                item.AddRange(dict[item]);
            }
        }
        #endregion
    }
}
