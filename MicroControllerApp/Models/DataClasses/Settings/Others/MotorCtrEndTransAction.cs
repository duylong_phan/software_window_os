﻿using LongInterface.Models;
using MicroControllerApp.Models.Base;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Models.DataClasses.Settings.Others
{
    [Flags]
    public enum MotorCtrEndTransCommands
    {
        StoreAction = 1 << 0,
        StartAfter = 1 << 1
    }

    public class MotorCtrEndTransAction : IBuffer
    {
        #region Getter
        public ushort Amount { get; private set; }
        public byte Command { get; private set; }
        public ushort Delay { get; private set; }
        #endregion

        #region Constructor
        public MotorCtrEndTransAction(byte amount ,byte command, ushort delay)
        {
            this.Amount = amount;
            this.Command = command;
            this.Delay = delay;
        }
        #endregion

        #region Public Method
        public  byte[] GetBytes()
        {
            byte[] data = null;
            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {                
                writer.Write(this.Amount);
                writer.Write(this.Command);
                writer.Write(this.Delay);
                data = stream.ToArray();
            }

            return data;
        }
        #endregion
    }
}
