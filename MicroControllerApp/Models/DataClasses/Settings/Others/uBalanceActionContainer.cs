﻿using MicroControllerApp.Models.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using LongModel.Collections;
using MicroControllerApp.Interfaces;
using LongInterface.ViewModels.Logics;

namespace MicroControllerApp.Models.DataClasses.Settings.Others
{
    public class uBalanceActionContainer : ManualObservableCollection<uBalanceChannelAction>, IDataEditor
    {
        #region Getter, setter
        public ICommand AddAction { get; set; }
        public ICommand EditAction { get; set; }
        public ICommand RemoveAction { get; set; }
        public ICommand UpAction { get; set; }
        public ICommand DownAction { get; set; }
        #endregion

        #region Getter
        public string Text { get; private set; }
        public uBalanceChannelInfoBits Channel { get; private set; }
        #endregion

        #region Constructor
        public uBalanceActionContainer(string text, uBalanceChannelInfoBits channel)
        {
            this.Text = text;
            this.Channel = channel;
        }
        #endregion                
    }
}
