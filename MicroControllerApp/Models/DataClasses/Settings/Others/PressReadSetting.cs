﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Models.DataClasses.Settings.Others
{
    public class PressReadSetting : PressAction
    {
        #region Getter
        public byte Index { get; set; }
        #endregion

        #region Constructor
        public PressReadSetting(byte index)
            : base((byte)PressTypes.ReadSetting)
        {
            this.Index = index;
        }
        #endregion

        #region Public Method
        public override byte[] GetBytes()
        {
            return new byte[] { this.Index };
        }
        #endregion
    }
}
