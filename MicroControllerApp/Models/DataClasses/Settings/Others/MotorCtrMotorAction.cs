﻿using LongInterface.Models;
using LongModel.BaseClasses;
using MicroControllerApp.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MicroControllerApp.Models.DataClasses.Settings.Others
{
    public enum MotorCtrTypes
    {
        GetStatus = 0,
        StartTrans = 1,
        Action = 2,
        EndTrans = 3,
        StartOper = 4,
        StopOper = 5,
        RealAction = 6,
        ToggleSample = 7,
        DataSample = 8,
        ReadSetting = 9,
        WriteSetting = 10,
        ReadPwm = 11,
        WritePwm = 12,
        RealBothAction = 13
    }

    [Flags]
    public enum MotorControl1Flags
    {
        Motor1 = 1 << 0,
        Motor2 = 1 << 1,
        Trigger = 1 << 2,
        Delay = 1 << 3,
        InvertDir = 1 << 4,
        HasDelay = 1 << 5
    }

    [Flags]
    public enum MotorControl2Flags
    {
        HasIncrement = 1 << 0,
        HasBackward = 1 << 1,
        AfterPackage = 1 << 2,
        HasPwm = 1 << 3,
        RunForever = 1 << 4
    }

    public class MotorCtrMotorAction : ObservableObject, IBuffer, ICalculateProperty
    {
        #region Const
        public const int BestPeriod = 985;
        public const int ResonancePeriod = 975;
        public const int ResonanceFrequency = 32;
        public const int PackageSize = 14;
        public const string FileExtension = ".motact";
        public static string FileFilter
        {
            get
            {
                return string.Format("MotorCtr Action File(*{0})|*{0}", FileExtension);
            }
        }
        #endregion

        #region View getter, setter
        [XmlIgnore]
        public string TypeInfo { get; private set; }
        [XmlIgnore]
        public string DurationInfo { get; private set; }
        [XmlIgnore]
        public string FrequencyInfo { get; private set; }         
        [XmlIgnore]
        public object ActionCollection { get; set; }
        [XmlIgnore]
        public object BackwardCollection { get; set; }
        [XmlIgnore]
        public object ShortRule { get; set; }
        [XmlIgnore]
        public object UshortRule { get; set; }
        [XmlIgnore]
        public object NonZeroUshortRule { get; set; }
        #endregion

        #region Model getter, setter
        private bool canInvert;
        private bool canDelay;
        private bool canIncrement;
        private bool canBackward;
        private bool hasPwm;
        private bool canRunForever;
        
        private byte actionType;
        private byte backwardType;
        private ushort amount;
        private ushort delayAmount;
        private ushort triggerAmount;       
        private ushort pwmPeriod;
        private short increment;

        [XmlAttribute("MotorType")]
        public byte MotorType { get; set; }
        [XmlAttribute("CanInvert")]
        public bool CanInvert
        {
            get { return this.canInvert; }
            set
            {
                this.SetProperty(ref this.canInvert, value);
            }
        }
        [XmlAttribute("CanDelay")]
        public bool CanDelay
        {
            get { return this.canDelay; }
            set
            {
                this.SetProperty(ref this.canDelay, value);
            }
        }
        [XmlAttribute("CanIncrement")]
        public bool CanIncrement
        {
            get { return this.canIncrement; }
            set
            {
                this.SetProperty(ref this.canIncrement, value);
            }
        }
        [XmlAttribute("CanReverse")]
        public bool CanBackward
        {
            get { return this.canBackward; }
            set
            {
                this.SetProperty(ref this.canBackward, value);
            }
        }
        [XmlAttribute("HasPwm")]
        public bool HasPwm
        {
            get { return this.hasPwm; }
            set
            {
                this.SetProperty(ref this.hasPwm, value);
            }
        }
        [XmlAttribute("CanRunForever")]
        public bool CanRunForever
        {
            get { return this.canRunForever; }
            set
            {
                this.SetProperty(ref this.canRunForever, value);
            }
        }
        [XmlAttribute("ActionType")]
        public byte ActionType
        {
            get { return this.actionType; }
            set
            {
                this.SetProperty(ref this.actionType, value);
            }
        }
        [XmlAttribute("BackwardType")]
        public byte BackwardType
        {
            get { return this.backwardType; }
            set
            {
                this.SetProperty(ref this.backwardType, value);
            }
        }
        [XmlAttribute("Amount")]
        public ushort Amount
        {
            get { return this.amount; }
            set
            {
                this.SetProperty(ref this.amount, value);
            }
        }
        [XmlAttribute("DelayAmount")]
        public ushort DelayAmount
        {
            get { return this.delayAmount; }
            set
            {
                this.SetProperty(ref this.delayAmount, value);
            }
        }
        [XmlAttribute("TriggerAmount")]
        public ushort TriggerAmount
        {
            get { return this.triggerAmount; }
            set
            {
                this.SetProperty(ref this.triggerAmount, value);
            }
        }       
        [XmlAttribute("PwmPeriod")]
        public ushort PwmPeriod
        {
            get { return this.pwmPeriod; }
            set
            {
                this.SetProperty(ref this.pwmPeriod, value);
            }
        }
        [XmlAttribute("Increment")]
        public short Increment
        {
            get { return this.increment; }
            set
            {
                this.SetProperty(ref this.increment, value);
            }
        }        
        #endregion

        #region Constructor
        public MotorCtrMotorAction()
        {
            this.MotorType = (byte)MotorControl1Flags.Motor1;
            this.ActionType = (byte)MotorControl1Flags.Trigger;
            //After pulse
            this.backwardType = 0;  
            //disable all extra feature
            this.canInvert = this.canIncrement = this.canBackward = this.canIncrement = this.hasPwm = this.canRunForever = false;
            this.amount = 10;
            this.delayAmount = 100;
            this.triggerAmount = 100;
            this.increment = 10;
            this.pwmPeriod = BestPeriod;
        }
        #endregion

        #region Public Method
        public byte[] GetBytes()
        {
            byte control1 = (byte)(this.MotorType | this.actionType);
            byte control2 = 0;

            if (canInvert)
            {
                control1 |= (byte)MotorControl1Flags.InvertDir;
            }
            if (canDelay)
            {
                control1 |= (byte)MotorControl1Flags.HasDelay;
            }

            if (canIncrement)
            {
                control2 |= (byte)MotorControl2Flags.HasIncrement;
            }
            if (canBackward)
            {
                control2 |= (byte)MotorControl2Flags.HasBackward;
                control2 |= this.backwardType;
            }
            if (hasPwm)
            {
                control2 |= (byte)MotorControl2Flags.HasPwm;
            }
            if (this.canRunForever)
            {
                control2 |= (byte)MotorControl2Flags.RunForever;
            }

            byte[] data = null;
            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write(control1);
                writer.Write(control2);                
                writer.Write(this.Amount);
                writer.Write(this.DelayAmount);
                writer.Write(this.TriggerAmount);                
                writer.Write(this.PwmPeriod);
                writer.Write(this.Increment);
                writer.Write(ushort.MinValue);
                data = stream.ToArray();
            }
            return data;
        }

        public static byte[] GetEmptyBytes()
        {
            return new byte[PackageSize];
        }

        public void CalculateProperty()
        {
            double duration = 0;
            double pulseDuration = 1 / (double)ResonanceFrequency;

            if (this.actionType != (byte)MotorControl1Flags.Trigger)
            {
                this.TypeInfo = "Delay";
                this.FrequencyInfo = "N.A";
                duration += this.delayAmount;
            }
            else
            {
                this.TypeInfo = "Pulse Trigger";
                this.FrequencyInfo = ResonanceFrequency.ToString(App.NumberFormat) + " kHz";
                if (hasPwm)
                {
                    //Get Frequency
                    double tmpFrequency = ResonancePeriod * ResonanceFrequency / (double)this.pwmPeriod;
                    this.FrequencyInfo = tmpFrequency.ToString(App.NumberFormat) + " kHz";
                    //Pulse duration
                    pulseDuration = 1 / tmpFrequency;
                }
                duration += this.triggerAmount * pulseDuration;
                if (canDelay)
                {
                    duration += this.delayAmount;
                }
            }

            //Total duration for n time
            duration *= this.amount;
            //only for trigger with Invert
            if (this.canBackward && this.actionType == (byte)MotorControl1Flags.Trigger)
            {
                duration *= 2;
            }

            //Improve View
            if (duration >= 1000)
            {
                duration /= 1000.0;
                this.DurationInfo = duration.ToString(App.NumberFormat) + " s";
            }
            else
            {
                this.DurationInfo = duration.ToString(App.NumberFormat) + " ms";
            }

            OnPropertyChanged("TypeInfo");
            OnPropertyChanged("FrequencyInfo");
            OnPropertyChanged("DurationInfo");
        }
        #endregion
    }
}
