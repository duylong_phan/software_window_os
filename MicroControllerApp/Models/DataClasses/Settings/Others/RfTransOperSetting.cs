﻿using LongModel.BaseClasses;
using MicroControllerApp.Interfaces;
using MicroControllerApp.Models.Base;
using MicroControllerApp.Models.DataClasses.Packages;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MicroControllerApp.Models.DataClasses.Settings.Others
{
    public class RfTransOperSetting : RfTransAction
    {
        #region Getter
        private byte interval;
        private ushort duration;

        [XmlAttribute("Interval")]
        public byte Interval
        {
            get { return this.interval; }
            set
            {
                this.SetProperty(ref this.interval, value);
            }
        }
        [XmlAttribute("Duration")]
        public ushort Duration
        {
            get { return this.duration; }
            set
            {
                this.SetProperty(ref this.duration, value);
            }
        }
        #endregion

        #region Getter, Setter       
        [XmlIgnore]
        public object DurationRule { get; set; }
        #endregion

        #region Constructor
        public RfTransOperSetting()
            : base((byte)RfTransStatus.WriteSetting)
        {
            this.interval = 10;
            this.duration = 60;
        }
        #endregion

        #region Public Method
        /// <summary>
        /// Get Buffer to transmit to Slave over RF
        /// </summary>
        /// <returns></returns>
        public override byte[] GetBytes()
        {
            byte[] data = null;
            using (var stream = new MemoryStream())
                using (var writer = new BinaryWriter(stream))
            {
                writer.Write(this.Action);
                writer.Write(this.interval);
                writer.Write(this.duration);
                data = stream.ToArray();
            }            
            return data;
        }
        #endregion
    }
}
