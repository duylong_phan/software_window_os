﻿using LongInterface.Models;
using LongModel.BaseClasses;
using MicroControllerApp.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace MicroControllerApp.Models.DataClasses.Settings.Others
{
    [Flags]
    public enum uBalanceChannelInfoBits
    {
        Channel1 = 1 << 0,
        Channel2 = 1 << 1,
        Channel3 = 1 << 2,
        Trigger = 1 << 3,
        Delay = 1 << 4
    }

    public class uBalanceChannelAction : ObservableObject, ICalculateProperty
    {
        #region Const
        public const int PackageLength = 10;
        public const int DurationUnit = 100;            //in us       
        #endregion

        #region Static
        public static byte[] GetEmptyBytes()
        {
            return new byte[PackageLength];
        }
        #endregion

        #region Getter, setter
        private uBalanceChannelInfoBits channel;
        private uBalanceChannelInfoBits action;
        private ushort length;
        private ushort amount;
        private byte width;
        private ushort index_5V;
        private ushort index_12V;
        private bool use5V;
        private bool use12V;
        
        [XmlAttribute("Channel")]
        public uBalanceChannelInfoBits Channel
        {
            get { return this.channel; }
            set
            {
                this.SetProperty(ref this.channel, value);
            }
        }
        [XmlAttribute("Action")]
        public uBalanceChannelInfoBits Action
        {
            get { return this.action; }
            set
            {
                this.SetProperty(ref this.action, value);
            }
        }
        [XmlAttribute("Length")]
        public ushort Length
        {
            get { return this.length; }
            set
            {
                this.SetProperty(ref this.length, value);
            }
        }
        [XmlAttribute("Amount")]
        public ushort Amount
        {
            get { return this.amount; }
            set
            {
                this.SetProperty(ref this.amount, value);
            }
        }
        [XmlAttribute("Width")]
        public byte Width
        {
            get { return this.width; }
            set
            {
                this.SetProperty(ref this.width, value);
            }
        }
        [XmlAttribute("Index_5V")]
        public ushort Index_5V
        {
            get { return this.index_5V; }
            set
            {
                this.SetProperty(ref this.index_5V, value);
            }
        }
        [XmlAttribute("Index_12V")]
        public ushort Index_12V
        {
            get { return this.index_12V; }
            set
            {
                this.SetProperty(ref this.index_12V, value);
            }
        }
        [XmlAttribute("Use5V")]
        public bool Use5V
        {
            get { return this.use5V; }
            set
            {
                this.SetProperty(ref this.use5V, value);
            }
        }
        [XmlAttribute("Use12V")]
        public bool Use12V
        {
            get { return this.use12V; }
            set
            {
                this.SetProperty(ref this.use12V, value);
            }
        }
        #endregion

        #region Getter
        [XmlIgnore]
        public double Duration { get; private set; }
        [XmlIgnore]
        public string Information { get; private set; }
        [XmlIgnore]
        public object ActionCollection { get; set; }
        [XmlIgnore]
        public object IndexRule { get; set; }
        [XmlIgnore]
        public object AmountRule { get; set; }
        [XmlIgnore]
        public object WidthRule { get; set; }
        #endregion

        #region Constructor
        public uBalanceChannelAction()
        {
            this.channel = uBalanceChannelInfoBits.Channel1;
            this.action = uBalanceChannelInfoBits.Trigger;
            this.length = 10;
            this.amount = 1;
            this.width = 4;
            this.index_5V = 0;
            this.index_12V = 0;
            this.use5V = false;
            this.use12V = true;
            this.Duration = 0;
        }
        #endregion

        #region Public Method
        public void CalculateProperty()
        {
            int duration = this.Length * DurationUnit * this.Amount;
            this.Duration = duration / 1000.0;

            //Request Update
            this.Information = string.Format("{0}: {1}ms", this.action, this.Duration);
            OnPropertyChanged("Information");
        }

        public byte[] GetBytes()
        {
            byte[] data = null;
            using (var stream = new MemoryStream())
            using (var writer = new BinaryWriter(stream))
            {
                writer.Write((byte)(this.Channel | this.Action));
                writer.Write(this.Width);

                if (this.Action == uBalanceChannelInfoBits.Delay)
                {
                    writer.Write(ushort.MaxValue);
                    writer.Write(ushort.MaxValue);
                }
                else
                {
                    if (this.use5V)
                    {
                        writer.Write(this.Index_5V);
                    }
                    else
                    {
                        writer.Write(ushort.MaxValue);
                    }
                    if (this.use12V)
                    {
                        writer.Write(this.Index_12V);
                    }
                    else
                    {
                        writer.Write(ushort.MaxValue);
                    }
                }               

                writer.Write(this.Length);
                writer.Write(this.Amount);
                data = stream.ToArray();
            }

            return data;
        }
        #endregion
    }
}
