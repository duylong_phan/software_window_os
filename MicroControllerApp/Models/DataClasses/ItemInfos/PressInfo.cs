﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace MicroControllerApp.Models.DataClasses.ItemInfos
{
    public class PressInfo
    {
        #region Const
        public const string InfoTag = "Press";
        public const string IdTag = "ID";
        public const string AmountTag = "Amount";
        public const string DateTimeTag = "DateTime";
        #endregion

        #region Getter, setter       
        public int ID { get; set; }        
        public int Amount { get; set; }       
        public DateTime DateTime { get; set; }

        public object Group { get; set; }
        #endregion

        #region Constructor        
        public PressInfo(int id, int amount, DateTime datetime)
        {
            this.ID = id;
            this.Amount = amount;
            this.DateTime = datetime;
        }
        #endregion

        #region Convert Method
        public override string ToString()
        {
            var element = new XElement(InfoTag);
            element.Add(new XAttribute(IdTag, this.ID));
            element.Add(new XAttribute(AmountTag, this.Amount));
            element.Add(new XAttribute(DateTimeTag, this.DateTime.ToString(App.DateTimeFormat)));

            return element.ToString(); 
        }

        public static PressInfo Parse(string text)
        {
            var element = XElement.Parse(text);
            int id = int.Parse(element.Attribute(IdTag).Value);
            int amount = int.Parse(element.Attribute(AmountTag).Value);
            DateTime datetime = DateTime.ParseExact(element.Attribute(DateTimeTag).Value, App.DateTimeFormat, App.CultureFormat);

            return new PressInfo(id, amount, datetime);
        }
        #endregion
    }
}
