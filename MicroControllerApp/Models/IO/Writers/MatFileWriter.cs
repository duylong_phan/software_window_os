﻿using LongModel.ViewModels.Logics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using csmatio.types;
using System.IO;

namespace MicroControllerApp.Models.IO.Writers
{
    public class MatFileWriter : SingleWorker
    {
        public MatFileWriter(string filePath, string name, Queue<double[]> queue)
        {
            SetAction(() =>
            {
                #region Get Buffer
                int index = 0;
                double[][] buffer = new double[queue.Count][];
                while (true)
                {
                    if (queue.Count == 0)
                    {
                        break;
                    }
                    buffer[index] = queue.Dequeue();
                    index++;
                }

                if (index != buffer.Length)
                {
                    Array.Resize(ref buffer, index);
                }
                #endregion

                #region Write
                MLDouble wrapper = new MLDouble(name, buffer);
                List<MLArray> collection = new List<MLArray>() { wrapper };

                var directory = Path.GetDirectoryName(filePath);
                if (!Directory.Exists(directory))   //NOT
                {
                    Directory.CreateDirectory(directory);
                }

                var writer = new csmatio.io.MatFileWriter(filePath, collection, false);
                #endregion
            });            
        }
    }
}
