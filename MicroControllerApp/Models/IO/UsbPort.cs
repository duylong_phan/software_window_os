﻿using CyUSB;
using LongInterface.Models.IO.Files;
using MicroControllerApp.Interfaces;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Models.IO
{
    public class UsbDataReceivedEventArgs : EventArgs
    {

    }

    public delegate void UsbDataReceivedEventHandler(object sender, UsbDataReceivedEventArgs e);

    public class UsbPort : IDisposable, IUsbSetting
    {
        #region Const
        public const int DefaultVendorID = 0x4B4;
        public const int DefaultProductID = 0x4720;
        #endregion

        #region Getter, setter
        public int VendorID { get; set; }
        public int ProductID { get; set; }
        #endregion

        #region Getter
        public bool IsOpen { get; private set; }
        public int BytesToRead
        {
            get
            {
                int amount = 0;
                if (this.dataQueue != null)
                {
                    amount = dataQueue.Count;
                }
                return amount;
            }
        }
        public uint InTimeout { get; private set; }
        public uint OutTimeout { get; private set; }
        #endregion

        #region Event
        public event UsbDataReceivedEventHandler DataReceived;

        protected void OnDataReceived(UsbDataReceivedEventArgs e)
        {
            UsbDataReceivedEventHandler handler = DataReceived;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        #endregion

        #region Field
        private byte[] tmpBuffer;
        private int lengthIndex;
        private Queue<byte> dataQueue;
        private USBDeviceList usbCollection;
        private CyUSBDevice usbDevice;
        private CyBulkEndPoint inEP = null;
        private CyBulkEndPoint outEP = null;

        private System.Threading.CancellationTokenSource cancelSource;
        #endregion

        #region Constructor
        public UsbPort()
            : this(DefaultVendorID, DefaultProductID)
        {

        }

        public UsbPort(int vendorID, int productID)
            : this(vendorID, productID, 5, 5, 64)
        {

        }

        public UsbPort(int vendorID, int productID, uint inTimeout, uint outTimeout, int inLength)
        {
            this.VendorID = vendorID;
            this.ProductID = productID;
            this.InTimeout = inTimeout;
            this.OutTimeout = outTimeout;
            this.tmpBuffer = new byte[inLength];
            this.lengthIndex = inLength - 1;
            this.dataQueue = new Queue<byte>(4096);
            this.usbCollection = null;
            this.usbDevice = null;
            this.inEP = null;
            this.outEP = null;
        }
        #endregion

        #region Public Method
        public void Open()
        {
            USBDeviceList usbList = new USBDeviceList(CyConst.DEVICES_CYUSB);
            Open(usbList);
        }

        public void Open(USBDeviceList usbList)
        {
            if (this.IsOpen)
            {
                throw new Exception("USB device has been connected");
            }

            #region Check Device
            this.usbCollection = usbList;
            this.usbDevice = this.usbCollection[this.VendorID, this.ProductID] as CyUSBDevice;
            if (this.usbDevice == null)
            {
                throw new Exception("No USB device with given information can be found");
            }
            #endregion

            #region Check EndPoint
            foreach (var item in this.usbDevice.EndPoints)
            {
                if (item.Attributes != 2)
                {
                    //skip
                    continue;
                }

                if (item.bIn)
                {
                    this.inEP = item as CyBulkEndPoint;
                }
                else
                {
                    this.outEP = item as CyBulkEndPoint;
                }
            }

            if (this.inEP == null || this.outEP == null)
            {
                throw new Exception("In or Out Endpoint was not implemented");
            }

            //Timeout for write and read operation
            this.inEP.TimeOut = this.InTimeout;
            this.outEP.TimeOut = this.OutTimeout;
            #endregion

            #region Check Incoming Data
            this.cancelSource = new System.Threading.CancellationTokenSource();
            Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    if (this.cancelSource.IsCancellationRequested)
                    {
                        this.IsOpen = false;
                        this.cancelSource.Dispose();
                        this.cancelSource = null;
                        break;
                    }

                    CheckIncomingData();
                }
            });
            #endregion

            this.IsOpen = true;
        }

        public void Close()
        {
            if (this.IsOpen && this.cancelSource != null)
            {
                this.cancelSource.Cancel();
                //make sure BackgroundWorker is stoped properly
                Task.Delay(200).Wait();
            }
        }

        public void Dispose()
        {
            //Connection
            if (this.IsOpen)
            {
                this.Close();
            }

            //HW
            if (this.usbCollection != null)
            {
                this.usbCollection.Dispose();
                //make sure USB is stoped properly
                Task.Delay(50).Wait();
                this.usbCollection = null;
                this.usbDevice = null;
            }

            //buffer
            this.tmpBuffer = null;
            while (this.dataQueue.Count > 0)
            {
                this.dataQueue.Dequeue();
            }
        }

        public void Write(byte[] data, int offset, int length)
        {
            byte[] tmpData = new byte[length];
            Buffer.BlockCopy(data, offset, tmpData, 0, length);
            this.outEP.XferData(ref tmpData, ref length);

            if (length <= 0)
            {
                throw new Exception("Write package error");
            }
        }

        public void Read(byte[] data, int offset, int length)
        {
            if (length > this.dataQueue.Count)
            {
                throw new Exception("Invalid Length");
            }

            length += offset;
            for (int i = offset; i < length; i++)
            {
                data[i] = this.dataQueue.Dequeue();
            }
        }
        #endregion

        #region Private Method
        private void CheckIncomingData()
        {
            //available length of the buffer
            int length = this.tmpBuffer.Length;
            try
            {
                this.inEP.XferData(ref this.tmpBuffer, ref length);
            }
            catch (Exception) { }   //Ignore

            //collected amount
            if (length > 0)
            {
                length = this.tmpBuffer[this.lengthIndex];
                for (int i = 0; i < length; i++)
                {
                    this.dataQueue.Enqueue(this.tmpBuffer[i]);
                }
                //synchronous due to Queue
                this.OnDataReceived(new UsbDataReceivedEventArgs());
            }
        }
        #endregion
    }
}
