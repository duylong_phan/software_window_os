﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LongModel.Models.IO.Files;
using MicroControllerApp.Models.IO.Writers;
using LongModel.ViewModels.Logics;
using System.Windows.Threading;
using MicroControllerApp.Models.Base;

namespace MicroControllerApp.Models.IO.Handlers
{
    public class MatFileHandler : FileBaseHandler<FileSingleSetting, double[]>
    {
        #region Field  
        private PortableTimer timer;
        private Action<string> writerStoped;
        private Action<Exception> writerHasError;
        #endregion

        #region Constructor
        public MatFileHandler(FileSingleSetting setting, Action<string> writerStoped, Action<Exception> writerHasError)
            : base(setting)
        {
            this.writerStoped = writerStoped;
            this.writerHasError = writerHasError;
        }
        #endregion

        #region Public Method
        public override void Start()
        {
            if (this.IsEnabled)
            {
                return;
            }

            this.IsEnabled = true;
            if (this.Setting.UseDuration)
            {
                this.timer = new PortableTimer(this.Setting.Duration * 1000);
                this.timer.Ticked += Timer_Ticked;
                this.timer.Start();
            }
        }        

        public override void Stop()
        {
            if (this.IsEnabled)
            {
                //Use Duration => dispose Timer
                if (this.Setting.UseDuration && this.timer != null)
                {
                    this.timer.Stop();
                }

                //Write remain Data
                this.IsEnabled = false;
                WriteFile();
            }
        }

        public override void Add(double[] data)
        {
            if (data == null)
            {
                return;
            }

            this.dataQueue.Enqueue(data);
            this.SampleAmount++;
            if (this.dataQueue.Count >= this.Setting.SampleAmount)
            {
                WriteFile();
            }
        }
        
        #endregion

        #region Private Method
        protected override void WriteFile()
        {
            //No Data is available
            if (this.dataQueue.Count == 0)
            {
                return;
            }

            #region Get FilePath
            string filePath = string.Empty;
            string name = string.Empty;
            if (this.Setting.UseIndex)
            {
                name = string.Format("{0}_{1}", this.Setting.FileName, this.Setting.Index);
                this.Setting.Index++;
            }
            else
            {
                name = this.Setting.FileName;
            }
            filePath = string.Format("{0}\\{1}{2}", this.Setting.Directory, name, this.Setting.FileExtension);
            #endregion

            #region Initialze, start Writer                
            var writer = new MatFileWriter(filePath, name, this.dataQueue);
            writer.HasError += (_sender, _e) =>
            {
                this.writerHasError(_e.Error);
            };
            writer.Stoped += (_sender, _e) =>
            {
                this.writerStoped(filePath);
            };
            writer.Start();
            #endregion

            //new Queue for incomming Data
            this.dataQueue = new Queue<double[]>();
        }

        private void Timer_Ticked(object sender, TickedEventArgs e)
        {
            //Dispose
            var timer = sender as PortableTimer;
            if (timer != null)
            {
                timer.Stop();
            }

            //Stop Handler
            Stop();
        }
        #endregion
    }
}
