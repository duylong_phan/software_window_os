﻿using MicroControllerApp.Models.Base;
using MicroControllerApp.Models.DataClasses.ItemInfos;
using MicroControllerApp.Models.DataClasses.Settings.Sub;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LongModel.ViewModels.Logics;
using MicroControllerApp.Models.DataClasses.Settings;

namespace MicroControllerApp.Models.IO.Handlers
{
    public class PressFileHandler : FileBaseHandler<PressFileSetting, PressInfo>
    {
        #region Static
        public static List<PressInfo> Load(string filePath)
        {
            using (var stream = File.OpenRead(filePath))
            {
                return Load(stream);
            } 
        }

        public static List<PressInfo> Load(Stream stream)
        {
            var collection = new List<PressInfo>();

            return collection;
        }
        #endregion

        #region Field
        private PortableTimer timer;
        #endregion

        #region Constructor
        public PressFileHandler(PressFileSetting setting)
            : base(setting)
        {

        }
        #endregion

        #region Public Method
        public override void Add(PressInfo data)
        {
            if (data != null)
            {
                this.dataQueue.Enqueue(data);
            }            
        }

        public override void Start()
        {
            if (this.IsEnabled)
            {
                return;
            }

            this.IsEnabled = true;
            this.timer = new PortableTimer(this.Setting.Duration * 1000);
            this.timer.Ticked += Timer_Ticked;
            this.timer.Start();
        }

        public override void Stop()
        {
            if (this.IsEnabled)
            {
                if (this.timer != null)
                {
                    this.timer.Stop();
                    this.timer = null;
                }
                WriteFile();
            }
        }

        public void WriteFile(Stream stream, List<PressInfo> collection)
        {
            using (var writer = new StreamWriter(stream))
            {
                foreach (var item in collection)
                {
                    writer.WriteLine(item.ToString());
                }
            }
        }
        #endregion

        #region Private Method
        protected async override void WriteFile()
        {
            if (this.dataQueue.Count == 0)
            {
                return;
            }

            var tmp = this.dataQueue;
            this.dataQueue = new Queue<PressInfo>();
            await Task.Factory.StartNew(() =>
            {
                var collection = new List<PressInfo>();
                while (tmp.Count > 0)
                {
                    var info = tmp.Dequeue();
                    if (info != null)
                    {
                        collection.Add(info);
                    }
                }

                if (!Directory.Exists(this.Setting.Directory))  //NOT
                {
                    Directory.CreateDirectory(this.Setting.Directory);
                }

                string filePath = string.Format("{0}/{1}{2}", this.Setting.Directory, 
                                                              DateTime.Now.ToString(PressSetting.PressNameFormat, CultureInfo.InvariantCulture), 
                                                              PressSetting.PressExtension);
                using (var stream = (File.Exists(filePath) ? File.Open(filePath, FileMode.Append) : File.Create(filePath)))                
                {
                    WriteFile(stream, collection);
                }
            });
        }

        private void Timer_Ticked(object sender, TickedEventArgs e)
        {
            WriteFile();
        }
        #endregion
    }
}
