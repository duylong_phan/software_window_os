﻿using MicroControllerApp.Models.ViewClasses.Infos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace MicroControllerApp.Models.ViewClasses.Selectors
{
    public class FileContentTemplateSelector : DataTemplateSelector
    {
        #region Getter, setter
        public DataTemplate Press { get; set; }
        #endregion

        #region Public Method
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            DataTemplate template = null;

            if (item is FileContentInfo)
            {
                var info = item as FileContentInfo;
                switch (info.Extension)
                {
                    case FileContentExtensions.press:
                        template = this.Press;
                        break;

                    default:
                        //Ignore
                        break;
                }
            }

            return template;
        }
        #endregion
    }
}
