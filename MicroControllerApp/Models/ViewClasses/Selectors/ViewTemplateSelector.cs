﻿using MicroControllerApp.Models.DataClasses.Settings;
using MicroControllerApp.Models.ViewClasses.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace MicroControllerApp.Models.ViewClasses.Selectors
{
    public class ViewTemplateSelector : DataTemplateSelector
    {
        #region Getter, setter
        public DataTemplate Graphic { get; set; }
        public DataTemplate OutPinCtr { get; set; }
        public DataTemplate InPinCtr { get; set; }
        public DataTemplate PressTable { get; set; }
        public DataTemplate PressLocalControl { get; set; }
        public DataTemplate PressRemoteControl { get; set; }
        public DataTemplate CapControl { get; set; }
        public DataTemplate AccControl { get; set; }
        public DataTemplate uBalaceEditor { get; set; }
        public DataTemplate uBalanceControl { get; set; }
        public DataTemplate RfTransControl { get; set; }
        public DataTemplate MotorCtrContol { get; set; }        
        public DataTemplate MotorCtrEditor { get; set; }
        #endregion

        #region Public Method
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            DataTemplate template = null;
            if (item is ViewOption)
            {
                var option = item as ViewOption;
                switch (option.Type)
                {
                    case AppViews.Graphic:                      template = this.Graphic;                    break;
                    case AppViews.OutPinCtr:                    template = this.OutPinCtr;                  break;
                    case AppViews.InPinCtr:                     template = this.InPinCtr;                   break;
                    case AppViews.PressTable:                   template = this.PressTable;                 break;
                    case AppViews.PressLocalControl:            template = this.PressLocalControl;          break;
                    case AppViews.PressRemoteControl:           template = this.PressRemoteControl;         break;
                    case AppViews.CapControl:                   template = this.CapControl;                 break;
                    case AppViews.uBalanceEditor:               template = this.uBalaceEditor;              break;
                    case AppViews.uBalanceControl:              template = this.uBalanceControl;            break;
                    case AppViews.RfTransControl:               template = this.RfTransControl;             break;
                    case AppViews.MotorCtrContol:               template = this.MotorCtrContol;             break;
                    case AppViews.MotorCtrEditor:               template = this.MotorCtrEditor;             break;
                    case AppViews.AccControl:                   template = this.AccControl;                 break;
                    case AppViews.Non:
                    default:                                    template = null;                            break;
                }
            }

            return template;
        }
        #endregion
    }
}
