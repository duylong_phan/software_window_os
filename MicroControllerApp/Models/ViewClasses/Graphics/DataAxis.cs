﻿using LongModel.Helpers;
using MicroControllerApp.Interfaces;
using OxyPlot.Axes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Models.ViewClasses.Graphics
{
    public class DataAxis : LinearAxis, IAxis
    {
        public DataAxis()
        {
            
        }

        public void CopyProperties(IAxis item)
        {
            if (item != null)
            {
                ModelHelper.CopyProperties(item, this);
            }
        }
    }
}
