﻿using LongModel.Helpers;
using MicroControllerApp.Interfaces;
using OxyPlot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Models.ViewClasses.Graphics
{
    public class DataPlotModel : PlotModel, IPlot
    {

        #region Public Method
        public void CopyProperties(IPlot item)
        {
            if (item != null)
            {
                ModelHelper.CopyProperties(item, this);
            }
        }
        #endregion
    }
}
