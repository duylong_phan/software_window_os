﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Models.ViewClasses.Infos
{
    public class DoubleMatVariableInfo
    {
        #region Getter
        public string Text { get; private set; }
        public double[][] Array { get; private set; }
        #endregion

        #region Constructor
        public DoubleMatVariableInfo(string text, double[][] array)
        {
            this.Text = text;
            this.Array = array;
        }
        #endregion
    }
}
