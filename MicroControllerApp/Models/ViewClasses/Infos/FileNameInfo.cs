﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Xml.Linq;

namespace MicroControllerApp.Models.ViewClasses.Infos
{
    public class FileNameInfo
    {
        #region Const
        public const string InfoTag = "File";
        public const string NameTag = "Name";
        public const string LengthTag = "Length";
        #endregion

        #region Getter, setter
        public string Name { get; set; }
        public long Length { get; set; }
        public ICommand Command { get; set; }
        #endregion

        #region Constructor
        public FileNameInfo()
            : this(string.Empty, 0)
        {

        }

        public FileNameInfo(string name, long length)
        {
            this.Name = name;
            this.Length = length;
        }
        #endregion

        #region Convert Method
        public override string ToString()
        {
            var element = new XElement(InfoTag);
            element.Add(new XAttribute(NameTag, this.Name));
            element.Add(new XAttribute(LengthTag, this.Length));

            return element.ToString();
        }

        public static FileNameInfo Parse(string text)
        {
            var element = XElement.Parse(text);
            var name = element.Attribute(NameTag).Value;
            var length = long.Parse(element.Attribute(LengthTag).Value);

            return new FileNameInfo(name, length);
        }
        #endregion
    }
}
