﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MicroControllerApp.Models.ViewClasses.Infos
{
    public class PressFileContentInfo : FileContentInfo
    {
        #region Getter, setter
        private object filteredContent;

        public object FilteredContent
        {
            get { return this.filteredContent; }
            set
            {
                this.SetProperty(ref this.filteredContent, value);
            }
        }
        #endregion

        #region Getter
        public PressFilterInfo Filter { get; private set; }
        #endregion

        #region Constructor
        public PressFileContentInfo(object header, object content, FileContentExtensions extension, ICommand command, IEnumerable functions, PressFilterInfo filter)
            : base(header, content, extension, command, functions)
        {
            this.Filter = filter;
        }
        #endregion
    }
}
