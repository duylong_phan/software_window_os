﻿using LongModel.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Models.ViewClasses.Infos
{
    public class PressFilterInfo : ObservableObject
    {
        #region Getter, setter
        private bool useId;
        private bool useTime;
        private DateTime startTime;
        private DateTime endTime;
        private int id;
        private int duration;
        private bool useDuration;

        public bool UseId
        {
            get { return this.useId; }
            set
            {
                this.SetProperty(ref this.useId, value);
            }
        }
        public bool UseTime
        {
            get { return this.useTime; }
            set
            {
                this.SetProperty(ref this.useTime, value);
            }
        }        
        public bool UseDuration
        {
            get { return this.useDuration; }
            set
            {
                this.SetProperty(ref this.useDuration, value);
            }
        }
        public DateTime StartTime
        {
            get { return this.startTime; }
            set
            {
                this.SetProperty(ref this.startTime, value);
            }
        }
        public DateTime EndTime
        {
            get { return this.endTime; }
            set
            {
                this.SetProperty(ref this.endTime, value);
            }
        }
        public int ID
        {
            get { return this.id; }
            set
            {
                this.SetProperty(ref this.id, value);
            }
        }
        public int Duration
        {
            get { return this.duration; }
            set
            {
                this.SetProperty(ref this.duration, value);
            }
        }

        public object IdRule { get; set; }
        public object DurationRule { get; set; }
        #endregion

        #region Constructor
        public PressFilterInfo(DateTime startTime, DateTime endTime)
        {
            this.useId = true;
            this.useDuration = true;
            this.useTime = true;
            this.id = 1;
            this.duration = 60;
            this.startTime = startTime;
            this.endTime = endTime;
        }
        #endregion
    }
}
