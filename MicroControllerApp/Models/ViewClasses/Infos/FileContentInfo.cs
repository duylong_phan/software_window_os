﻿using LongModel.Views.ControlModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MicroControllerApp.Models.ViewClasses.Infos
{
    public enum FileContentExtensions
    {
        press
    }


    public class FileContentInfo : HeaderItemInfo
    {
        #region Getter
        public FileContentExtensions Extension { get; private set; }
        public IEnumerable Functions { get; private set; }
        #endregion

        #region Constructor
        public FileContentInfo(object header, object content, FileContentExtensions extension, ICommand command, IEnumerable functions)
            : base(header, content, command)
        {
            this.Extension = extension;
            this.Functions = functions;
        }
        #endregion
    }
}
