﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using LongWpfUI.Models.ValidationRules.Base;

namespace MicroControllerApp.Models.ViewClasses.ValidationRules
{
    public class RfAddressRule : RuleBase
    {
        #region Const
        public const int AddressSize = 5;
        public const char Separator = '-';
        public const string NumberFormat = "X2";
        #endregion

        #region Static
        public static string GetAddressText(byte[] buffer, int index)
        {
            var builder = new StringBuilder();

            for (int i = 0; i < AddressSize; i++)
            {
                if (i != 0)
                {
                    builder.Append(Separator);
                }
                builder.Append(buffer[i + index].ToString(NumberFormat));
            }

            return builder.ToString();
        }

        public static byte[] GetAddressBytes(string text)
        {
            var buffer = new byte[AddressSize];
            var elements = text.Split(Separator);

            for (int i = 0; i < AddressSize; i++)
            {
                buffer[i] = Convert.ToByte(elements[i], 16);
            }

            return buffer;
        }

        #endregion


        #region Constructor
        public RfAddressRule()
        {
            this.InitialMessage = "Invalid address. Format: XX-XX-XX-XX-XX";
            this.InvalidMessage = "Invalid Hexa Number, range [00;FF]";
        }
        #endregion

        #region Public Method
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            ValidationResult result = new ValidationResult(false, this.InitialMessage);

            if (value is string)
            {
                var input = value as string;
                var elements = input.Split(Separator);
                if (elements.Length == AddressSize)
                {
                    try
                    {
                        foreach (var item in elements)
                        {
                            Convert.ToByte(item, 16);
                        }
                        result = new ValidationResult(true, string.Empty);
                    }
                    catch (Exception)
                    {
                        result = new ValidationResult(false, this.InvalidMessage);
                    }
                }                
            }

            return result;
        }
        #endregion
    }
}
