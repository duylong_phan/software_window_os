﻿using LongInterface.Models.IO.Files;
using LongModel.Views.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Models.ViewClasses.Options
{
    public class FileChangedResponseOption : OptionBase<FileChangedResponses>
    {
        public FileChangedResponseOption(string text, FileChangedResponses type)
            : base(text, type)
        {

        }
    }
}
