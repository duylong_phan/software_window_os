﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Models.ViewClasses.Options
{
    public class LocalSetting : List<PhysicalPortOption>, INotifyPropertyChanged
    {
        #region Event
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region Getter, setter
        private object server;

        public object Server
        {
            get { return this.server; }
            set
            {
                this.server = value;
                OnPropertyChanged("Server");
            }
        }
        #endregion

        #region Constructor
        public LocalSetting()
        {

        }
        #endregion
    }
}
