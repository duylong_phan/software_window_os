﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LongModel.Views.Options;
using MicroControllerApp.Models.DataClasses.Settings;

namespace MicroControllerApp.Models.ViewClasses.Options
{
    public class AppPositionOption : OptionContentBase<AppPositions>
    {
        public AppPositionOption(string text, AppPositions type)
            : base(text, type)
        {

        }
    }
}
