﻿using LongModel.Views.Options;
using MicroControllerApp.Models.DataClasses.Settings.Others;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Models.ViewClasses.Options
{
    public class uBalanceBitOption : OptionBase<uBalanceChannelInfoBits>
    {
        public uBalanceBitOption(string text, uBalanceChannelInfoBits type)
            : base(text, type)
        {

        }
    }
}
