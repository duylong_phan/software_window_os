﻿using LongModel.Views.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Models.ViewClasses.Options
{
    public class MotorCtrActionOption : OptionBase<byte>
    {
        public MotorCtrActionOption(string text, byte type)
            : base(text, type)
        {

        }
    }
}
