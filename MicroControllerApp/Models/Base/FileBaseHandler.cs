﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Models.Base
{
    public abstract class FileBaseHandler<TSetting, TData>
    {
        #region Getter
        public TSetting Setting { get; private set; }
        public int SampleAmount { get; protected set; }
        public bool IsEnabled { get; protected set; }
        #endregion

        #region Field
        protected Queue<TData> dataQueue;
        #endregion

        #region Constructor
        public FileBaseHandler(TSetting setting)
        {
            this.Setting = setting;
            this.SampleAmount = 0;
            this.IsEnabled = false;
            this.dataQueue = new Queue<TData>();
        }
        #endregion

        #region Abstract Method
        public abstract void Start();
        public abstract void Stop();
        public abstract void Add(TData data);
        protected abstract void WriteFile();
        #endregion

        #region Public Method
        public void ClearSampleAmount()
        {
            this.SampleAmount = 0;
        }
        #endregion
    }
}
