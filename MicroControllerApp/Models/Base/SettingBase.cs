﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using LongModel.Models.IO.Files;
using MicroControllerApp.Models.DataClasses.Settings.Sub;
using MicroControllerApp.Models.DataClasses.Settings;
using LongModel.BaseClasses;
using System.Net;
using MicroControllerApp.Models.DataClasses.Packages;

namespace MicroControllerApp.Models.Base
{
    public enum ErrorNoticicationTypes
    {
        AppStatus,
        MessageBox
    }

    public abstract class SettingBase : ObservableObject
    {
        #region Getter, Setter     
        private ErrorNoticicationTypes notificationType;
        private AppPositions position;
        private PhysicalPorts port;

        
        [XmlAttribute("NotificationType")]
        public ErrorNoticicationTypes NotificationType
        {
            get { return this.notificationType; }
            set
            {
                this.SetProperty(ref this.notificationType, value);
            }
        }
        [XmlAttribute("TaskID")]
        public MicroTasks TaskID { get; set; }       
        [XmlAttribute("Position")]        
        public AppPositions Position
        {
            get { return this.position; }
            set
            {
                this.SetProperty(ref this.position, value);
            }
        }
        [XmlAttribute("Port")]
        public PhysicalPorts Port
        {
            get { return this.port; }
            set
            {
                this.SetProperty(ref this.port, value);
            }
        }
        [XmlAttribute("LeftView")]
        public AppViews LeftView { get; set; }
        [XmlAttribute("RightView")]
        public AppViews RightView { get; set; }
        [XmlElement("UsbSetting")]
        public UsbSetting UsbSetting { get; set; }
        [XmlElement("SerialPortSetting")]
        public SerialPortSetting SerialPortSetting { get; set; }
        [XmlElement("RemoteSetting")]
        public RemoteSetting RemoteSetting { get; set; }
        [XmlElement("ServerSetting")]
        public ServerSetting ServerSetting { get; set; }        
        #endregion

        #region Field
        private Action initializeSW;
        private Action initializeHW;
        private Action update;
        private Action disposeHW;
        private Action disposeSW;
        private Action<HttpListenerRequest, HttpListenerResponse> onRequested;
        private Action<string, byte[]> onResponse;
        #endregion

        #region Constructor
        public SettingBase(MicroTasks taskID, RemoteSetting remoteSetting, ServerSetting serverSetting, 
                           ErrorNoticicationTypes notificationType = ErrorNoticicationTypes.MessageBox)
        {
            this.TaskID = taskID;
            this.notificationType = notificationType;
            this.position = AppPositions.Local;
            this.port = PhysicalPorts.Serial;
            this.LeftView = AppViews.Non;
            this.RightView = AppViews.Non;
            this.UsbSetting = new UsbSetting();
            this.SerialPortSetting = new SerialPortSetting(string.Empty, 115200, 8, 0, 1, 1, false, false, false);
            this.RemoteSetting = remoteSetting;
            this.ServerSetting = serverSetting;            
        }
        #endregion

        #region Public Method
        public abstract DataPackage ParseBuffer(byte[] buffer);

        public DataPackage ParseSharedBuffer(byte[] buffer)
        {
            DataPackage package = null;

            switch (buffer[0])
            {
                case (byte)PcCommands.Command_Done:
                    package = new CommandDonePackage(buffer[0], buffer);
                    break;

                case (byte)PcCommands.Read_MasterSetting:
                    package = new MasterSettingPackage(buffer[0], buffer);
                    break;

                case (byte)PcCommands.Read_SlaveSetting:
                    package = new SubSlaveSettingPackage(buffer[0], buffer);
                    break;

                default:
                    //ignore
                    break;
            }

            return package;
        }

        public void SetLocalAction(Action initializeSW, Action initializeHW, Action update, Action disposeSW, Action disposeHW)
        {
            this.initializeSW = initializeSW;
            this.initializeHW = initializeHW;
            this.update = update;
            this.disposeSW = disposeSW;
            this.disposeHW = disposeHW;
        }

        public void SetRemoteAction(Action<HttpListenerRequest, HttpListenerResponse> onRequested, Action<string, byte[]> onResponse)
        {
            this.onRequested = onRequested;
            this.onResponse = onResponse;
        }

        /// <summary>
        /// Initialize SW in UI Thread
        /// </summary>
        public void InitializeSW()
        {
            if (this.initializeSW != null)
            {
                this.initializeSW();
            }
        }

        /// <summary>
        /// Initialize HW in Background Thread
        /// </summary>
        /// <returns></returns>
        public Task InitializeHW()
        {
            return Task.Factory.StartNew(() =>
            {
                if (this.initializeHW != null)
                {
                    this.initializeHW();
                }
            });
        }

        /// <summary>
        /// Update Task Data in UI Thread
        /// </summary>
        public void Update()
        {
            if (this.update != null)
            {
                this.update();
            }
        }

        /// <summary>
        /// Dispose SW resource in UI Thread
        /// </summary>
        public void DisposeSW()
        {
            if (this.disposeSW != null)
            {
                this.disposeSW();
            }
        }

        /// <summary>
        /// Dispose SW resource in Background Thread
        /// </summary>
        public Task DisposeHW()
        {
            return Task.Factory.StartNew(() =>
            {
                if (this.disposeHW != null)
                {
                    this.disposeHW();
                }
            });
        }

        /// <summary>
        /// When a request is available, On Server side
        /// </summary>
        public void OnRequested(HttpListenerRequest request, HttpListenerResponse response)
        {
            if (this.onRequested != null)
            {
                this.onRequested(request, response);
            }
        }

        /// <summary>
        /// When a response is available. On Remote side
        /// </summary>
        public void OnResponsed(string suffix, byte[] buffer)
        {
            if (this.onResponse != null)
            {
                this.onResponse(suffix, buffer);
            }
        }
        #endregion
    }
}
