﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using MicroControllerApp.Models.ViewClasses.Graphics;
using LongModel.Models;
using LongModel.Collections;
using MicroControllerApp.Models.DataClasses.ItemInfos;
using LongModel.Collections.MultiChoice;
using MicroControllerApp.Models.ViewClasses.Options;
using MicroControllerApp.Models.DataClasses.Settings;
using MicroControllerApp.Models.Base;
using MicroControllerApp.Models.DataClasses.Settings.Others;
using MicroControllerApp.Models.ViewClasses.Infos;
using LongModel.Views.Infos;
using LongModel.Models.References;
using LongWpfUI.Resources.Accessors;
using MicroControllerApp.Models.DataClasses.Packages;

namespace MicroControllerApp.Models
{    
    public class OutputParameters : Model
    {
        #region Getter, setter
        private bool isWorking;
        private string appStatus;
        private MultiChoice2Option<ViewOption> viewCollection;
        private int matSampleAmount;
        private double capacitor1;
        private double capacitor2;
        private double capacitor3;
        private double capacitor4;
        private AccValue _accelerometer1;
        private AccValue _accelerometer2;
        private AccValue _accelerometer3;
        private AccValue _accelerometer4;
        private string motorCtr_AutoBalanceStatus;
        

        //Share
        public bool IsWorking
        {
            get { return this.isWorking; }
            set
            {
                this.SetProperty(ref this.isWorking, value);
            }
        }        
        public string AppStatus
        {
            get { return this.appStatus; }
            private set
            {
                this.SetProperty(ref this.appStatus, value);
            }
        }        
        public MultiChoice2Option<ViewOption> ViewCollection
        {
            get { return this.viewCollection; }
            set
            {
                this.SetProperty(ref this.viewCollection, value);
            }
        }
        public int MatSampleAmount
        {
            get { return this.matSampleAmount; }
            set
            {
                this.SetProperty(ref this.matSampleAmount, value);
            }
        }

        //Capacitor        
        public double Capacitor1
        {
            get { return this.capacitor1; }
            set
            {
                this.SetProperty(ref this.capacitor1, value);
            }
        }
        public double Capacitor2
        {
            get { return this.capacitor2; }
            set
            {
                this.SetProperty(ref this.capacitor2, value);
            }
        }
        public double Capacitor3
        {
            get { return this.capacitor3; }
            set
            {
                this.SetProperty(ref this.capacitor3, value);
            }
        }
        public double Capacitor4
        {
            get { return this.capacitor4; }
            set
            {
                this.SetProperty(ref this.capacitor4, value);
            }
        }
        //Acceleration
        
        public AccValue Accelerometer1
        {
            get { return _accelerometer1; }
            set
            {
                this.SetProperty(ref _accelerometer1, value);
            }
        }        
        public AccValue Accelerometer2
        {
            get { return _accelerometer2; }
            set
            {
                this.SetProperty(ref _accelerometer2, value);
            }
        }        
        public AccValue Accelerometer3
        {
            get { return _accelerometer3; }
            set
            {
                this.SetProperty(ref _accelerometer3, value);
            }
        }        
        public AccValue Accelerometer4
        {
            get { return _accelerometer4; }
            set
            {
                this.SetProperty(ref _accelerometer4, value);
            }
        }

        public string MotorCtr_AutoBalanceStatus
        {
            get { return this.motorCtr_AutoBalanceStatus; }
            set
            {
                this.SetProperty(ref this.motorCtr_AutoBalanceStatus, value);
            }
        }  
        #endregion

        #region Getter
        public string SoftwareFullName { get; private set; }

        //File Reader
        public ObservableCollection<FileContentInfo> DocumentPages { get; private set; }

        //Graphic
        public DataPlotModel PlotModel { get; private set; }

        //Press
        public ManualObservableCollection<PressInfo> PressCollection { get; private set; }

        //uBalance
        public List<uBalanceActionContainer> uBalanceContainers { get; private set; }

        //MotorCtr
        public List<MotorCtrActionContainer> MotorCtrContainer { get; private set; }

        //Acc
        public AccAxisStatus AxisStatus1 { get; private set; }
        public AccAxisStatus AxisStatus2 { get; private set; }
        public AccAxisStatus AxisStatus3 { get; private set; }
        public AccAxisStatus AxisStatus4 { get; private set; }

        //About
        public SectionInfo AboutDescriptions { get; private set; }
        public SectionInfo AboutFunctions { get; private set; }
        public SectionInfo AboutReferences { get; private set; }
        public SectionInfo AboutSupports { get; private set; }
        #endregion

        #region Field
        private Action<AppStatusTypes> updateStatusColor;
        #endregion

        #region Constructor
        public OutputParameters(Action<AppStatusTypes> updateStatusColor)
        {
            this.updateStatusColor = updateStatusColor;
            this.isWorking = false;
            this.appStatus = string.Empty;
            this.matSampleAmount = 0;
            this.capacitor1 = this.capacitor2 = this.capacitor3 = this.capacitor4 = 0;
            this.motorCtr_AutoBalanceStatus = string.Empty;

            //Software
            this.SoftwareFullName = string.Format("{0} {1}", App.SoftwareName, Properties.Settings.Default.SoftwareVersion);

            //File Reader
            this.DocumentPages = new ObservableCollection<FileContentInfo>();

            //Graphic
            this.PlotModel = new DataPlotModel();

            //Press
            this.PressCollection = new ManualObservableCollection<PressInfo>();

            //uBalance
            this.uBalanceContainers = new List<uBalanceActionContainer>();
            this.uBalanceContainers.Add(new uBalanceActionContainer("Channel 1", uBalanceChannelInfoBits.Channel1));
            this.uBalanceContainers.Add(new uBalanceActionContainer("Channel 2", uBalanceChannelInfoBits.Channel2));
            this.uBalanceContainers.Add(new uBalanceActionContainer("Channel 3", uBalanceChannelInfoBits.Channel3));

            //MotorCtr
            this.MotorCtrContainer = new List<MotorCtrActionContainer>();
            this.MotorCtrContainer.Add(new MotorCtrActionContainer("Motor 1", MotorControl1Flags.Motor1));
            this.MotorCtrContainer.Add(new MotorCtrActionContainer("Motor 2", MotorControl1Flags.Motor2));

            //acc
            this.AxisStatus1 = new AccAxisStatus(0);
            this.AxisStatus1.VisibilityChanged += AxisStatus_VisibilityChanged;
            this.AxisStatus2 = new AccAxisStatus(1);
            this.AxisStatus2.VisibilityChanged += AxisStatus_VisibilityChanged;
            this.AxisStatus3 = new AccAxisStatus(2);
            this.AxisStatus3.VisibilityChanged += AxisStatus_VisibilityChanged;
            this.AxisStatus4 = new AccAxisStatus(3);
            this.AxisStatus4.VisibilityChanged += AxisStatus_VisibilityChanged;

            #region About
            this.AboutDescriptions = new SectionInfo("Descriptions");
            this.AboutDescriptions.Collection.Add("Connect and control micro-controller over Serial or USB connection");
            this.AboutDescriptions.Collection.Add("Collect sample, write and read file");
            this.AboutDescriptions.Collection.Add("Interface in order to execute function, task on Microcontroller");

            this.AboutFunctions = new SectionInfo("Functions");
            this.AboutFunctions.Collection.Add("Detect hose clamp pliers closed");
            this.AboutFunctions.Collection.Add("Measure capacitance of capacitors during roration");
            this.AboutFunctions.Collection.Add("Measure acceleration during rotation");
            this.AboutFunctions.Collection.Add("Control uBalance system");
            this.AboutFunctions.Collection.Add("Test RF Transceiver");

            this.AboutReferences = new SectionInfo("References");
            this.AboutReferences.Collection.Add(new BasicReference("Oxyplot", "The MIT License (MIT) \nCopyright (c) 2014 OxyPlot contributors",
                                                                   IconAccessor.DLL.Icon32));
            this.AboutReferences.Collection.Add(new BasicReference("Cypress USB", "Cypress Semiconductor",
                                                                    IconAccessor.DLL.Icon32));
            this.AboutReferences.Collection.Add(new BasicReference("csmatio", "Copyright (c) 2007-2009, David Zier",
                                                                    IconAccessor.DLL.Icon32));

            this.AboutSupports = new SectionInfo("Supports");
            this.AboutSupports.Collection.Add(new ContactInfo("Duy Long Phan", "phanduylong@gmail.com", "0152 15020987"));
            #endregion
        }
        
        #endregion

        #region Public Method
        public void UpdateAppStatus(string message, AppStatusTypes type = AppStatusTypes.Normal)
        {
            this.AppStatus = message;
            this.updateStatusColor(type);
        }

        public bool HasView(AppViews type)
        {
            bool hasView = false;

            if (this.viewCollection != null)
            {
                if (this.viewCollection.SelectedItem1 != null && this.viewCollection.SelectedItem1.Type == type)
                {
                    hasView = true;
                }
                else if (this.viewCollection.SelectedItem2 != null && this.viewCollection.SelectedItem2.Type == type)
                {
                    hasView = true;
                }
            }
            return hasView;
        }

        public void ClearGraphic()
        {
            foreach (var item in this.PlotModel.Series)
            {
                var series = item as DataCurve;
                if (series != null)
                {
                    series.Points.Clear();
                }
            }
            if (HasView(AppViews.Graphic))
            {
                UpdateGraphic();
            }
        }

        public void UpdateGraphic()
        {
            this.PlotModel.InvalidatePlot(true);
        }
        #endregion

        #region Event Handler
        private void AxisStatus_VisibilityChanged(object sender, AccAxisVisibilityChangedEventArgs e)
        {
            if (e.Axis is DataCurve)
            {
                var curve = e.Axis as DataCurve;
                if (this.PlotModel.Series.Contains(curve))
                {
                    var index = this.PlotModel.Series.IndexOf(curve);
                    this.PlotModel.Series[index].IsVisible = e.CanShow;
                }
            }
        }
        #endregion
    }
}
