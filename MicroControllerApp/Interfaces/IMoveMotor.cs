﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Interfaces
{
    public interface IMoveMotor
    {
        byte Type { get; }
        bool IsForward { get; set; }
        ushort Pulse { get; set; }
    }
}
