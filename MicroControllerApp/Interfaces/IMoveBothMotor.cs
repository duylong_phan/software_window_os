﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Interfaces
{
    public interface IMoveBothMotor
    {
        IMoveMotor MotorCtr_Motor1Move { get; }
        IMoveMotor MotorCtr_Motor2Move { get; }
        ushort MotorCtr_PwmPeriod { get; }
        ushort MotorCtr_MoveBothDelay { get; }
    }
}
