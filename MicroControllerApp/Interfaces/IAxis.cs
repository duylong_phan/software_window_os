﻿using LongInterface.Models;
using OxyPlot;
using OxyPlot.Axes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Interfaces
{
    public interface IAxis : ICopyable<IAxis>
    {
        string Title { get; set; }
        string Unit { get; set; }
        bool IsZoomEnabled { get; set; }
        bool IsPanEnabled { get; set; }
        LineStyle MajorGridlineStyle { get; set; }
        AxisPosition Position { get; set; }
    }
}
