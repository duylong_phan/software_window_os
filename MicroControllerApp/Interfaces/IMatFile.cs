﻿using LongModel.Models.IO.Files;
using MicroControllerApp.Models.DataClasses.Settings.Sub;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Interfaces
{
    public interface IMatFile
    {
        FileSingleSetting MatFileSetting { get; set; }
        SampleInfo SampleInfo { get; set; }
    }
}
