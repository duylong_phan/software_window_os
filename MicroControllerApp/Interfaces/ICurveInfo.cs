﻿using LongInterface.Models;
using OxyPlot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Interfaces
{
    public interface ICurveInfo : ICopyable<ICurveInfo>
    {
        string Title { get; set; }
        OxyColor Color { get; set; }
        LineStyle LineStyle { get; set; }
    }
}
