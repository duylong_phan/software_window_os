﻿using MicroControllerApp.Models;
using MicroControllerApp.Models.Base;
using MicroControllerApp.Models.DataClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Interfaces
{
    public interface ITaskModel<T>
    {
        T Setting { get; }
        void ParseData(Payload payload);
        void Add(DataPackage package);
        void Update(OutputParameters para);
    }
}
