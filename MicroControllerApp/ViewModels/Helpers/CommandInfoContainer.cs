﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LongModel.ViewModels;
using LongModel.Views.ControlModels;
using LongWpfUI.Resources.Accessors;

namespace MicroControllerApp.ViewModels.Helpers
{
    public class CommandInfoContainer : ViewModelHelper<MVController>
    {
        #region Getter
        public ButtonInfo AboutApp { get; private set; }
        public ButtonInfo CloseApp { get; private set; }
        public ButtonInfo NewConfig { get; private set; }
        public ButtonInfo LoadConfig { get; private set; }
        public ButtonInfo EditConfig { get; private set; }
        public ButtonInfo SaveConfig { get; private set; }
        public ButtonInfo StartOper { get; private set; }
        public ButtonInfo StopOper { get; private set; }
        public ButtonInfo ReadMasterSetting { get; private set; }        
        public ButtonInfo OpenFolder { get; private set; }
        public ButtonInfo ShowBasicHelp { get; private set; }
        public ButtonInfo LoadDataFile { get; private set; }
        public ButtonInfo StartMatHandler { get; private set; }
        public ButtonInfo StopMatHandler { get; private set; }
        public ButtonInfo ShowSampleInfo { get; private set; }
        public ButtonInfo SaveFilePage { get; private set; }

        public ButtonInfo OutPin_ResetPin { get; private set; }
        public ButtonInfo OutPin_SetPin { get; private set; }
        public ButtonInfo OutPin_ExeCommand { get; private set; }
        public ButtonInfo OutPin_LoadFile { get; private set; }

        public ButtonInfo Press_RequestFileInfo { get; private set; }
        public ButtonInfo Press_FilterPress { get; private set; }
        public ButtonInfo Press_GetStatus { get; private set; }
        public ButtonInfo Press_ToggleDebug { get; private set; }
        public ButtonInfo Press_ReadSetting { get; private set; }        

        public ButtonInfo uBalance_LoadActions { get; private set; }
        public ButtonInfo uBalance_SaveActions { get; private set; }
        public ButtonInfo uBalance_GetStatus { get; private set; }
        public ButtonInfo uBalance_StartTrans { get; private set; }
        public ButtonInfo uBalance_StartOper { get; private set; }
        public ButtonInfo uBalance_StopOper { get; private set; }

        public ButtonInfo RfTrans_GetStatus { get; private set; }
        public ButtonInfo RfTrans_WriteSetting { get; private set; }
        public ButtonInfo RfTrans_StartOper { get; private set; }
        public ButtonInfo RfTrans_Help { get; private set; }
                
        public ButtonInfo MotorCtr_LoadActions { get; private set; }
        public ButtonInfo MotorCtr_SaveActions { get; private set; }
        public ButtonInfo MotorCtr_DeviceStatus { get; private set; }
        public ButtonInfo MotorCtr_ToggleSample { get; private set; }
        public ButtonInfo MotorCtr_SelectDataOption { get; private set; }
        public ButtonInfo MotorCtr_StartOper { get; private set; }
        public ButtonInfo MotorCtr_StopOper { get; private set; }
        public ButtonInfo MotorCtr_TransmitActions { get; private set; }
        public ButtonInfo MotorCtr_MoveMotor1Left { get; private set; }
        public ButtonInfo MotorCtr_MoveMotor1Right { get; private set; }
        public ButtonInfo MotorCtr_MoveMotor2Left { get; private set; }
        public ButtonInfo MotorCtr_MoveMotor2Right { get; private set; }
        public ButtonInfo MotorCtr_MoveBothMotor { get; private set; }
        public ButtonInfo MotorCtr_WritePwmPeriod { get; private set; }
        public ButtonInfo MotorCtr_StartAutoBalance { get; private set; }
        public ButtonInfo MotorCtr_StopAutoBalance { get; private set; }
        #endregion

        #region Constructor
        public CommandInfoContainer(MVController viewModel)
            : base(viewModel, true)
        {
            #region Basic
            this.AboutApp = new ButtonInfo("About", viewModel.AboutApp)
            {
                IconInfo = new IconInfo(IconAccessor.Info.Icon32, 32, 32)
            };
            this.CloseApp = new ButtonInfo("Close", viewModel.CloseApp)
            {
                IconInfo = new IconInfo(IconAccessor.Close.Icon32, 32, 32)
            };
            #endregion

            #region Config
            this.NewConfig = new ButtonInfo("New", viewModel.NewConfig)
            {
                IconInfo = new IconInfo(IconAccessor.NewFile.Icon32, 32, 32)
            };
            this.LoadConfig = new ButtonInfo("Load", viewModel.LoadConfig)
            {
                IconInfo = new IconInfo(IconAccessor.Load.Icon32, 32, 32)
            };
            this.EditConfig = new ButtonInfo("Edit", viewModel.EditConfig)
            {
                IconInfo = new IconInfo(IconAccessor.Edit.Icon32, 32, 32)
            };
            this.SaveConfig = new ButtonInfo("Save", viewModel.SaveConfig)
            {
                IconInfo = new IconInfo(IconAccessor.Save.Icon32, 32, 32)
            };
            this.OpenFolder = new ButtonInfo("Folder", viewModel.OpenFolder)
            {
                IconInfo = new IconInfo(IconAccessor.OpenFolder.Icon32, 32, 32)
            };
            this.ShowBasicHelp = new ButtonInfo("Help", viewModel.ShowBasicHelp)
            {
                IconInfo = new IconInfo(IconAccessor.Help.Icon32, 32, 32)
            };
            #endregion

            #region Oper
            this.StartOper = new ButtonInfo("Start", viewModel.StartOper)
            {
                IconInfo = new IconInfo(IconAccessor.Play.Icon32, 32, 32)
            };
            this.StopOper = new ButtonInfo("Stop", viewModel.StopOper)
            {
                IconInfo = new IconInfo(IconAccessor.Stop.Icon32, 32, 32)
            };
            this.ReadMasterSetting = new ButtonInfo("Master Setting", viewModel.ReadMasterSetting)
            {
                IconInfo = new IconInfo(IconAccessor.Chip.Icon32, 32, 32)
            };            
            #endregion   

            #region Mat
            this.ShowSampleInfo = new ButtonInfo("Mat Table", viewModel.ShowSampleInfo)
            {
                IconInfo = new IconInfo(IconAccessor.Table.Icon32, 32, 32)
            };
            this.StartMatHandler = new ButtonInfo("Start collecting", viewModel.StartMatHandler)
            {
                IconInfo = new IconInfo(IconAccessor.Play.Icon32, 32, 32)
            };
            this.StopMatHandler = new ButtonInfo("Stop collecting", viewModel.StopMatHandler)
            {
                IconInfo = new IconInfo(IconAccessor.Stop.Icon32, 32, 32)
            };
            #endregion

            #region File Page
            this.LoadDataFile = new ButtonInfo("Load", viewModel.LoadDataFile)
            {
                IconInfo = new IconInfo(IconAccessor.Load.Icon32, 32, 32)
            };

            this.SaveFilePage = new ButtonInfo("Save", viewModel.SaveFilePage)
            {
                IconInfo = new IconInfo(IconAccessor.Save.Icon32, 32, 32)
            };
            #endregion

            #region OutPin
            this.OutPin_ResetPin = new ButtonInfo("Signal to 0/Stop Command", viewModel.OutPin_ResetPin)
            {
                IconInfo = new IconInfo(IconAccessor.Ok.Icon32, 32, 32)
            };
            this.OutPin_SetPin = new ButtonInfo("Signal to 1", viewModel.OutPin_SetPin)
            {
                IconInfo = new IconInfo(IconAccessor.Ok.Icon32, 32, 32)
            };
            this.OutPin_ExeCommand = new ButtonInfo("Execute Command", viewModel.OutPin_ExeCommand)
            {
                IconInfo = new IconInfo(IconAccessor.Ok.Icon32, 32, 32)
            };
            this.OutPin_LoadFile = new ButtonInfo("Load File", viewModel.OutPin_LoadFile)
            {
                IconInfo = new IconInfo(IconAccessor.Load.Icon24, 24, 24)
            };
            #endregion

            #region Press
            this.Press_RequestFileInfo = new ButtonInfo("Get File Info", viewModel.Press_RequestFileInfo)
            {
                IconInfo = new IconInfo(IconAccessor.File.Icon32, 32, 32)
            };
            this.Press_FilterPress = new ButtonInfo("Filter", viewModel.Press_FilterPress)
            {
                IconInfo = new IconInfo(IconAccessor.Filter.Icon32, 32, 32)
            };
            this.Press_GetStatus = new ButtonInfo("Status", viewModel.Press_GetStatus)
            {
                IconInfo = new IconInfo(IconAccessor.Info.Icon32, 32, 32)
            };
            this.Press_ToggleDebug = new ButtonInfo("Enable/Disable Debug", viewModel.Press_ToggleDebug)
            {
                IconInfo = new IconInfo(IconAccessor.Ok.Icon32, 32, 32)
            };
            this.Press_ReadSetting = new ButtonInfo("Read Setting", viewModel.Press_ReadSetting)
            {
                IconInfo = new IconInfo(IconAccessor.Setting.Icon32, 32, 32)
            };           
            #endregion

            #region uBalance
            this.uBalance_LoadActions = new ButtonInfo("Load Actions", viewModel.uBalance_LoadActions)
            {
                IconInfo = new IconInfo(IconAccessor.Load.Icon24, 24, 24)
            };
            this.uBalance_SaveActions = new ButtonInfo("Save Actions", viewModel.uBalance_SaveActions)
            {
                IconInfo = new IconInfo(IconAccessor.Save.Icon24, 24, 24)
            };
            this.uBalance_GetStatus = new ButtonInfo("Status", viewModel.uBalance_GetStatus)
            {
                IconInfo = new IconInfo(IconAccessor.Info.Icon24, 24, 24)
            };
            this.uBalance_StartTrans = new ButtonInfo("Transmit", viewModel.uBalance_StartTrans)
            {
                IconInfo = new IconInfo(IconAccessor.AddMessage.Icon24, 24, 24)
            };
            this.uBalance_StartOper = new ButtonInfo("Start", viewModel.uBalance_StartOper)
            {
                IconInfo = new IconInfo(IconAccessor.Play.Icon24, 24, 24)
            };
            this.uBalance_StopOper = new ButtonInfo("Stop", viewModel.uBalance_StopOper)
            {
                IconInfo = new IconInfo(IconAccessor.Stop.Icon24, 24, 24)
            };
            #endregion

            #region RfTrans
            this.RfTrans_GetStatus = new ButtonInfo("Status", viewModel.RfTrans_GetStatus)
            {
                IconInfo = new IconInfo(IconAccessor.Info.Icon32, 32, 32)
            };
            this.RfTrans_WriteSetting = new ButtonInfo("Write Setting", viewModel.RfTrans_WriteSetting)
            {
                IconInfo = new IconInfo(IconAccessor.Configure.Icon32, 32, 32)
            };
            this.RfTrans_StartOper = new ButtonInfo("Start Operation", viewModel.RfTrans_StartOper)
            {
                IconInfo = new IconInfo(IconAccessor.Play.Icon32, 32, 32)
            };
            this.RfTrans_Help = new ButtonInfo("Help", viewModel.RfTrans_Help)
            {
                IconInfo = new IconInfo(IconAccessor.Help.Icon32, 32, 32)
            };
            #endregion

            #region MotorCtr
            this.MotorCtr_LoadActions = new ButtonInfo("Load Actions", viewModel.MotorCtr_LoadActions)
            {
                IconInfo = new IconInfo(IconAccessor.Load.Icon32, 32, 32)
            };
            this.MotorCtr_SaveActions = new ButtonInfo("Save Actions", viewModel.MotorCtr_SaveActions)
            {
                IconInfo = new IconInfo(IconAccessor.Save.Icon32, 32, 32)
            };
            this.MotorCtr_DeviceStatus = new ButtonInfo("Device Status", viewModel.MotorCtr_DeviceStatus)
            {
                IconInfo = new IconInfo(IconAccessor.Info.Icon32, 32, 32)
            };
            this.MotorCtr_ToggleSample = new ButtonInfo("E/Disable Data", viewModel.MotorCtr_ToggleSample)
            {
                IconInfo = new IconInfo(IconAccessor.Ok.Icon32, 32, 32)
            };
            this.MotorCtr_SelectDataOption = new ButtonInfo("Select Data Sample Option", viewModel.MotorCtr_SelectDataOption)
            {
                IconInfo = new IconInfo(IconAccessor.Setting.Icon32, 32, 32)
            };
            this.MotorCtr_StartOper = new ButtonInfo("Start Actions", viewModel.MotorCtr_StartOper)
            {
                IconInfo = new IconInfo(IconAccessor.Play.Icon32, 32, 32)
            };
            this.MotorCtr_StopOper = new ButtonInfo("Stop Actions", viewModel.MotorCtr_StopOper)
            {
                IconInfo = new IconInfo(IconAccessor.Stop.Icon32, 32, 32)
            };
            this.MotorCtr_TransmitActions = new ButtonInfo("Transmit Actions", viewModel.MotorCtr_TransmitActions)
            {
                IconInfo = new IconInfo(IconAccessor.Ok.Icon32, 32, 32)
            };
            this.MotorCtr_MoveMotor1Left = new ButtonInfo("Move Backward", viewModel.MotorCtr_MoveMotor1Left)
            {
                IconInfo = new IconInfo(IconAccessor.Left.Icon32, 32, 32)
            };
            this.MotorCtr_MoveMotor1Right = new ButtonInfo("Move Forward", viewModel.MotorCtr_MoveMotor1Right)
            {
                IconInfo = new IconInfo(IconAccessor.Right.Icon32, 32, 32)
            };
            this.MotorCtr_MoveMotor2Left = new ButtonInfo("Move Backward", viewModel.MotorCtr_MoveMotor2Left)
            {
                IconInfo = new IconInfo(IconAccessor.Left.Icon32, 32, 32)
            };
            this.MotorCtr_MoveMotor2Right = new ButtonInfo("Move Forward", viewModel.MotorCtr_MoveMotor2Right)
            {
                IconInfo = new IconInfo(IconAccessor.Right.Icon32, 32, 32)
            };
            this.MotorCtr_MoveBothMotor = new ButtonInfo("Move Both Motor", viewModel.MotorCtr_MoveBothMotor)
            {
                IconInfo = new IconInfo(IconAccessor.Ok.Icon32, 32, 32)
            };
            this.MotorCtr_WritePwmPeriod = new ButtonInfo("Update PWM Period", viewModel.MotorCtr_WritePwmPeriod)
            {
                IconInfo = new IconInfo(IconAccessor.Configure.Icon32, 32, 32)
            };
            this.MotorCtr_StartAutoBalance = new ButtonInfo("Stat Auto Balance", viewModel.MotorCtr_StartAutoBalance)
            {
                IconInfo = new  IconInfo(IconAccessor.Play.Icon32, 32, 32)
            };
            this.MotorCtr_StopAutoBalance = new ButtonInfo("Stop Auto Balance", viewModel.MotorCtr_StopAutoBalance)
            {
                IconInfo = new IconInfo(IconAccessor.Stop.Icon32, 32, 32)
            };
            #endregion
        }

        #endregion
    }
}
