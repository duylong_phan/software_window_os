﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LongModel.ViewModels;
using LongWpfUI.Models.ValidationRules;
using MicroControllerApp.Models.ViewClasses.ValidationRules;

namespace MicroControllerApp.ViewModels.Helpers
{
    public class ValidationRuleContainer : ViewModelHelper<MVController>
    {
        #region Getter

        //Share
        public IntegerRule ByteRule { get; private set; }
        public IntegerRule NonZeroByteRule { get; private set; }
        public IntegerRule UshortRule { get; private set; }
        public IntegerRule NonZeroUshortRule { get; private set; }
        public IntegerRule PosShortRule { get; private set; }
        public IntegerRule PosIntRule { get; private set; }
        public IntegerRule ShortRule { get; private set; }
        public DoubleRule PosDoubleRule { get; private set; }

        //Specific
        public FileNameRule NameRule { get; private set; }
        public IPAddressRule AddressRule { get; private set; }        
        public IntegerRule PointAmountRule { get; private set; }
        public MatVariableRule FileNameRule { get; private set; }
        public IntegerRule SampleAmountRule { get; private set; }       
        public IntegerRule DurationRule { get; private set; }
        public IntegerRule ThresholdAmountRule { get; private set; }
        public HexaRule UsbIdRule { get; set; }        
        public RfAddressRule RfAddressRule { get; private set; }
        public IntegerRule Cap_ResistorRule { get; private set; }        
        public IntegerRule Press_Duration { get; private set; }        
        public IntegerRule uBalance_IndexRule { get; private set; }        
        public IntegerRule RfTrans_Duration { get; private set; }
        #endregion

        #region Constructor
        public ValidationRuleContainer(MVController viewModel)
            : base(viewModel, false)
        {
            //Shared
            this.ByteRule = new IntegerRule(0, byte.MaxValue);
            this.NonZeroByteRule = new IntegerRule(1, byte.MaxValue);
            this.UshortRule = new IntegerRule(0, ushort.MaxValue);
            this.NonZeroUshortRule = new IntegerRule(1, ushort.MaxValue);
            this.PosShortRule = new IntegerRule(0, short.MaxValue);
            this.PosIntRule = new IntegerRule(0, int.MaxValue);
            this.ShortRule = new IntegerRule(short.MinValue, short.MaxValue);
            this.PosDoubleRule = new DoubleRule(0);

            //Specific
            this.NameRule = new FileNameRule() { AllowEmpty = false};
            this.AddressRule = new IPAddressRule();            
            this.PointAmountRule = new IntegerRule(200, 5000);
            this.FileNameRule = new MatVariableRule();
            this.SampleAmountRule = new IntegerRule(200, 50000);            
            this.DurationRule = new IntegerRule(10, int.MaxValue);            
            this.ThresholdAmountRule = new IntegerRule(1, 256);
            this.UsbIdRule = new HexaRule("0001", "FFFF");            
            this.RfAddressRule = new RfAddressRule();            
            this.Press_Duration = new IntegerRule(1);            
            this.Cap_ResistorRule = new IntegerRule(2000, int.MaxValue);            
            this.uBalance_IndexRule = new IntegerRule(0, ushort.MaxValue - 1);
            this.RfTrans_Duration = new IntegerRule(5, ushort.MaxValue);
        }
        #endregion
    }
}
