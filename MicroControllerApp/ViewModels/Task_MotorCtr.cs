﻿using LongInterface.Models;
using LongInterface.Models.IO.Files;
using LongModel.Helpers;
using LongModel.ViewModels;
using MicroControllerApp.Interfaces;
using MicroControllerApp.Models;
using MicroControllerApp.Models.Base;
using MicroControllerApp.Models.DataClasses.Packages;
using MicroControllerApp.Models.DataClasses.Settings;
using MicroControllerApp.Models.DataClasses.Settings.Others;
using MicroControllerApp.Models.ViewClasses.Graphics;
using MicroControllerApp.ViewModels.Helpers;
using MicroControllerApp.Views.Dialogs;
using Microsoft.Win32;
using OxyPlot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using LongModel.ExtensionMethods;
using System.Globalization;
using MicroControllerApp.ViewModels.Logics;

namespace MicroControllerApp.ViewModels
{
    public partial class MVController : ViewModel<MainWindow, InputParameters, OutputParameters, CommandInfoContainer, ValidationRuleContainer>
    {
        #region Local
        private void MotorCtr_InitializeSW()
        {
            var setting = this.InputParameter.CurrentConfig.CurrentSetting as MotorCtrSetting;
            Setting_InitializeSW(setting);

            if (setting.FileChangedSetting.IsEnabled && setting.FileChangedSetting.CaptureOnStart)
            {
                this.fileWatcher = new System.IO.FileSystemWatcher()
                {
                    Path = System.IO.Path.GetDirectoryName(setting.FileChangedSetting.FilePath),
                    Filter = System.IO.Path.GetFileName(setting.FileChangedSetting.FilePath),
                    NotifyFilter = System.IO.NotifyFilters.LastWrite,                    
                };
                this.fileWatcher.Changed += FileWatcher_Changed;
            }
        }

        private void MotorCtr_InitializeHW()
        {
            var setting = this.InputParameter.CurrentConfig.CurrentSetting as MotorCtrSetting;
            Setting_InitializeHW(setting);
        }
        
        private void MotorCtr_Update()
        {
            if (this.packageQueue.Count == 0)
            {
                return;
            }

            var setting = this.InputParameter.CurrentConfig.CurrentSetting as MotorCtrSetting;
            var sampleCollection = new List<double[]>();
            DataPackage package = null;

            #region Get Collection
            while (this.packageQueue.Count > 0)
            {
                this.packageQueue.TryDequeue(out package);
                if (package is ISharedPackage)
                {
                    Setting_Update(setting, package as ISharedPackage);
                }
                else if(package is MotorCtrStatusPackage)
                {
                    //Normal update for AppStatus  => avoid Thread Blocking
                    if (this.hasResponse)
                    {
                        var statusPackage = package as MotorCtrStatusPackage;
                        var message = MotorCtr_GetDeviceStatusMessage(statusPackage.Status);
                        PcActionHelper.DoAction((uint)PcActions.AppStatus, message, true);
                    }
                    //for waitResponse Command
                    else
                    {
                        this.lastResponse = package;
                        this.hasResponse = true;
                    }                    
                }                
                else if (package is MotorCtrSamplePackage)
                {
                    var rawPackage = package as MotorCtrSamplePackage;                    
                    //Check Flag, and get value
                    double battery = ((rawPackage.SampleFlag & (uint)MotorCtrSampleTypes.Battery) != 0) ? rawPackage.Battery : 0;
                    double pulse = ((rawPackage.SampleFlag & (uint)MotorCtrSampleTypes.Pulse) != 0) ? rawPackage.Pulse : 0;
                    double pwmFrequency = ((rawPackage.SampleFlag & (uint)MotorCtrSampleTypes.PwmFreq) != 0) ? rawPackage.Pwm_Frequency : 0;
                    sampleCollection.Add(new double[] {battery, pulse, pwmFrequency });
                }
            }
            #endregion

            #region View
            if (sampleCollection.Count > 0)
            {
                //Get Array
                var pointCollection = new List<DataPoint>[MotorCtrDataOption.OptionAmount];
                var curves = new DataCurve[MotorCtrDataOption.OptionAmount];
                for (int i = 0; i < MotorCtrDataOption.OptionAmount; i++)
                {
                    pointCollection[i] = new List<DataPoint>();
                    curves[i] = this.OutputParameter.PlotModel.Series[i] as DataCurve;
                }

                //go throung each sample
                foreach (var item in sampleCollection)
                {
                    for (int i = 0; i < MotorCtrDataOption.OptionAmount; i++)
                    {
                        pointCollection[i].Add(new DataPoint(this.currentTime, item[i]));
                    }
                }
                this.currentTime += samplePeriod;

                //Check overflow
                int remain = setting.GraphicSetting.PointAmount - curves[0].Points.Count;
                int overflow = pointCollection[0].Count - remain;
                if (overflow > 0)
                {
                    for (int i = 0; i < MotorCtrDataOption.OptionAmount; i++)
                    {
                        curves[i].Points.RemoveRange(0, overflow);
                    }
                }

                //add Point
                for (int i = 0; i < MotorCtrDataOption.OptionAmount; i++)
                {
                    curves[i].Points.AddRange(pointCollection[i]);
                }

                //View is available
                if (this.OutputParameter.HasView(AppViews.Graphic))
                {
                    this.OutputParameter.UpdateGraphic();
                }
            }
            #endregion

            #region Sample in File
            if (this.matFileHandler != null && this.matFileHandler.IsEnabled)
            {        
                foreach (var item in sampleCollection)
                {
                    var container = new double[MotorCtrDataOption.OptionAmount + 1];
                    container[0] = (DateTime.Now.Ticks - this.startTick) / (double)TimeSpan.TicksPerSecond;
                    Buffer.BlockCopy(item, 0, container, 1, MotorCtrDataOption.OptionAmount);
                    this.matFileHandler.Add(container);
                }
                this.OutputParameter.MatSampleAmount = this.matFileHandler.SampleAmount;
            }
            #endregion
        }

        private void MotorCtr_DisposeSW()
        {
            var setting = this.InputParameter.CurrentConfig.CurrentSetting as MotorCtrSetting;

            Setting_DisposeSW(setting);
            this.MotorCtr_StopAutoBalance.TryToExecute();
            if (setting.FileChangedSetting.IsEnabled && setting.FileChangedSetting.CaptureOnStart)
            {
                if (this.fileWatcher != null)
                {
                    this.fileWatcher.EnableRaisingEvents = false;
                    this.fileWatcher.Dispose();
                    this.fileWatcher = null;
                }
            }
        }

        private void MotorCtr_DisposeHW()
        {
            var setting = this.InputParameter.CurrentConfig.CurrentSetting as MotorCtrSetting;
            Setting_DisposeHW(setting);
        }
        #endregion

        #region Remote
        private void MotorCtr_OnRequested(HttpListenerRequest request, HttpListenerResponse respone)
        {
           
        }

        private void MotorCtr_OnResponsed(string suffix, byte[] buffer)
        {
            
        }
        #endregion

        #region Command       

        #region Editor
        private bool Can_MotorCtr_AddAction(object parameter)
        {
            return parameter is MotorCtrActionContainer;
        }

        private void Exe_MotorCtr_AddAction(object parameter)
        {
            try
            {
                var container = parameter as MotorCtrActionContainer;
                var action = new MotorCtrMotorAction();
                action.MotorType = (byte)container.Type;
                SetViewProperties(action);
                var window = new MotorCtrMotorActionSettingWindow(this.View, action, true, this.MotorCtr_Help);
                if (window.ShowDialog() == true)
                {
                    container.Add(action);
                    action.CalculateProperty();
                    this.OutputParameter.UpdateAppStatus("Motor Action was added");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Add Action Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private bool Can_MotorCtr_EditAction(object parameter)
        {
            return parameter is MotorCtrMotorAction;
        }

        private void Exe_MotorCtr_EditAction(object parameter)
        {
            try
            {
                var action = parameter as MotorCtrMotorAction;
                SetViewProperties(action);
                var window = new MotorCtrMotorActionSettingWindow(this.View, action, false, this.MotorCtr_Help);
                if(window.ShowDialog() == true)
                {
                    action.CalculateProperty();
                    this.OutputParameter.UpdateAppStatus("Motor Action was edited");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Edit Action Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private bool Can_MotorCtr_RemoveAction(object parameter)
        {
            return parameter is MotorCtrMotorAction;
        }

        private void Exe_MotorCtr_RemoveAction(object parameter)
        {
            try
            {
                var action = parameter as MotorCtrMotorAction;
                MotorCtrActionContainer container = null;
                foreach (var item in this.OutputParameter.MotorCtrContainer)
                {
                    if (item.Contains(action))
                    {
                        container = item;
                        break;
                    }
                }

                if (container == null)
                {
                    throw new Exception("Action Container is not available");
                }

                var result = MessageBox.Show("Do you really want to remove this action?", "Remove action", MessageBoxButton.YesNo, MessageBoxImage.Information);
                if (result == MessageBoxResult.Yes)
                {
                    container.Remove(action);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Edit Action Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private bool Can_MotorCtr_UpAction(object parameter)
        {
            return parameter is MotorCtrMotorAction;
        }

        private void Exe_MotorCtr_UpAction(object parameter)
        {
            try
            {
                var action = parameter as MotorCtrMotorAction;
                MotorCtrActionContainer container = null;
                int index = -1;
                foreach (var item in this.OutputParameter.MotorCtrContainer)
                {
                    if (item.Contains(action))
                    {
                        container = item;
                        index = container.IndexOf(action);
                        break;
                    }
                }

                if (container != null && index > 0 && index < container.Count)
                {
                    index--;
                    container.Remove(action);
                    container.Insert(index, action);
                    this.View.UpdatePackageListBoxSelectedIndex(index);
                }
                else
                {
                    this.OutputParameter.UpdateAppStatus("Action can't be moved");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Move Action Up Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private bool Can_MotorCtr_DownAction(object parameter)
        {
            return parameter is MotorCtrMotorAction;
        }

        private void Exe_MotorCtr_DownAction(object parameter)
        {
            try
            {
                var action = parameter as MotorCtrMotorAction;
                MotorCtrActionContainer container = null;
                int index = -1;
                foreach (var item in this.OutputParameter.MotorCtrContainer)
                {
                    if (item.Contains(action))
                    {
                        container = item;
                        index = container.IndexOf(action);
                        break;
                    }
                }

                if (container != null && index >= 0 && index < (container.Count - 1))
                {
                    index++;
                    container.Remove(action);
                    container.Insert(index, action);
                    this.View.UpdatePackageListBoxSelectedIndex(index);
                }
                else
                {
                    this.OutputParameter.UpdateAppStatus("Action can't be moved");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Move Down Up Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private bool Can_MotorCtr_Help()
        {
            return true;
        }

        private void Exe_MotorCtr_Help()
        {
            NotifyNoFunction();
        }
        #endregion

        #region File
        private bool Can_MotorCtr_LoadActions()
        {
            return true;
        }

        private async void Exe_MotorCtr_LoadActions()
        {
            try
            {
                var dialog = new OpenFileDialog();
                dialog.InitialDirectory = App.DataPath;
                dialog.Filter = MotorCtrMotorAction.FileFilter;
                if (dialog.ShowDialog() == true)
                {
                    #region Read, and parse Data
                    var collection = new List<MotorCtrMotorAction>();
                    await Task.Factory.StartNew(() =>
                    {
                        using (var reader = new System.IO.StreamReader(dialog.FileName))
                        {
                            var line = reader.ReadLine();
                            while (line != null)
                            {
                                var data = XmlSerializerHelper.GetData(line, typeof(MotorCtrMotorAction)) as MotorCtrMotorAction;
                                if (data != null)
                                {
                                    collection.Add(data);
                                }
                                line = reader.ReadLine();
                            }

                        }
                    });
                    #endregion

                    #region Remove old data, add into Collection
                    foreach (var item in this.OutputParameter.MotorCtrContainer)
                    {
                        item.Clear();
                    }                    
                    foreach (var item in collection)
                    {
                        foreach (var subItem in this.OutputParameter.MotorCtrContainer)
                        {
                            if (item.MotorType == (byte)subItem.Type)
                            {
                                subItem.Add(item);
                                //Important => Update Property
                                item.CalculateProperty();
                                break;
                            }
                        }
                    }
                    #endregion

                    this.OutputParameter.UpdateAppStatus("Load MotorCtr Action File successfully");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Load MotorCtr Action file error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private bool Can_MotorCtr_SaveActions()
        {
            return true;
        }

        private async void Exe_MotorCtr_SaveActions()
        {
            try
            {
                var collection = new List<MotorCtrMotorAction>();
                foreach (var item in this.OutputParameter.MotorCtrContainer)
                {
                    collection.AddRange(item);
                }
                if (collection.Count == 0)
                {
                    throw new Exception("No Action is available");
                }

                var dialog = new SaveFileDialog();
                dialog.InitialDirectory = App.DataPath;
                dialog.Filter = MotorCtrMotorAction.FileFilter;
                if (dialog.ShowDialog() == true)
                {
                    await Task.Factory.StartNew(() =>
                    {
                        using (var writer = new System.IO.StreamWriter(System.IO.File.Create(dialog.FileName)))
                        {
                            foreach (var item in collection)
                            {
                                writer.WriteLine(XmlSerializerHelper.GetXML(item));
                            }
                        }
                    });

                    this.OutputParameter.UpdateAppStatus("Save MotorCtr Action File successfully");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Save MotorCtr Action file error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        #endregion

        #region Operation
        private bool Can_MotorCtr_DeviceStatus()
        {
            return this.OutputParameter.IsWorking;
        }

        private async void Exe_MotorCtr_DeviceStatus()
        {
            try
            {
                var rfPackage = new RfPackage((byte)MicroTasks.MotorCtr, (byte)MotorCtrTypes.GetStatus);
                await MorotCtr_TransmitBasicCommandAsync(rfPackage, true);
            }
            catch (Exception ex)
            {
                MotorCtr_NotifyError(ex.Message, "Get Device Status Error");
            }
        }

        private bool Can_MotorCtr_ToggleSample()
        {
            return this.OutputParameter.IsWorking;
        }

        private async void Exe_MotorCtr_ToggleSample()
        {
            try
            {
                var setting = this.InputParameter.CurrentConfig.CurrentSetting as MotorCtrSetting;
                var payload = setting.DataOption.GetBytes();
                var rfPackage = new RfPackage((byte)MicroTasks.MotorCtr, (byte)MotorCtrTypes.ToggleSample, payload);
                await MorotCtr_TransmitBasicCommandAsync(rfPackage, true);
            }
            catch (Exception ex)
            {
                MotorCtr_NotifyError(ex.Message, "E/Disable Data sample Error");
            }
        }

        private bool Can_MotorCtr_SelectDataOption()
        {
            return true;
        }

        private void Exe_MotorCtr_SelectDataOption()
        {
            try
            {
                var setting = this.InputParameter.CurrentConfig.CurrentSetting as MotorCtrSetting;
                SetViewProperties(setting.DataOption);
                var window = new MotorCtrDataOptionWindow(this.View, setting.DataOption);
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MotorCtr_NotifyError(ex.Message, "Select Data Sample Error");
            }
        }

        private bool Can_MotorCtr_TransmitActions()
        {
            return this.OutputParameter.IsWorking;
        }

        private async void Exe_MotorCtr_TransmitActions()
        {
            try
            {
                #region Check Package amount
                int amount = 0;
                foreach (var item in this.OutputParameter.MotorCtrContainer)
                {
                    amount += item.Count;
                }
                if (amount == 0)
                {
                    throw new Exception("No Action is available. Please create actions");
                }
                if (amount > byte.MaxValue)
                {
                    throw new Exception("Package amount should be smaller or equal 255");
                }
                #endregion

                #region Generate and transmit
                byte command = 0;
                if (this.InputParameter.MotorCtr_StoreAction)
                {
                    command |= (byte)MotorCtrEndTransCommands.StoreAction;
                }
                if (this.InputParameter.MotorCtr_AutoStartAfter)
                {
                    command |= (byte)MotorCtrEndTransCommands.StartAfter;
                }

                var collection = MotorCtr_GetActionTransPackage(command, this.InputParameter.MotorCtr_StartDelay); ;
                for (int i = 0; i < collection.Count; i++)
                {
                    if (i != 0)
                    {
                        await Task.Delay(50);
                    }
                    WriteOut(collection[i]); 
                }
                #endregion
            }
            catch (Exception ex)
            {                
                MotorCtr_NotifyError(ex.Message, "Transmit Action Error");
            }
        }

        private bool Can_MotorCtr_StartOper()
        {
            return this.OutputParameter.IsWorking;
        }

        private async void Exe_MotorCtr_StartOper()
        {
            try
            {
                var rfPackage = new RfPackage((byte)MicroTasks.MotorCtr, (byte)MotorCtrTypes.StartOper);
                await MorotCtr_TransmitBasicCommandAsync(rfPackage, true);
            }
            catch (Exception ex)
            {
                MotorCtr_NotifyError(ex.Message, "Start systen operation Error");
            }
        }
        
        private bool Can_MotorCtr_StopOper()
        {
            return this.OutputParameter.IsWorking;
        }

        private async void Exe_MotorCtr_StopOper()
        {
            try
            {
                var rfPackage = new RfPackage((byte)MicroTasks.MotorCtr, (byte)MotorCtrTypes.StopOper);
                await MorotCtr_TransmitBasicCommandAsync(rfPackage, true);
            }
            catch (Exception ex)
            {
                MotorCtr_NotifyError(ex.Message, "Stop systen operation Error");
            }
        }
        #endregion

        #region Real-Time Move        
        
        private bool Can_MotorCtr_MoveMotor1Left(object parameter)
        {
            return this.OutputParameter.IsWorking;
        }

        private async void Exe_MotorCtr_MoveMotor1Left(object parameter)
        {
            try
            {                
                ushort pulse = this.InputParameter.MotorCtr_Motor1Move.Pulse;
                if (parameter is ushort)
                {
                    pulse = (ushort)parameter;
                }

                var action = new MotorCtrMotorAction()
                {
                    MotorType = this.InputParameter.MotorCtr_Motor1Move.Type,
                    Amount = 1,
                    CanInvert = true,
                    HasPwm = true,
                    TriggerAmount = pulse,
                    PwmPeriod = this.InputParameter.MotorCtr_PwmPeriod
                };
                var rfPackage = new RfPackage((byte)MicroTasks.MotorCtr, (byte)MotorCtrTypes.RealAction, action.GetBytes());
                await MorotCtr_TransmitBasicCommandAsync(rfPackage, false);
            }
            catch (Exception ex)
            {
                MotorCtr_NotifyError(ex.Message, "Move Backware Error");
            }
        }        

        private bool Can_MotorCtr_MoveMotor1Right(object parameter)
        {
            return this.OutputParameter.IsWorking;
        }

        private async void Exe_MotorCtr_MoveMotor1Right(object parameter)
        {
            try
            {
                ushort pulse = this.InputParameter.MotorCtr_Motor1Move.Pulse;
                if (parameter is ushort)
                {
                    pulse = (ushort)parameter;
                }

                var action = new MotorCtrMotorAction()
                {
                    MotorType = this.InputParameter.MotorCtr_Motor1Move.Type,
                    Amount = 1,                    
                    HasPwm = true,
                    TriggerAmount = pulse,
                    PwmPeriod = this.InputParameter.MotorCtr_PwmPeriod
                };
                var rfPackage = new RfPackage((byte)MicroTasks.MotorCtr, (byte)MotorCtrTypes.RealAction, action.GetBytes());
                await MorotCtr_TransmitBasicCommandAsync(rfPackage, false);
            }
            catch (Exception ex)
            {                
                MotorCtr_NotifyError(ex.Message, "Move Forward Error");
            }
        }

        private bool Can_MotorCtr_MoveMotor2Left(object parameter)
        {
            return this.OutputParameter.IsWorking;
        }

        private async void Exe_MotorCtr_MoveMotor2Left(object parameter)
        {
            try
            {
                ushort pulse = this.InputParameter.MotorCtr_Motor2Move.Pulse;
                if (parameter is ushort)
                {
                    pulse = (ushort)parameter;
                }

                var action = new MotorCtrMotorAction()
                {
                    MotorType = this.InputParameter.MotorCtr_Motor2Move.Type,
                    Amount = 1,
                    CanInvert = true,
                    HasPwm = true,
                    TriggerAmount = pulse,
                    PwmPeriod = this.InputParameter.MotorCtr_PwmPeriod
                };
                var rfPackage = new RfPackage((byte)MicroTasks.MotorCtr, (byte)MotorCtrTypes.RealAction, action.GetBytes());
                await MorotCtr_TransmitBasicCommandAsync(rfPackage, false);
            }
            catch (Exception ex)
            {
                MotorCtr_NotifyError(ex.Message, "Move Backward Error");
            }
        }

        private bool Can_MotorCtr_MoveMotor2Right(object parameter)
        {
            return this.OutputParameter.IsWorking;
        }

        private async void Exe_MotorCtr_MoveMotor2Right(object parameter)
        {
            try
            {
                ushort pulse = this.InputParameter.MotorCtr_Motor2Move.Pulse;
                if (parameter is ushort)
                {
                    pulse = (ushort)parameter;
                }

                var action = new MotorCtrMotorAction()
                {
                    MotorType = this.InputParameter.MotorCtr_Motor2Move.Type,
                    Amount = 1,
                    HasPwm = true,
                    TriggerAmount = pulse,
                    PwmPeriod = this.InputParameter.MotorCtr_PwmPeriod
                };
                var rfPackage = new RfPackage((byte)MicroTasks.MotorCtr, (byte)MotorCtrTypes.RealAction, action.GetBytes());
                await MorotCtr_TransmitBasicCommandAsync(rfPackage, false);
            }
            catch (Exception ex)
            {
                MotorCtr_NotifyError(ex.Message, "Move Forward Error");
            }
        }
               
        private bool Can_MotorCtr_MoveBothMotor(object parameter)
        {
            return this.OutputParameter.IsWorking && parameter is IMoveBothMotor;
        }

        private async void Exe_MotorCtr_MoveBothMotor(object parameter)
        {
            try
            {
                var dataModel = parameter as IMoveBothMotor;
                var action1 = new MotorCtrMotorAction()
                {
                    MotorType = dataModel.MotorCtr_Motor1Move.Type,
                    Amount = 1,
                    CanInvert = dataModel.MotorCtr_Motor1Move.IsForward,
                    HasPwm = true,
                    TriggerAmount = dataModel.MotorCtr_Motor1Move.Pulse,
                    PwmPeriod = dataModel.MotorCtr_PwmPeriod
                };
                var package = new RfPackage((byte)MicroTasks.MotorCtr, (byte)MotorCtrTypes.RealAction, action1.GetBytes());
                WriteOut(package);

                //for the Voltage to recover
                await Task.Delay(dataModel.MotorCtr_MoveBothDelay);

                var action2 = new MotorCtrMotorAction()
                {
                    MotorType = dataModel.MotorCtr_Motor2Move.Type,
                    Amount = 1,
                    CanInvert = dataModel.MotorCtr_Motor2Move.IsForward,
                    HasPwm = true,
                    TriggerAmount = dataModel.MotorCtr_Motor2Move.Pulse,
                    PwmPeriod = dataModel.MotorCtr_PwmPeriod
                };
                package = new RfPackage((byte)MicroTasks.MotorCtr, (byte)MotorCtrTypes.RealAction, action2.GetBytes());
                WriteOut(package);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Move Both Motor Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }       

        private bool Can_MotorCtr_WritePwmPeriod()
        {
            return this.OutputParameter.IsWorking;
        }

        private async void Exe_MotorCtr_WritePwmPeriod()
        {
            try
            {
                var action = new MotorCtrPwmPeriodAction((ushort)this.InputParameter.MotorCtr_PwmPeriod);
                var rfPackage = new RfPackage((byte)MicroTasks.MotorCtr, (byte)MotorCtrTypes.WritePwm, action.GetBytes());
                await MorotCtr_TransmitBasicCommandAsync(rfPackage, false);
            }
            catch (Exception ex)
            {
                MotorCtr_NotifyError(ex.Message, "Move Backward Error");
            }
        }
        #endregion

        #region Automatic function        
        private bool Can_MotorCtr_StartAutoBalance()
        {
            return this.OutputParameter.IsWorking && !this.InputParameter.MotorCtr_CanAutoBalance;    //NOT
        }

        private void Exe_MotorCtr_StartAutoBalance()
        {
            try
            {
                var setting = this.InputParameter.CurrentConfig.CurrentSetting as MotorCtrSetting;

                if (!setting.FileChangedSetting.IsEnabled || !setting.FileChangedSetting.CaptureOnStart)
                {
                    throw new Exception("Please check enable and capture on start option of File Observation in Configuration");
                }
                this.fileChangedIndex = 0;
                this.fileChangedOffset = 2;
                this.fileWatcher.EnableRaisingEvents = true;
                this.InputParameter.MotorCtr_CanAutoBalance = true;
                this.motorCtr_UnbalanceController = new MotorUnbalanceController(setting.UnbalanceSetting);
                this.OutputParameter.UpdateAppStatus("Enable Automatic Balance Function");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Start Automatic Balance error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private bool Can_MotorCtr_StopAutoBalance()
        {
            return this.OutputParameter.IsWorking && this.InputParameter.MotorCtr_CanAutoBalance;
        }

        private void Exe_MotorCtr_StopAutoBalance()
        {
            try
            {
                this.fileWatcher.EnableRaisingEvents = true;
                this.InputParameter.MotorCtr_CanAutoBalance = false;
                this.OutputParameter.UpdateAppStatus("Disable Automatic Balance Function");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Start Automatic Balance error", MessageBoxButton.OK, MessageBoxImage.Information);
            }            
        }

        #endregion
        #endregion

        #region SubMethod
        private void MotorCtr_NotifyError(string message, string title)
        {
            var type = ErrorNoticicationTypes.MessageBox;

            if (this.InputParameter.CurrentConfig.CurrentSetting != null)
            {
                type = this.InputParameter.CurrentConfig.CurrentSetting.NotificationType;
            }

            switch (type)
            {
                case ErrorNoticicationTypes.AppStatus:                    
                    this.OutputParameter.UpdateAppStatus(message, AppStatusTypes.Error);
                    break;

                case ErrorNoticicationTypes.MessageBox:
                default:
                    MessageBox.Show(message, title, MessageBoxButton.OK, MessageBoxImage.Information);
                    break;
            }
        }

        private string MotorCtr_GetDeviceStatusMessage(uint status)
        {
            var messages = new List<string>();
            foreach (MotorCtrStatusTypes item in Enum.GetValues(typeof(MotorCtrStatusTypes)))
            {
                if ((status & (uint)item) != 0)
                {
                    switch (item)
                    {
                        case MotorCtrStatusTypes.Available:
                           messages.Add("available");
                            break;
                        case MotorCtrStatusTypes.TransDone:
                           messages.Add("action transmision is done");
                            break;
                        case MotorCtrStatusTypes.TransError:
                           messages.Add("action package lost during transmition");
                            break;
                        case MotorCtrStatusTypes.OperDone:
                           messages.Add("system operation is done");
                            break;
                        case MotorCtrStatusTypes.OperError:
                           messages.Add("system operation has error");
                            break;
                        case MotorCtrStatusTypes.HasActions:
                           messages.Add("action is available");
                            break;
                        case MotorCtrStatusTypes.NoActions:
                           messages.Add("there is no action");
                            break;
                        case MotorCtrStatusTypes.OperWorking:
                           messages.Add("system is working");
                            break;
                        case MotorCtrStatusTypes.NoOperWorking:
                           messages.Add("system is not working");
                            break;
                        case MotorCtrStatusTypes.SampleOn:
                           messages.Add("data sample is enabled");
                            break;
                        case MotorCtrStatusTypes.SampleOff:
                           messages.Add("data sample is disable");
                            break;
                        default:
                           messages.Add("undefined Status");
                            break;
                    }
                }
            }
            return "Device status: " + string.Join(", ", messages);
        }

        private async Task MorotCtr_TransmitBasicCommandAsync(IBuffer buffer, bool canCheck)
        {            
            //Check => hasResponse muss be false => Invert Value
            this.hasResponse = !canCheck;
            WriteOut(buffer);
            if (canCheck)
            {
                await Task.Delay(MotorCtr_WaitResponse);
                MotorCtr_CheckResponsePackage();
            }
        }

        private void MotorCtr_CheckResponsePackage()
        {
            if (this.hasResponse && this.lastResponse is MotorCtrStatusPackage)
            {
                var package = this.lastResponse as MotorCtrStatusPackage;
                var message = MotorCtr_GetDeviceStatusMessage(package.Status);
                PcActionHelper.DoAction(package.PcAction, message);
            }
            else
            {
                throw new Exception("Device is not available: device is off, or no response has been received");
            }
        }

        private List<RfPackage> MotorCtr_GetActionTransPackage(byte endTransCommand, ushort delay)
        {
            var collection = new List<RfPackage>();
            var input = new List<MotorCtrMotorAction>();
            input.AddRange(this.OutputParameter.MotorCtrContainer[0]);
            input.AddRange(this.OutputParameter.MotorCtrContainer[1]);

            int rfAmount = input.Count / 2;
            int index = 0;
            if ((input.Count % 2) != 0)
            {
                rfAmount++;
            }

            //Add Action 2 per RF Package
            for (int i = 0; i < rfAmount; i++)
            {
                using (var stream = new System.IO.MemoryStream())
                using (var writer = new System.IO.BinaryWriter(stream))
                {
                    for (int k = 0; k < 2; k++)
                    {
                        if (input.Count > index)
                        {
                            writer.Write(input[index].GetBytes());                            
                        }
                        else
                        {
                            writer.Write(MotorCtrMotorAction.GetEmptyBytes());
                        }
                        index++;
                    }
                    collection.Add(new RfPackage((byte)MicroTasks.MotorCtr, (byte)MotorCtrTypes.Action, stream.ToArray()));
                }
            }

            //Inserver StartTrans at top
            collection.Insert(0, new RfPackage((byte)MicroTasks.MotorCtr, (byte)MotorCtrTypes.StartTrans));

            //Add EndTrans
            var endPayload = new MotorCtrEndTransAction((byte)input.Count, endTransCommand, delay);
            collection.Add(new RfPackage((byte)MicroTasks.MotorCtr, (byte)MotorCtrTypes.EndTrans, endPayload.GetBytes()));
            return collection;
        }

        private void MotorCtr_OnFileChanged(object parameter)
        {
            var input = parameter as string;
            if (string.IsNullOrEmpty(input))
            {
                return;
            }
            var elements = input.Split('\t');
            double unBalance = 0;
            double angle = 0;
            var hasInput = false;
            if (elements.Length >= 2 && double.TryParse(elements[0], NumberStyles.Any, CultureInfo.InvariantCulture, out unBalance)
                                     && double.TryParse(elements[1], NumberStyles.Any, CultureInfo.InvariantCulture, out angle))
            {
                hasInput = true;
            }

            if (hasInput)
            {
                this.motorCtr_UnbalanceController.Calculate(unBalance, angle);

                //Update in UI Thread
                App.Current.Dispatcher.Invoke(() =>
                {
                    MotorCtr_UpdateControllStatus(unBalance, angle);
                });
            }
        }

        private async void MotorCtr_UpdateControllStatus(double unBalance, double angle)
        {
            if (this.motorCtr_UnbalanceController.IsOn)
            {
                var updatePara = false;                
                if (this.motorCtr_UnbalanceController.CanInvert)
                {
                    var isBoth = MotorCtr_DoAutoBalanceAction(this.motorCtr_UnbalanceController, true);
                    //time for voltage to recover after command
                    await Task.Delay(500);
                    //both motor are enable
                    if (isBoth)
                    {
                        await Task.Delay(this.InputParameter.MotorCtr_MoveBothDelay);
                    }

                    //reset, select next method
                    this.motorCtr_UnbalanceController.CanInvert = false;
                    if (this.motorCtr_UnbalanceController.Method == MotorAutoMethods.Motor2Neg)
                    {
                        this.motorCtr_UnbalanceController.Method = MotorAutoMethods.SamePos;
                        updatePara = true;
                    }
                    else
                    {
                        this.motorCtr_UnbalanceController.Method++;
                    }
                }

                MotorCtr_DoAutoBalanceAction(this.motorCtr_UnbalanceController);

                //a period of method is passed => update parameters
                if (updatePara)
                {
                    this.motorCtr_UnbalanceController.Pulse = (ushort)(MotorUnbalanceController.NewPulsePercent * this.motorCtr_UnbalanceController.Pulse);                    
                }
            }

            //Show Controller Parameter
            this.OutputParameter.MotorCtr_AutoBalanceStatus = string.Format("Unbalance: \t{0}\n, Angle: \t\t{1}\n, IsOn: \t\t{2}\n, Method: \t{3}\n, Pulse: \t\t{4}",
                                                                            unBalance.ToString(App.NumberFormat, CultureInfo.InvariantCulture),
                                                                            angle.ToString(App.NumberFormat, CultureInfo.InvariantCulture),
                                                                            this.motorCtr_UnbalanceController.IsOn,
                                                                            this.motorCtr_UnbalanceController.Method,
                                                                            this.motorCtr_UnbalanceController.Pulse);
        }

        private bool MotorCtr_DoAutoBalanceAction(MotorUnbalanceController controller, bool isInvert = false)
        {
            bool isBoth = false;

            switch (controller.Method)
            {
                case MotorAutoMethods.Motor1Pos:
                    if (isInvert)
                    {
                        this.MotorCtr_MoveMotor1Right.TryToExecute(controller.Pulse);
                    }
                    else
                    {
                        this.MotorCtr_MoveMotor1Left.TryToExecute(controller.Pulse);
                    }                   
                    break;

                case MotorAutoMethods.Motor1Neg:
                    if (isInvert)
                    {
                        this.MotorCtr_MoveMotor1Left.TryToExecute(controller.Pulse);
                    }
                    else
                    {
                        this.MotorCtr_MoveMotor1Right.TryToExecute(controller.Pulse);
                    }                    
                    break;

                case MotorAutoMethods.Motor2Pos:
                    if (isInvert)
                    {
                        this.MotorCtr_MoveMotor2Right.TryToExecute(controller.Pulse);
                    }
                    else
                    {
                        this.MotorCtr_MoveMotor2Left.TryToExecute(controller.Pulse);
                    }
                    break;

                case MotorAutoMethods.Motor2Neg:
                    if (isInvert)
                    {
                        this.MotorCtr_MoveMotor2Left.TryToExecute(controller.Pulse);
                    }
                    else
                    {
                        this.MotorCtr_MoveMotor2Right.TryToExecute(controller.Pulse);
                    }                    
                    break;

                case MotorAutoMethods.SamePos:
                default:
                    this.InputParameter.MotorCtr_Motor1Move.IsForward = (isInvert) ? false : true;
                    this.InputParameter.MotorCtr_Motor2Move.IsForward = (isInvert) ? true  : false;
                    this.InputParameter.MotorCtr_Motor1Move.Pulse = this.InputParameter.MotorCtr_Motor2Move.Pulse = controller.Pulse;
                    this.MotorCtr_MoveBothMotor.TryToExecute(this.InputParameter);
                    isBoth = true;
                    break;
            }

            return isBoth;
        }
        #endregion
    }
}
