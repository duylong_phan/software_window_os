﻿using LongModel.ViewModels;
using LongWpfUI.Windows;
using MicroControllerApp.Interfaces;
using MicroControllerApp.Models;
using MicroControllerApp.Models.Base;
using MicroControllerApp.Models.DataClasses.Packages;
using MicroControllerApp.Models.DataClasses.Settings;
using MicroControllerApp.Models.DataClasses.Settings.Others;
using MicroControllerApp.ViewModels.Helpers;
using MicroControllerApp.Views.Dialogs;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MicroControllerApp.ViewModels
{
    public partial class MVController : ViewModel<MainWindow, InputParameters, OutputParameters, CommandInfoContainer, ValidationRuleContainer>
    {
        #region Local
        private void uBalance_InitializeSW()
        {
            var setting = this.InputParameter.CurrentConfig.CurrentSetting;
            if (setting != null)
            {
                Setting_InitializeSW(setting);
            }
        }

        private void uBalance_InitializeHW()
        {
            var setting = this.InputParameter.CurrentConfig.CurrentSetting;
            if (setting != null)
            {
                Setting_InitializeHW(setting);
            }
        }

        private void uBalance_Update()
        {
            var setting = this.InputParameter.CurrentConfig.CurrentSetting;
            DataPackage package = null;

            while (this.packageQueue.Count > 0)
            {
                this.packageQueue.TryDequeue(out package);
                if (package is ISharedPackage)
                {
                    Setting_Update(setting, package as ISharedPackage);
                }
                else if (package is uBalanceStatusPackage)
                {
                    bool isOk = false;
                    var uBalancePackage = package as uBalanceStatusPackage;
                    //Notify user
                    var message = uBalance_TranslateStatus(uBalancePackage, out isOk);
                    uBalance_NofityStatus(message, isOk);

                    //Check Operation Status
                    if (uBalancePackage.Status == uBalanceTypes.ActionWorking)
                    {
                        this.hasResponse = false;
                    }
                    else
                    {
                        this.hasResponse = true;
                    }                    
                }
            }
        }       

        private void uBalance_DisposeSW()
        {
            var setting = this.InputParameter.CurrentConfig.CurrentSetting;
            if (setting != null)
            {
                Setting_DisposeSW(setting);
            }
        }

        private void uBalance_DisposeHW()
        {
            var setting = this.InputParameter.CurrentConfig.CurrentSetting;
            if (setting != null)
            {
                Setting_DisposeHW(setting);
            }
        }
        #endregion

        #region Remote
        private void uBalance_OnRequested(HttpListenerRequest request, HttpListenerResponse response)
        {

        }

        private void uBalance_OnResponsed(string suffix, byte[] buffer)
        {

        }
        #endregion

        #region Command

        #region Action Editor
        private bool Can_uBalance_Help()
        {
            return true;
        }

        private void Exe_uBalance_Help()
        {
            NotifyNoFunction();
        }

        private bool Can_uBalance_AddAction(object parameter)
        {
            return parameter is uBalanceActionContainer;
        }

        private void Exe_uBalance_AddAction(object parameter)
        {
            try
            {
                var container = parameter as uBalanceActionContainer;
                var action = new uBalanceChannelAction();
                action.Channel = container.Channel;
                SetViewProperties(action);
                var window = new uBalanceActionSettingWindow(this.View, action, true, this.uBalance_Help);
                if (window.ShowDialog() == true)
                {
                    container.Add(action);
                    action.CalculateProperty();
                    this.OutputParameter.UpdateAppStatus("uBalance Action was added");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Add Action Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private bool Can_uBalance_EditAction(object parameter)
        {
            return parameter is uBalanceChannelAction;
        }

        private void Exe_uBalance_EditAction(object parameter)
        {
            try
            {
                var action = parameter as uBalanceChannelAction;
                SetViewProperties(action);
                var window = new uBalanceActionSettingWindow(this.View, action, false, this.uBalance_Help);
                if(window.ShowDialog() == true)
                {
                    action.CalculateProperty();
                    this.OutputParameter.UpdateAppStatus("uBalance Action was edited");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Edit Action Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        
        private bool Can_uBalance_RemoveAction(object parameter)
        {
            return parameter is uBalanceChannelAction;
        }

        private void Exe_uBalance_RemoveAction(object parameter)
        {
            try
            {                
                var action = parameter as uBalanceChannelAction;
                uBalanceActionContainer container = null;
                foreach (var item in this.OutputParameter.uBalanceContainers)
                {
                    if (item.Contains(action))
                    {
                        container = item;
                        break;
                    }
                }
                if (container == null)
                {
                    throw new Exception("Selected Action cannot be found");
                }
                var result = MessageBox.Show("Do you really want to remove this action", "Remove Action", MessageBoxButton.YesNo, MessageBoxImage.Information);
                if (result == MessageBoxResult.Yes)
                {
                    container.Remove(action);
                    this.OutputParameter.UpdateAppStatus("uBalance Action was removed");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Remove Action Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private bool Can_uBalance_DownAction(object parameter)
        {
            return parameter is uBalanceChannelAction;
        }

        private void Exe_uBalance_DownAction(object parameter)
        {
            try
            {
                var action = parameter as uBalanceChannelAction;
                uBalanceActionContainer container = null;
                int index = -1;
                foreach (var item in this.OutputParameter.uBalanceContainers)
                {
                    if (item.Contains(action))
                    {
                        container = item;
                        index = container.IndexOf(action);
                        break;
                    }
                }

                if (container != null && index >= 0 && index < (container.Count - 1))
                {
                    index++;
                    container.Remove(action);
                    container.Insert(index, action);
                    this.View.UpdatePackageListBoxSelectedIndex(index);
                }
                else
                {
                    this.OutputParameter.UpdateAppStatus("Action can't be moved");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Move Action down Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        
        private bool Can_uBalance_UpAction(object parameter)
        {
            return parameter is uBalanceChannelAction;
        }

        private void Exe_uBalance_UpAction(object parameter)
        {
            try
            {
                var action = parameter as uBalanceChannelAction;
                uBalanceActionContainer container = null;
                int index = -1;
                foreach (var item in this.OutputParameter.uBalanceContainers)
                {
                    if (item.Contains(action))
                    {
                        container = item;
                        index = container.IndexOf(action);
                        break;
                    }
                }

                if (container != null && index > 0 && index < container.Count)
                {
                    index--;
                    container.Remove(action);
                    container.Insert(index, action);
                    this.View.UpdatePackageListBoxSelectedIndex(index);
                }
                else
                {
                    this.OutputParameter.UpdateAppStatus("Action can't be moved");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Move Action up Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        #endregion

        #region File
        private bool Can_uBalance_LoadActions()
        {
            return true;     
        }

        private async void Exe_uBalance_LoadActions()
        {
            try
            {
                var dialog = new OpenFileDialog();
                dialog.Filter = uBalanceSetting.ActionFilter;
                if (dialog.ShowDialog() == true)
                {
                    await Task.Factory.StartNew(() =>
                    {
                        using (var stream = System.IO.File.OpenRead(dialog.FileName))
                        {
                            uBalanceHelper.LoadFile(stream, this.OutputParameter.uBalanceContainers);
                        }
                    });

                    //Explicit update request
                    foreach (var item in this.OutputParameter.uBalanceContainers)
                    {
                        //Update Item on ListBox
                        item.RequestUpdateEvent();

                        //Update Item Property
                        foreach (var subItem in item)
                        {
                            subItem.CalculateProperty();
                        }
                    }
                    this.OutputParameter.UpdateAppStatus("uBalance Actions file was loaded");                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Load Action File error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private bool Can_uBalance_SaveActions()
        {
            return true;
        }

        private async void Exe_uBalance_SaveActions()
        {
            try
            {
                var dialog = new SaveFileDialog();
                dialog.Filter = uBalanceSetting.ActionFilter;
                if (dialog.ShowDialog() == true)
                {
                    await Task.Factory.StartNew(() =>
                    {
                        using (var stream = System.IO.File.Create(dialog.FileName))
                        {
                            uBalanceHelper.SaveFile(stream, this.OutputParameter.uBalanceContainers);
                        }
                    });                    
                    this.OutputParameter.UpdateAppStatus("uBalance Actions file was saved");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Save Action File error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        #endregion

        #region Operation
        private bool Can_uBalance_GetStatus()
        {
            return this.OutputParameter.IsWorking;
        }

        private async void Exe_uBalance_GetStatus()
        {
            try
            {
                this.hasResponse = false;
                var package = new RfPackage((byte)MicroTasks.uBalance, (byte)uBalanceTypes.Ping, new byte[0]);
                WriteOut(package.GetBytes());
                await Task.Delay(200);
                if (!this.hasResponse)  //NOT
                {
                    throw new Exception("uBalance System is not available");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "uBalance System Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }        

        private bool Can_uBalance_StartTrans()
        {
            return this.OutputParameter.IsWorking;
        }

        private async void Exe_uBalance_StartTrans()
        {
            try
            {
                #region Check Status
                this.hasResponse = false;
                var package = new RfPackage((byte)MicroTasks.uBalance, (byte)uBalanceTypes.Ping, new byte[0]);
                WriteOut(package.GetBytes());
                await Task.Delay(100);
                if (!this.hasResponse)  //NOT
                {
                    throw new Exception("uBalance System is not available, or during operation");
                }
                #endregion

                #region Transmit Package
                //only when system is available, and free
                //Get Actions Collection
                var input = new List<uBalanceChannelAction>();
                foreach (var item in this.OutputParameter.uBalanceContainers)
                {
                    input.AddRange(item);
                }
                if (input.Count == 0)
                {
                    throw new Exception("No actions are available. Please create actions");
                }
                //Convert to byte
                var output = uBalanceHelper.GetActionsPackages(input);
                
                ProgressingWindow.ShowDialog(this.View, "Transmitting action...");
                await Task.Delay(100);
                await Task.Factory.StartNew(() =>
                {
                    foreach (var item in output)
                    {
                        WriteOut(item);
                        Task.Delay(50).Wait();
                    }
                });
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Transmit actions error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            ProgressingWindow.CloseDialog();
        }

        private bool Can_uBalance_StartOper()
        {
            return this.OutputParameter.IsWorking;
        }

        private void Exe_uBalance_StartOper()
        {
            try
            {
                var package = new RfPackage((byte)MicroTasks.uBalance, (byte)uBalanceTypes.StartOper, new byte[0]);
                WriteOut(package.GetBytes());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Start operation error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private bool Can_uBalance_StopOper()
        {
            return this.OutputParameter.IsWorking;
        }

        private void Exe_uBalance_StopOper()
        {
            try
            {
                var package = new RfPackage((byte)MicroTasks.uBalance, (byte)uBalanceTypes.StopOper, new byte[0]);
                WriteOut(package.GetBytes());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Start operation error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        #endregion

        #endregion

        #region Sub
        private int uBalance_GetActionCount()
        {
            int count = 0;

            foreach (var item in this.OutputParameter.uBalanceContainers)
            {
                count += item.Count;
            }

            return count;
        }

        private string uBalance_TranslateStatus(uBalanceStatusPackage package, out bool isOk)
        {
            string message = string.Empty;
            isOk = true;

            switch (package.Status)
            {
                case uBalanceTypes.TransDone:
                    message = "Actions were transmitted successfully to uBalance System";
                    break;

                case uBalanceTypes.TransError:
                    message = "Transmision of Action failed";
                    isOk = false;
                    break;

                case uBalanceTypes.OperDone:
                    message = "uBalance Operation was done";
                    break;

                case uBalanceTypes.OperError:
                    message = "There was an error during operation";
                    isOk = false;
                    break;

                case uBalanceTypes.NoActions:
                    message = "No actions have been transmited to uBalance System";
                    isOk = false;
                    break;

                case uBalanceTypes.ActionWorking:
                    message = "uBalance system is working";
                    break;

                case uBalanceTypes.Ping:
                default:
                    message = "uBalance system is available";
                    break;
            }

            return message;
        }

        private async void uBalance_NofityStatus(string message, bool isOk)
        {
            //Avoid blocking DispatcherTimer
            await Task.Delay(100);
            if (isOk)
            {
                this.OutputParameter.UpdateAppStatus(message);
            }
            else
            {
                MessageBox.Show(message, "uBalance System Status", MessageBoxButton.OK, MessageBoxImage.Warning);
            }            
        }

        #endregion
    }
}
