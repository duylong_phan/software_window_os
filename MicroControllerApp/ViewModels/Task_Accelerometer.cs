﻿using LongModel.ViewModels;
using MicroControllerApp.Interfaces;
using MicroControllerApp.Models;
using MicroControllerApp.Models.Base;
using MicroControllerApp.Models.DataClasses.Packages;
using MicroControllerApp.Models.DataClasses.Settings;
using MicroControllerApp.Models.ViewClasses.Graphics;
using MicroControllerApp.ViewModels.Helpers;
using OxyPlot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.ViewModels
{
    public partial class MVController : ViewModel<MainWindow, InputParameters, OutputParameters, CommandInfoContainer, ValidationRuleContainer>
    {
        //setting.SetLocalAction(XXX_InitializeSW, XXX_InitializeHW, XXX_Update, XXX_DisposeSW, XXX_DisposeHW);
        //setting.SetRemoteAction(XXX_OnRequested, XXX_OnResponsed);

        #region Local
        private void Acc_InitializeSW()
        {
            var setting = this.InputParameter.CurrentConfig.CurrentSetting as AccSetting;
            Setting_InitializeSW(setting);
            this.currentTime = 0;
        }

        private void Acc_InitializeHW()
        {
            var setting = this.InputParameter.CurrentConfig.CurrentSetting as AccSetting;
            Setting_InitializeHW(setting);
        }

        private void Acc_Update()
        {
            if (this.packageQueue.Count == 0)
            {
                return;
            }

            #region Get Collection
            var setting = this.InputParameter.CurrentConfig.CurrentSetting as AccSetting;
            var sampleCollection = new List<AccSamplePackage>();
            DataPackage package = null;
            while (this.packageQueue.Count > 0)
            {
                this.packageQueue.TryDequeue(out package);
                if (package is ISharedPackage)
                {
                    Setting_Update(setting, package as ISharedPackage);
                }
                else if (package is AccSamplePackage)
                {
                    sampleCollection.Add(package as AccSamplePackage);
                }
            }
            #endregion

            #region Update View
            //Take the last Sample
            if (package is AccSamplePackage)
            {
                var accSamplePackage = package as AccSamplePackage;
                this.OutputParameter.Accelerometer1 = accSamplePackage.Samples[0];
                this.OutputParameter.Accelerometer2 = accSamplePackage.Samples[1];
                this.OutputParameter.Accelerometer3 = accSamplePackage.Samples[2];
                this.OutputParameter.Accelerometer4 = accSamplePackage.Samples[3];
            }

            //Add Sample into graphic
            if (sampleCollection.Count > 0)
            {
                var pointCollection = new List<DataPoint>[AccSamplePackage.AccAmount * AccSamplePackage.AxisAmount];
                var curves = new DataCurve[pointCollection.Length];
                for (int i = 0; i < pointCollection.Length; i++)
                {
                    pointCollection[i] = new List<DataPoint>();
                    curves[i] = this.OutputParameter.PlotModel.Series[i] as DataCurve;
                }

                //go throung each sample
                foreach (var item in sampleCollection)
                {                    
                    for (int i = 0, k = 0; i < AccSamplePackage.AccAmount; i++, k += 3)
                    {
                        pointCollection[k + 0].Add(new DataPoint(this.currentTime, item.Samples[i].X));
                        pointCollection[k + 1].Add(new DataPoint(this.currentTime, item.Samples[i].Y));
                        pointCollection[k + 2].Add(new DataPoint(this.currentTime, item.Samples[i].Z));
                    }
                    this.currentTime += this.samplePeriod;
                }

                //Check overflow
                int remain = setting.GraphicSetting.PointAmount - curves[0].Points.Count;
                int overflow = pointCollection[0].Count - remain;
                if (overflow > 0)
                {
                    for (int i = 0; i < curves.Length; i++)
                    {
                        curves[i].Points.RemoveRange(0, overflow);
                    }
                }
                //add Point
                for (int i = 0; i < curves.Length; i++)
                {
                    curves[i].Points.AddRange(pointCollection[i]);
                }

                //View is available
                if (this.OutputParameter.HasView(AppViews.Graphic))
                {
                    this.OutputParameter.UpdateGraphic();
                }
            }
            #endregion

            #region Mat
            if (this.matFileHandler != null && this.matFileHandler.IsEnabled)
            {
                foreach (var item in sampleCollection)
                {
                    var container = new double[AccSamplePackage.AccAmount * AccSamplePackage.AxisAmount + 1];
                    container[0] = (DateTime.Now.Ticks - this.startTick) / (double)TimeSpan.TicksPerMillisecond;

                    for (int i = 0; i < AccSamplePackage.AccAmount; i++)
                    {
                        container[i * 3 + 1] = item.Samples[i].X;
                        container[i * 3 + 2] = item.Samples[i].Y;
                        container[i * 3 + 3] = item.Samples[i].Z;
                    }
                    this.matFileHandler.Add(container);
                }
                this.OutputParameter.MatSampleAmount = this.matFileHandler.SampleAmount;
            }
            #endregion
        }

        private void Acc_DisposeSW()
        {
            Setting_DisposeSW(this.InputParameter.CurrentConfig.CurrentSetting as SettingBase);
        }

        private void Acc_DisposeHW()
        {
            Setting_DisposeHW(this.InputParameter.CurrentConfig.CurrentSetting as SettingBase);
        }
        #endregion

        #region Remote
        private void Acc_OnRequested(HttpListenerRequest request, HttpListenerResponse respone)
        {
            throw new NotImplementedException();
        }

        private void Acc_OnResponsed(string suffix, byte[] buffer)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
