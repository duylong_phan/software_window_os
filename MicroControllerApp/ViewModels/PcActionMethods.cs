﻿using LongModel.ViewModels;
using MicroControllerApp.Models;
using MicroControllerApp.Models.DataClasses.Settings;
using MicroControllerApp.ViewModels.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MicroControllerApp.ViewModels
{
    public partial class MVController : ViewModel<MainWindow, InputParameters, OutputParameters, CommandInfoContainer, ValidationRuleContainer>
    {
        #region Initialize
        private void Initialize_PcActionHelper()
        {
            PcActionHelper.ClearActions();
            PcActionHelper.AddAction((uint)PcActions.AppStatus, PcAction_NotifyAppStatus);
            PcActionHelper.AddAction((uint)PcActions.InfoMessage, PcAction_NotifyInfoMessage);
            PcActionHelper.AddAction((uint)PcActions.WarningMessage, PcAction_NotifyWarningMessage);
            PcActionHelper.AddAction((uint)PcActions.ErrorMessage, PcAction_NotifyErrorMessage);
        }
        #endregion

        #region Sub Action
        private void PcAction_NotifyAppStatus(object parameter)
        {
            if (parameter is string)
            {
                this.View.UpdateAppStatusColor(AppStatusTypes.Normal);
                this.OutputParameter.UpdateAppStatus(parameter as string);
            }
        }

        private void PcAction_NotifyInfoMessage(object parameter)
        {
            if (parameter is string)
            {
                this.View.UpdateAppStatusColor(AppStatusTypes.Error);
                this.OutputParameter.UpdateAppStatus(parameter as string, AppStatusTypes.Error);
            }
        }

        private void PcAction_NotifyWarningMessage(object parameter)
        {
            if (parameter is string)
            {
                MessageBox.Show(parameter as string, "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void PcAction_NotifyErrorMessage(object parameter)
        {
            if (parameter is string)
            {
                MessageBox.Show(parameter as string, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        #endregion
    }
}
