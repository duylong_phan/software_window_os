﻿using LongModel.Helpers;
using LongModel.ViewModels;
using LongModel.Views.ControlModels;
using LongWpfUI.SystemHelper;
using MicroControllerApp.Interfaces;
using MicroControllerApp.Models;
using MicroControllerApp.Models.Base;
using MicroControllerApp.Models.DataClasses.ItemInfos;
using MicroControllerApp.Models.DataClasses.Packages;
using MicroControllerApp.Models.DataClasses.Settings;
using MicroControllerApp.Models.DataClasses.Settings.Others;
using MicroControllerApp.Models.DataClasses.Settings.Sub;
using MicroControllerApp.Models.IO.Handlers;
using MicroControllerApp.Models.ViewClasses;
using MicroControllerApp.Models.ViewClasses.Graphics;
using MicroControllerApp.Models.ViewClasses.Infos;
using MicroControllerApp.ViewModels.Helpers;
using MicroControllerApp.Views.Dialogs;
using OxyPlot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MicroControllerApp.ViewModels
{
    public partial class MVController : ViewModel<MainWindow, InputParameters, OutputParameters, CommandInfoContainer, ValidationRuleContainer>
    {
        #region Local
        private void Press_InitializeSW()
        {
            var setting = this.InputParameter.CurrentConfig.CurrentSetting as PressSetting;
                        
            Setting_InitializeSW(setting);
            this.pressFileHandler = new PressFileHandler(setting.PressFileSetting);
            this.pressFileHandler.Start();
        }

        private void Press_InitializeHW()
        {
            var setting = this.InputParameter.CurrentConfig.CurrentSetting as PressSetting;
            Setting_InitializeHW(setting);
        }

        private void Press_Update()
        {
            if (this.packageQueue.Count == 0)
            {
                return;
            }

            var setting = this.InputParameter.CurrentConfig.CurrentSetting as PressSetting;
            var pressCollection = new List<PressInfo>();
            var sampleCollection = new List<double[]>();
            DataPackage package = null;

            #region Get Collection
            while (this.packageQueue.Count > 0)
            {
                this.packageQueue.TryDequeue(out package);
                if(package is ISharedPackage)
                {
                    Setting_Update(setting, package as ISharedPackage);
                }
                else if (package is PressInfoPackage)
                {
                    var rawPackage = package as PressInfoPackage;
                    pressCollection.Add(new PressInfo(rawPackage.ID, rawPackage.Amount, DateTime.Now));
                }
                else if(package is PressSamplePackage)
                {
                    var rawPackage = package as PressSamplePackage;
                    for (int i = 0; i < PressSamplePackage.ArrayLength; i++)
                    {
                        sampleCollection.Add(new double[] { currentTime, rawPackage.MagD[i], rawPackage.Force[i] });
                        currentTime += samplePeriod;
                    }
                }  
                else if(package is PressStatusPackage)
                {
                    var statusPackage = package as PressStatusPackage;
                    this.hasResponse = true;
                    PcActionHelper.DoAction(statusPackage.PcCommand, Press_GetDeviceStatusMessage(statusPackage.ID, statusPackage.Status));
                }                

                //Server support
                if (setting.ServerSetting.IsEnabled && setting.Port != PhysicalPorts.WebServer && package != null)
                {
                    this.serverQueue.Enqueue(package);
                }
            }
            #endregion

            #region View   

            #region Add Press 
            if (pressCollection.Count > 0)
            {
                int remain = setting.PressFileSetting.PointAmount - this.OutputParameter.PressCollection.Count;
                int overflow = pressCollection.Count - remain;
                if (overflow > 0)
                {
                    this.OutputParameter.PressCollection.RemoveRange(0, overflow);
                }
                this.OutputParameter.PressCollection.AddRange(pressCollection);
                this.OutputParameter.PressCollection.RequestUpdateEvent();

                //View is available
                if (this.OutputParameter.HasView(AppViews.PressTable))
                {
                    this.View.ScrollViewToBottom();
                }                
            }
            #endregion

            #region Add Graphic
            if (sampleCollection.Count > 0)
            {
                //Get Array
                var pointCollection = new List<DataPoint>[2];
                var curves = new DataCurve[2];
                for (int i = 0; i < 2; i++)
                {
                    pointCollection[i] = new List<DataPoint>();
                    curves[i] = this.OutputParameter.PlotModel.Series[i] as DataCurve;
                }

                //go through each Point
                foreach (var item in sampleCollection)
                {
                    //MagD
                    pointCollection[0].Add(new DataPoint(item[0], item[1]));
                    //Force
                    pointCollection[1].Add(new DataPoint(item[0], item[2]));
                }
                
                //Check overflow
                int remain = setting.GraphicSetting.PointAmount - curves[0].Points.Count;
                int overflow = pointCollection[0].Count - remain;
                if (overflow > 0)
                {
                    for (int i = 0; i < 2; i++)
                    {
                        curves[i].Points.RemoveRange(0, overflow);
                    }
                }
                //Add Point
                for (int i = 0; i < 2; i++)
                {
                    curves[i].Points.AddRange(pointCollection[i]);
                }

                //View is available
                if (this.OutputParameter.HasView(AppViews.Graphic))
                {
                    this.OutputParameter.UpdateGraphic();
                }
            }
            #endregion
            #endregion

            #region Sample in File
            if (this.pressFileHandler != null && this.pressFileHandler.IsEnabled)
            {
                foreach (var item in pressCollection)
                {
                    this.pressFileHandler.Add(item);
                }
            }

            if (this.matFileHandler != null && this.matFileHandler.IsEnabled)
            {
                foreach (var item in sampleCollection)
                {
                    this.matFileHandler.Add(item);
                }
                this.OutputParameter.MatSampleAmount = this.matFileHandler.SampleAmount;
            }
            #endregion
        }

        private void Press_DisposeSW()
        {
            var setting = this.InputParameter.CurrentConfig.CurrentSetting as PressSetting;

            Setting_DisposeSW(setting);
            if (this.pressFileHandler != null)
            {
                this.pressFileHandler.Stop();
            }
        }

        private void Press_DisposeHW()
        {
            var setting = this.InputParameter.CurrentConfig.CurrentSetting as PressSetting;
            Setting_DisposeHW(setting);
        }
        #endregion

        #region Remote
        private void Press_OnRequested(HttpListenerRequest request, HttpListenerResponse response)
        {
            try
            {
                //Test Connection
                if (request.RawUrl.EndsWith(WebHelper.Req_Ping))
                {
                    Press_OnRequested_Ping(response);
                }
                //Request Information about Press File
                else if (request.RawUrl.EndsWith(WebHelper.Req_Info))
                {
                    Press_OnRequested_Info(request, response);
                }
                //Request Press File
                else if (request.RawUrl.Length > 1)
                {
                    Press_OnRequested_File(request, response);
                }
                //Request Debug DataPackage
                else
                {
                    Press_OnRequested_Debug(response);
                }
            }
            catch (Exception)
            {
                //assume Server work correctly, Error only from Client request => Ignore
            }
        }

        #region Sub Requested
        private void Press_OnRequested_Ping(HttpListenerResponse response)
        {
            using (var stream = response.OutputStream)
            {
                var buffer = Encoding.UTF8.GetBytes(this.OutputParameter.SoftwareFullName);
                stream.Write(buffer, 0, buffer.Length);
            }
        }

        private void Press_OnRequested_Info(HttpListenerRequest request, HttpListenerResponse response)
        {            
            var setting = this.InputParameter.CurrentConfig.CurrentSetting as PressSetting;
            string info = string.Empty;

            //check and get all File
            if (System.IO.Directory.Exists(setting.PressFileSetting.Directory))
            {
                var builder = new StringBuilder();
                var files = System.IO.Directory.GetFiles(setting.PressFileSetting.Directory);
                //Get File name, and Size
                foreach (var item in files)
                {
                    var fileInfo = new System.IO.FileInfo(item);
                    if (fileInfo.Extension == PressSetting.PressExtension)
                    {
                        var tmp = new FileNameInfo(fileInfo.Name, fileInfo.Length);
                        builder.AppendLine(tmp.ToString());
                    }                    
                }
                info = builder.ToString();
            }

            //response
            var buffer = Encoding.UTF8.GetBytes(info);
            using (var stream = response.OutputStream)
            {
                stream.Write(buffer, 0, buffer.Length);
            }
        }

        private void Press_OnRequested_File(HttpListenerRequest request, HttpListenerResponse response)
        {
            var dict = WebHelper.GetParameters(request.RawUrl);
            var setting = this.InputParameter.CurrentConfig.CurrentSetting as PressSetting;
            byte[] buffer = new byte[0];

            //Check if Action == Request, FileName available
            if (dict.ContainsKey(WebHelper.Para_Action) && dict[WebHelper.Para_Action] == WebHelper.Action_Request && dict.ContainsKey(WebHelper.Para_Name))
            {                 
                var filePath = System.IO.Path.Combine(setting.PressFileSetting.Directory, dict[WebHelper.Para_Name]);                
                if (System.IO.File.Exists(filePath))
                {
                    using (var fileStream = System.IO.File.OpenRead(filePath))
                    using (var memoStream = new System.IO.MemoryStream())
                    {
                        fileStream.CopyTo(memoStream);
                        buffer = memoStream.ToArray();
                    }
                }
            }

            //response
            using (var stream = response.OutputStream)
            {
                stream.Write(buffer, 0, buffer.Length);
            }
        }

        private void Press_OnRequested_Debug(HttpListenerResponse response)
        {
            var header = new byte[] { (byte)'#', (byte)'#', (byte)'#', (byte)'#' };
            var buffer = new byte[0];

            //Get Debug Buffer
            using (var memoStream = new System.IO.MemoryStream())
            {
                while (this.serverQueue.Count > 0)
                {
                    var item = this.serverQueue.Dequeue();
                    if (item != null)
                    {
                        var tmpBuffer = item.GetBytes();                        
                        //header and Content Protocal is required
                        memoStream.Write(header, 0, header.Length);
                        memoStream.Write(tmpBuffer, 0, tmpBuffer.Length);
                    }
                }
                buffer = memoStream.ToArray();
            }

            //response
            using (var stream = response.OutputStream)
            {
                stream.Write(buffer, 0, buffer.Length);
            }
        }
        #endregion

        private void Press_OnResponsed(string suffix, byte[] buffer)
        {
            if (suffix == null || buffer == null)
            {
                return;
            }

            //only support for Debug
            for (int i = 0; i < buffer.Length; i++)
            {
                this.headerParser.Queue.Enqueue(buffer[i]);
            }
            //Important
            ProcessIn();
        }

        #endregion

        #region Command    
        #region File Reader   
        private bool Can_Press_RequestFileInfo()
        {
            return this.OutputParameter.IsWorking;
        }

        private async void Exe_Press_RequestFileInfo()
        {
            try
            {
                var setting = this.InputParameter.CurrentConfig.CurrentSetting as PressSetting;
                string url = NetworkHelper.GetHttpPrefix(setting.RemoteSetting.Address, setting.RemoteSetting.Port);

                #region Progress buffer
                var collection = new List<FileNameInfo>();
                Action<string, byte[]> action = (_suffix, _buffer) =>
                {
                    using (var stream = new System.IO.MemoryStream(_buffer))
                    using (var reader = new System.IO.StreamReader(stream))
                    {
                        string line = reader.ReadLine();
                        while (line != null)
                        {
                            var item = FileNameInfo.Parse(line);
                            item.Command = this.Press_RequestFile;
                            collection.Add(item);
                            line = reader.ReadLine();
                        }
                    }
                };
                #endregion

                #region Request server
                await Task.Factory.StartNew(() =>
                {
                    WebHelper.Request(url, WebHelper.Req_Info, action, 1000);
                });
                #endregion

                #region Show Result
                if (collection.Count > 0)
                {
                    //Notify User
                    this.OutputParameter.UpdateAppStatus("Received information from server");

                    //Show View
                    var window = new FileNameInfoWindow(this.View, collection);
                    window.ShowDialog();
                }
                else
                {
                    MessageBox.Show("No file is available on server", "Request File Inforation", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Request file Info error");
            }
        }

        private bool Can_Press_RequestFile(object parameter)
        {
            return this.OutputParameter.IsWorking && parameter is FileNameInfo;
        }

        private async void Exe_Press_RequestFile(object parameter)
        {
            try
            {
                #region Request Parameter
                var info = parameter as FileNameInfo;
                var setting = this.InputParameter.CurrentConfig.CurrentSetting as PressSetting;
                var dict = new Dictionary<string, string>();
                dict.Add(WebHelper.Para_Action, WebHelper.Action_Request);
                dict.Add(WebHelper.Para_Name, info.Name);

                string suffix = WebHelper.GetParametersText(dict);
                string url = NetworkHelper.GetHttpPrefix(setting.RemoteSetting.Address, setting.RemoteSetting.Port);
                #endregion

                #region Progress buffer
                FileContentInfo fileContent = null;
                Action<string, byte[]> action = (_suffix, _buffer) =>
                {
                    var collection = new List<PressInfo>();
                    using (var stream = new System.IO.MemoryStream(_buffer))
                    {
                        fileContent = Press_GetFileContent(stream, info.Name, false);
                    }            
                };
                #endregion

                #region Request server
                await Task.Factory.StartNew(() =>
                {
                    WebHelper.Request(url, suffix, action, 1000);
                });
                #endregion

                #region Show result
                if (fileContent != null)
                {
                    //Explicit select File Tab
                    this.View.UpdateRibbonTabIndex(MainWindow.Index_FileTab);
                    //Add Page to View
                    this.OutputParameter.DocumentPages.Add(fileContent);
                    //Notify User
                    this.OutputParameter.UpdateAppStatus("Received file from server");
                }
                else
                {
                    throw new Exception("File content is not available");
                }
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Request File Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }            
        }

        private bool Can_Press_FilterPress(object parameter)
        {
            return parameter is PressFileContentInfo;
        }

        private void Exe_Press_FilterPress(object parameter)
        {
            try
            {
                var info = parameter as PressFileContentInfo;
                SetViewProperties(info.Filter);
                var window = new PressFilterWindow(this.View, info.Filter);
                if (window.ShowDialog() == true)
                {
                    var inCollection = info.Content as List<PressInfo>;
                    var outCollection = new List<PressInfo>();

                    //in case Duration is used
                    DateTime endTime = info.Filter.EndTime;
                    if (info.Filter.UseDuration)
                    {
                        endTime = info.Filter.StartTime.AddSeconds(info.Filter.Duration);
                    }
                    
                    //check each item
                    foreach (var item in inCollection)
                    {
                        int okCount = 0;
                        int checkCount = 0;

                        if (info.Filter.UseId)
                        {
                            checkCount++;
                            if (Press_FilterCheckId(info.Filter.ID, item))
                            {
                                okCount++;
                            }
                        }
                        if (info.Filter.UseTime)
                        {
                            checkCount++;
                            if (Press_FilterCheckTime(info.Filter.StartTime, endTime, item))
                            {
                                okCount++;
                            }
                        }

                        if (okCount == checkCount)
                        {
                            outCollection.Add(item);
                        }
                    }
                    
                    Press_AssignGroup(outCollection);
                    info.FilteredContent = outCollection;
                    this.OutputParameter.UpdateAppStatus("File content was filtered");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Filter File content error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        #endregion

        #region Device Function
        private bool Can_Press_GetStatus()
        {
            return this.OutputParameter.IsWorking;
        }

        private async void Exe_Press_GetStatus()
        {
            try
            {
                var payload = PressAction.Ping;
                var rfPackage = new RfPackage((byte)MicroTasks.Press, payload.Type, payload.GetBytes());

                this.hasResponse = false;
                WriteOut(rfPackage);
                await Task.Delay(Press_WaitResponse);
                if (!this.hasResponse)
                {
                    throw new Exception("Device is not available, or device is in Low Power mode");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Get Device Status error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private bool Can_Press_ToggleDebug()
        {
            return this.OutputParameter.IsWorking;
        }

        private async void Exe_Press_ToggleDebug()
        {
            try
            {
                var payload = PressAction.ToggleDebug;
                var rfPackage = new RfPackage((byte)MicroTasks.Press, payload.Type, payload.GetBytes());

                this.hasResponse = false;
                WriteOut(rfPackage);
                await Task.Delay(Press_WaitResponse);
                if (!this.hasResponse)
                {
                    throw new Exception("Device is not available, or device is in Low Power mode");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Request Toggle Debug error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        
        private bool Can_Press_ReadSetting()
        {
            return this.OutputParameter.IsWorking;
        }

        private async void Exe_Press_ReadSetting()
        {
            try
            {                
                var collection = new List<SubSlaveSettingPackage>();
                //Package #0
                var package = await Press_RequestDeviceSettingAsync(0);
                if (package != null)
                {
                    collection.Add(package);
                }
                //Package #1
                package = await Press_RequestDeviceSettingAsync(1);
                if (package != null)
                {
                    collection.Add(package);
                }

                if (collection.Count == 2)
                {                    
                    this.OutputParameter.UpdateAppStatus("Slave Device setting is received");
                    var settingPackage = new PressSlaveSettingPackage((byte)PcCommands.Read_SlaveSetting, collection);
                    var window = new DeviceSettingEditorWindow(this.View, settingPackage, MicroTasks.Press);
                    window.ShowDialog();
                }
                else
                {
                    throw new Exception("Request setting from slave device failed");
                }                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Request Setting error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        
        private bool Can_Press_WriteSetting(object parameter)
        {
            return this.OutputParameter.IsWorking && parameter is PressSlaveSettingPackage;
        }

        private async void Exe_Press_WriteSetting(object parameter)
        {
            try
            {
                var settingPackage = parameter as PressSlaveSettingPackage;
                var window = settingPackage.View as DeviceSettingEditorWindow;
                var errorMessage = string.Empty;
                if (window.HasError(out errorMessage))
                {
                    throw new Exception(errorMessage);
                }

                var collection = settingPackage.GetTransmitPackages();               
                foreach (var item in collection)
                {
                    WriteOut(item);
                    await Task.Delay(WaitResponse);
                }

                this.OutputParameter.UpdateAppStatus("Slave device setting is written");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Write Slave Device Setting error", MessageBoxButton.OK, MessageBoxImage.Information);
            }    
        }
        #endregion

        #endregion

        #region Sub Method
        private string Press_GetDeviceStatusMessage(byte id, uint status)
        {
            var collection = new List<string>();

            foreach (PressStatusTypes item in Enum.GetValues(typeof(PressStatusTypes)))
            {
                //Find Item
                if ((status & (uint)item) != 0)
                {
                    //Get Item Message
                    switch (item)
                    {
                        case PressStatusTypes.SampleOn:
                            collection.Add("data sample is activated");
                            break;

                        case PressStatusTypes.SampleOff:
                            collection.Add("data sample is deactivated");
                            break;

                        case PressStatusTypes.Available:
                        default:
                            collection.Add("available");
                            break;
                    }
                }
            }

            var message = string.Format("Device (ID={0}): {1}", id, string.Join(", ", collection));
            return message;
        }

        private FileContentInfo Press_GetFileContent(System.IO.Stream stream, string fileName, bool isLocal)
        {
            FileContentInfo info = null;
            var collection = new List<PressInfo>();

            #region Parse File
            using (var reader = new System.IO.StreamReader(stream))
            {
                string line = reader.ReadLine();
                while (line != null)
                {
                    var pressInfo = PressInfo.Parse(line);
                    collection.Add(pressInfo);
                    line = reader.ReadLine();
                }
            }
            #endregion

            #region Get Info
            if (collection.Count > 0)
            {
                //Get filter
                DateTime startTime, endTime;
                Press_AssignGroup(collection, out startTime, out endTime);
                var filter = new PressFilterInfo(startTime, endTime);

                //reserve Functions
                var functions = new List<object>();                
                info = new PressFileContentInfo(fileName, collection, FileContentExtensions.press, this.CloseFilePage, functions, filter);

                //add supported Functions
                if (!isLocal)   //NOT
                {
                    functions.Add(new ButtonInfoPara(this.CommandInfoContainer.SaveFilePage, info));
                }
                functions.Add(new ButtonInfoPara(this.CommandInfoContainer.Press_FilterPress, info));
            }
            #endregion

            return info;
        }

        private void Press_AssignGroup(List<PressInfo> collection)
        {
            DateTime startTime;
            DateTime endTime;
            Press_AssignGroup(collection, out startTime, out endTime);
        }

        private void Press_AssignGroup(List<PressInfo> collection, out DateTime startTime, out DateTime endTime)
        {
            startTime = DateTime.Now;
            endTime = new DateTime(1, 1, 1, 1, 1, 1);

            var dict = new Dictionary<int, PressIdGroup>();
            foreach (var item in collection)
            {
                //check and assign Group
                if (!dict.ContainsKey(item.ID)) //NOT
                {
                    dict.Add(item.ID, new PressIdGroup(item.ID));
                }
                item.Group = dict[item.ID];

                //update Group Property
                dict[item.ID].Amount += item.Amount;
                if (dict[item.ID].StartTime > item.DateTime)
                {
                    dict[item.ID].StartTime = item.DateTime;
                }
                if (dict[item.ID].EndTime < item.DateTime)
                {
                    dict[item.ID].EndTime = item.DateTime;
                }

                //update File Property
                if (startTime > item.DateTime)
                {
                    startTime = item.DateTime;
                }
                if (endTime < item.DateTime)
                {
                    endTime = item.DateTime;
                }
            }
        }

        private bool Press_FilterCheckId(int id, PressInfo item)
        {
            bool isOk = false;
            if (item != null && item.ID == id)
            {
                isOk = true;
            }
            return isOk;
        }

        private bool Press_FilterCheckTime(DateTime startTime, DateTime endTime, PressInfo item)
        {
            bool isOk = false;
            if (item != null && item.DateTime >= startTime && item.DateTime <= endTime)
            {
                isOk = true;
            }
            return isOk;
        }

        private Task<SubSlaveSettingPackage> Press_RequestDeviceSettingAsync(byte index)
        {
            return Task.Factory.StartNew(() =>
            {
                //Create payload
                var payload = new PressReadSetting(index);
                var rfPackage = new RfPackage((byte)MicroTasks.Press, payload.Type, payload.GetBytes());
                SubSlaveSettingPackage package = null;

                //Send payload
                this.hasResponse = false;
                WriteOut(rfPackage);
                Task.Delay(Press_WaitResponse).Wait();

                //Check response
                if (hasResponse && this.lastResponse is SubSlaveSettingPackage)
                {
                    package = this.lastResponse as SubSlaveSettingPackage;
                    if (package.Position != index)
                    {
                        package = null;
                    }
                }

                return package;
            });
        }
        #endregion
    }
}
