﻿using LongModel.ViewModels;
using MicroControllerApp.Models;
using MicroControllerApp.Models.DataClasses.Packages;
using MicroControllerApp.Models.DataClasses.Settings;
using MicroControllerApp.Models.DataClasses.Settings.Others;
using MicroControllerApp.ViewModels.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MicroControllerApp.ViewModels
{
    public partial class MVController : ViewModel<MainWindow, InputParameters, OutputParameters, CommandInfoContainer, ValidationRuleContainer>
    {
        private void UpdateTimer_Tick(object sender, EventArgs e)
        {
            var setting = this.InputParameter.CurrentConfig.CurrentSetting;
            if (setting == null)
            {
                return;
            }

            try
            {
                setting.Update();

                if (this.InputParameter.SelectedOutPin != null)
                {
                    ushort currentValue = 0;
                    var isOk = false;
                    if (this.InputParameter.SelectedOutPin.Command == OutPinCommands.PWM ||
                        this.InputParameter.SelectedOutPin.Command == OutPinCommands.PWM_Servo ||
                        this.InputParameter.SelectedOutPin.Command == OutPinCommands.PWM_Servo_SV1270TG)
                    {
                        if (this.InputParameter.SelectedOutPin.Command == OutPinCommands.PWM)
                        {
                            currentValue = this.InputParameter.SelectedOutPin.PwmPercent;
                        }
                        else if(this.InputParameter.SelectedOutPin.Command == OutPinCommands.PWM_Servo ||
                                this.InputParameter.SelectedOutPin.Command == OutPinCommands.PWM_Servo_SV1270TG)
                        {
                            currentValue = this.InputParameter.SelectedOutPin.ServoPercent;
                        }
                        isOk = true;
                    }
                    else if(this.InputParameter.SelectedOutPin.IsFuncEnabled && this.InputParameter.SelectedOutPin.SelectedVariable != null)
                    {
                        //File is done
                        if (this.InputParameter.SelectedOutPin.ContentIndex >= this.InputParameter.SelectedOutPin.SelectedVariable.Array[0].Length)
                        {
                            this.OutPin_ResetPin.TryToExecute(this.InputParameter.SelectedOutPin);
                        }
                        //Input for PWM
                        else if (this.InputParameter.SelectedOutPin.Command == OutPinCommands.PWM_Auto)
                        {
                            this.InputParameter.SelectedOutPin.PwmPercent = (ushort)this.InputParameter.SelectedOutPin.SelectedVariable.Array[0][this.InputParameter.SelectedOutPin.ContentIndex];
                            this.InputParameter.SelectedOutPin.ContentIndex++;                            
                            isOk = true;
                        }
                        //Input for Servo
                        else if(this.InputParameter.SelectedOutPin.Command == OutPinCommands.PWM_Servo_Auto)
                        {
                            currentValue = (ushort)(OutPinHandlerSetting.ServorOriginal + this.InputParameter.SelectedOutPin.SelectedVariable.Array[0][this.InputParameter.SelectedOutPin.ContentIndex]);
                            this.InputParameter.SelectedOutPin.ServoPercent = currentValue;
                            this.InputParameter.SelectedOutPin.ContentIndex++;
                            isOk = true;
                        }  
                        //Input for Servo SV1270TG
                        else if(this.InputParameter.SelectedOutPin.Command == OutPinCommands.PWM_Servo_SV1270TG_Auto)
                        {
                            //get angle
                            currentValue = (ushort)(OutPinHandlerSetting.MaxDegree_SV1270TG + this.InputParameter.SelectedOutPin.SelectedVariable.Array[0][this.InputParameter.SelectedOutPin.ContentIndex]);
                            //get percent
                            currentValue = (ushort)(currentValue * 100.0 / OutPinHandlerSetting.MaxDegree_SV1270TG);
                            this.InputParameter.SelectedOutPin.ServoPercent = currentValue;
                            this.InputParameter.SelectedOutPin.ContentIndex++;
                            isOk = true;
                        }
                    }

                    try
                    {
                        if (isOk)
                        {
                            //only write when value is new
                            if (currentValue != _lastServoValue)
                            {
                                var package = new RawPackage((byte)PcCommands.PinWrite, this.InputParameter.SelectedOutPin.GetBytes());
                                WriteOut(package);
                                _lastServoValue = currentValue;
                            }                            

                            //Update status
                            if (this.InputParameter.SelectedOutPin.IsFuncEnabled && this.InputParameter.SelectedOutPin.SelectedVariable != null)
                            {
                                this.InputParameter.SelectedOutPin.AutoProgressPercent = this.InputParameter.SelectedOutPin.ContentIndex * 100 / this.InputParameter.SelectedOutPin.SelectedVariable.Array[0].Length;
                            }
                        }
                    }
                    catch (Exception) { }   //ignore
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error in Setting Update Method", MessageBoxButton.OK, MessageBoxImage.Information);
                this.StopOper.TryToExecute();
            }
        }
    }
}
