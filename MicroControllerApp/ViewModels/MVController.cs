﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Collections.Concurrent;

using LongModel.ViewModels;
using MicroControllerApp.Models;
using MicroControllerApp.ViewModels.Helpers;
using LongModel.ViewModels.Logics;
using LongWpfUI.Models.Commands;
using LongModel.Models.IO.Files;
using MicroControllerApp.Models.DataClasses.Settings;
using MicroControllerApp.Models.DataClasses.Settings.Sub;
using MicroControllerApp.Models.Base;
using MicroControllerApp.Models.IO;
using MicroControllerApp.Models.IO.Handlers;
using MicroControllerApp.Models.DataClasses;
using System.Windows.Threading;
using LongWpfUI.Models.Net;
using LongModel.Collections;
using MicroControllerApp.Models.DataClasses.Settings.Others;
using MicroControllerApp.Models.DataClasses.Packages;
using MicroControllerApp.Models.ViewClasses.Infos;
using MicroControllerApp.ViewModels.Logics;

namespace MicroControllerApp.ViewModels
{
    /*
    Add new Task:
    1. Add PcCommands in Models/DataClasses/MicroTask
    2. Add SettingBase SubClass
    3. Add DataPackage SubClass    
    4. Add MicroTaskOption in InputParameter
    5. Add Task_XXX In ViewModels Folder
    6. Add Action in Task_XXX, and implement after
    7. Assign Action in BasicCommand/AssignTaskAction
    8. Add Support View in BasicCommand/AssignTaskView
    9. Add View Template in MainWindow.xaml, also for Selector
    10. Add new Template with TabControl in ConfigWindow.xaml, also for Selector
    11. Add Property, Task in Configuration/CurrentSetting, and Initialize
    12. Add Task View Content in MVController/SetViewProperties, for object and SettingBase Method    
    13. Assign Template for Config
    14. Assign Template for ViewOption in ViewTemplateSelector      
    15. Assign Setting_Ini/Dis function for XXX_Task
    */

    public partial class MVController : ViewModel<MainWindow, InputParameters, OutputParameters, CommandInfoContainer, ValidationRuleContainer>
    {
        #region Const
        public const int UpdateInterval = 50;       
        public const int WaitResponse = 200;

        public const int Press_WaitResponse = 200;
        public const int RfTrans_WaitResponse = 200;
        public const int MotorCtr_WaitResponse = 100;
        #endregion

        #region SingleTon
        private static MVController instance;

        public static MVController Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new MVController();
                }
                return instance;
            }
        }
        #endregion

        #region Commands
        public WpfRelayCommand AboutApp { get; private set; }
        public WpfRelayCommand CloseApp { get; private set; }
        public WpfRelayCommand NewConfig { get; private set; }
        public WpfRelayCommandParameter LoadConfig { get; private set; }
        public WpfRelayCommand EditConfig { get; private set; }
        public WpfRelayCommand SaveConfig { get; private set; }
        public WpfRelayCommand StartOper { get; private set; }
        public WpfRelayCommand StopOper { get; private set; }
        public WpfRelayCommand ReadMasterSetting { get; private set; }
        public WpfRelayCommandParameter WriteMasterSetting { get; private set; }        
        public WpfRelayCommand OpenFolder { get; private set; }
        public WpfRelayCommand ShowBasicHelp { get; private set; }
        public WpfRelayCommandParameter ShowTaskHelp { get; private set; }        
        public WpfRelayCommandParameter LoadDataFile { get; private set; }
        public WpfRelayCommand RefreshPort { get; private set; }
        public WpfRelayCommandParameter OpenAxisSetting { get; private set; }
        public WpfRelayCommandParameter OpenCurveSetting { get; private set; }
        public WpfRelayCommandParameter CheckServer { get; private set; }        
        public WpfRelayCommandParameter GetIpAddress { get; private set; }
        public WpfRelayCommandParameter RegisterPort { get; private set; }
        public WpfRelayCommandParameter RegisterPrefix { get; private set; }
        public WpfRelayCommandParameter SelectDirectory { get; private set; }
        public WpfRelayCommandParameter SelectFile { get; private set; }       
        public WpfRelayCommand EnableDeviceSignature { get; private set; }
        public WpfRelayCommand DisableDeviceSignature { get; private set; }
        public WpfRelayCommand StartMatHandler { get; private set; }
        public WpfRelayCommand StopMatHandler { get; private set; }        
        public WpfRelayCommand ShowSampleInfo { get; private set; }
        public WpfRelayCommandParameter CloseFilePage { get; private set; }
        public WpfRelayCommandParameter SaveFilePage { get; private set; }

        public WpfRelayCommandParameter OutPin_ResetPin { get; private set; }
        public WpfRelayCommandParameter OutPin_SetPin { get; private set; }
        public WpfRelayCommandParameter OutPin_ExeCommand { get; private set; }        
        public WpfRelayCommandParameter OutPin_LoadFile { get; private set; }        
        public WpfRelayCommand OutPin_SendAngleServor { get; private set; }
                
        public WpfRelayCommand Press_RequestFileInfo { get; private set; }
        public WpfRelayCommandParameter Press_RequestFile { get; private set; }
        public WpfRelayCommandParameter Press_FilterPress { get; private set; }
        public WpfRelayCommand Press_GetStatus { get; private set; }
        public WpfRelayCommand Press_ReadSetting { get; private set; }
        public WpfRelayCommandParameter Press_WriteSetting { get; private set; }
        public WpfRelayCommand Press_ToggleDebug { get; private set; }

        public WpfRelayCommand uBalance_Help { get; private set; }
        public WpfRelayCommandParameter uBalance_AddAction { get; private set; }
        public WpfRelayCommandParameter uBalance_EditAction { get; private set; }
        public WpfRelayCommandParameter uBalance_RemoveAction { get; private set; }
        public WpfRelayCommandParameter uBalance_UpAction { get; private set; }
        public WpfRelayCommandParameter uBalance_DownAction { get; private set; }
        public WpfRelayCommand uBalance_LoadActions { get; private set; }
        public WpfRelayCommand uBalance_SaveActions { get; private set; }
        public WpfRelayCommand uBalance_GetStatus { get; private set; }
        public WpfRelayCommand uBalance_StartTrans { get; private set; }
        public WpfRelayCommand uBalance_StartOper { get; private set; }
        public WpfRelayCommand uBalance_StopOper { get; private set; }

        public WpfRelayCommand RfTrans_GetStatus { get; private set; }
        public WpfRelayCommand RfTrans_WriteSetting { get; private set; }
        public WpfRelayCommand RfTrans_StartOper { get; private set; }
        public WpfRelayCommand RfTrans_Help { get; private set; }

        public WpfRelayCommandParameter MotorCtr_AddAction { get; private set; }
        public WpfRelayCommandParameter MotorCtr_EditAction { get; private set; }
        public WpfRelayCommandParameter MotorCtr_RemoveAction { get; private set; }
        public WpfRelayCommandParameter MotorCtr_UpAction { get; private set; }
        public WpfRelayCommandParameter MotorCtr_DownAction { get; private set; }
        public WpfRelayCommand MotorCtr_Help { get; private set; }
        public WpfRelayCommand MotorCtr_LoadActions { get; private set; }
        public WpfRelayCommand MotorCtr_SaveActions { get; private set; }
        public WpfRelayCommand MotorCtr_DeviceStatus { get; private set; }
        public WpfRelayCommand MotorCtr_ToggleSample { get; private set; }
        public WpfRelayCommand MotorCtr_StartOper { get; private set; }
        public WpfRelayCommand MotorCtr_StopOper { get; private set; }
        public WpfRelayCommand MotorCtr_TransmitActions { get; private set; }
        public WpfRelayCommandParameter MotorCtr_MoveMotor1Left { get; private set; }
        public WpfRelayCommandParameter MotorCtr_MoveMotor1Right { get; private set; }
        public WpfRelayCommandParameter MotorCtr_MoveMotor2Left { get; private set; }
        public WpfRelayCommandParameter MotorCtr_MoveMotor2Right { get; private set; }       
        public WpfRelayCommandParameter MotorCtr_MoveBothMotor { get; private set; }
        public WpfRelayCommand MotorCtr_SelectDataOption { get; private set; }
        public WpfRelayCommand MotorCtr_WritePwmPeriod { get; private set; }
        public WpfRelayCommand MotorCtr_StartAutoBalance { get; private set; }
        public WpfRelayCommand MotorCtr_StopAutoBalance { get; private set; }
        #endregion

        #region Field  
        //File, IO handler
        private uint fileChangedIndex;
        private uint fileChangedOffset;
        private string configFilePath;
        private MatFileHandler matFileHandler;
        private PressFileHandler pressFileHandler;
        private System.IO.FileSystemWatcher fileWatcher;

        //Communication
        private bool hasResponse;
        private SerialPort serialPort;
        private UsbPort usbPort;
        private HttpServer httpServer;
        private HttpServer webServer;

        //Worker, Timer
        private double samplePeriod;
        private double currentTime;
        private long startTick;
        private PeriodWorker requestWorker;
        private DispatcherTimer updateTimer;

        //Parse
        private HeaderParser headerParser;
        private DataPackage lastResponse;
        private Queue<DataPackage> responseQueue;

        //Queue
        private ConcurrentQueue<DataPackage> packageQueue;
        private FixConcurrentQueue<DataPackage> serverQueue;

        //Logic
        private MotorUnbalanceController motorCtr_UnbalanceController;

        //Servo
        private ushort _lastServoValue;
        #endregion

        #region Constructor
        public MVController()
            : base(null)
        {
            InitializeParameter();
            InitializeMVVM();
            Initialize_PcActionHelper();
            Initialize_ViewProperties();
        }

        public override void InitializeParameter()
        {
            this.hasResponse = false;
            this.lastResponse = null;
            this.responseQueue = new Queue<DataPackage>(10);

            this.configFilePath = string.Empty;
            this.headerParser = new HeaderParser();
            this.packageQueue = new ConcurrentQueue<DataPackage>();
            this.serverQueue = new FixConcurrentQueue<DataPackage>(100);
            
            this.InputParameter = new InputParameters();
            this.OutputParameter = new OutputParameters(UpdateAppStatusColor);
        }

        public override void InitializeMVVM()
        {
            this.AboutApp = new WpfRelayCommand(Exe_AboutApp, Can_AboutApp);
            this.CloseApp = new WpfRelayCommand(Exe_CloseApp, Can_CloseApp);
            this.NewConfig = new WpfRelayCommand(Exe_NewConfig, Can_NewConfig);
            this.LoadConfig = new WpfRelayCommandParameter(Exe_LoadConfig, Can_LoadConfig);
            this.EditConfig = new WpfRelayCommand(Exe_EditConfig, Can_EditConfig);
            this.SaveConfig = new WpfRelayCommand(Exe_SaveConfig, Can_SaveConfig);
            this.StartOper = new WpfRelayCommand(Exe_StartOper, Can_StartOper);
            this.StopOper = new WpfRelayCommand(Exe_StopOper, Can_StopOper);
            this.ReadMasterSetting = new WpfRelayCommand(Exe_ReadMasterSetting, Can_ReadMasterSetting);
            this.WriteMasterSetting = new WpfRelayCommandParameter(Exe_WriteMasterSetting, Can_WriteMasterSetting);
            this.OpenFolder = new WpfRelayCommand(Exe_OpenFolder, Can_OpenFolder);
            this.ShowBasicHelp = new WpfRelayCommand(Exe_ShowBasicHelp, Can_ShowBasicHelp);
            this.ShowTaskHelp = new WpfRelayCommandParameter(Exe_ShowTaskHelp, Can_ShowTaskHelp);
            this.LoadDataFile = new WpfRelayCommandParameter(Exe_LoadDataFile, Can_LoadDataFile);
            this.RefreshPort = new WpfRelayCommand(Exe_RefreshPort, Can_RefreshPort);
            this.OpenAxisSetting = new WpfRelayCommandParameter(Exe_OpenAxisSetting, Can_OpenAxisSetting);
            this.OpenCurveSetting = new WpfRelayCommandParameter(Exe_OpenCurveSetting, Can_OpenCurveSetting);
            this.CheckServer = new WpfRelayCommandParameter(Exe_CheckServer, Can_CheckServer);
            this.GetIpAddress = new WpfRelayCommandParameter(Exe_GetIpAddress, Can_GetIpAddress);
            this.RegisterPort = new WpfRelayCommandParameter(Exe_RegisterPort, Can_RegisterPort);
            this.RegisterPrefix = new WpfRelayCommandParameter(Exe_RegisterPrefix, Can_RegisterPrefix);
            this.SelectDirectory = new WpfRelayCommandParameter(Exe_SelectDirectory, Can_SelectDirectory);
            this.SelectFile = new WpfRelayCommandParameter(Exe_SelectFile, Can_SelectFile);
            this.EnableDeviceSignature = new WpfRelayCommand(Exe_EnableDeviceSignature, Can_EnableDeviceSignature);
            this.DisableDeviceSignature = new WpfRelayCommand(Exe_DisableDeviceSignature, Can_DisableDeviceSignature);
            this.StartMatHandler = new WpfRelayCommand(Exe_StartMatHandler, Can_StartMatHandler);
            this.StopMatHandler = new WpfRelayCommand(Exe_StopMatHandler, Can_StopMatHandler);
            this.ShowSampleInfo = new WpfRelayCommand(Exe_ShowSampleInfo, Can_ShowSampleInfo);
            this.CloseFilePage = new WpfRelayCommandParameter(Exe_CloseFilePage, Can_CloseFilePage);
            this.SaveFilePage = new WpfRelayCommandParameter(Exe_SaveFilePage, Can_SaveFilePage);

            this.OutPin_ResetPin = new WpfRelayCommandParameter(Exe_OutPin_ResetPin, Can_OutPin_ResetPin);
            this.OutPin_SetPin = new WpfRelayCommandParameter(Exe_OutPin_SetPin, Can_OutPin_SetPin);
            this.OutPin_ExeCommand = new WpfRelayCommandParameter(Exe_OutPin_ExeCommand, Can_OutPin_ExeCommand);
            this.OutPin_LoadFile = new WpfRelayCommandParameter(Exe_OutPin_LoadFile, Can_OutPin_LoadFile);
            this.OutPin_SendAngleServor = new WpfRelayCommand(Exe_OutPin_SendAngleServor, Can_OutPin_SendAngleServor);

            this.Press_RequestFileInfo = new WpfRelayCommand(Exe_Press_RequestFileInfo, Can_Press_RequestFileInfo);
            this.Press_RequestFile = new WpfRelayCommandParameter(Exe_Press_RequestFile, Can_Press_RequestFile);
            this.Press_FilterPress = new WpfRelayCommandParameter(Exe_Press_FilterPress, Can_Press_FilterPress);
            this.Press_GetStatus = new WpfRelayCommand(Exe_Press_GetStatus, Can_Press_GetStatus);
            this.Press_ReadSetting = new WpfRelayCommand(Exe_Press_ReadSetting, Can_Press_ReadSetting);
            this.Press_WriteSetting = new WpfRelayCommandParameter(Exe_Press_WriteSetting, Can_Press_WriteSetting);
            this.Press_ToggleDebug = new WpfRelayCommand(Exe_Press_ToggleDebug, Can_Press_ToggleDebug);

            this.uBalance_Help = new WpfRelayCommand(Exe_uBalance_Help, Can_uBalance_Help);
            this.uBalance_AddAction = new WpfRelayCommandParameter(Exe_uBalance_AddAction, Can_uBalance_AddAction);
            this.uBalance_RemoveAction = new WpfRelayCommandParameter(Exe_uBalance_RemoveAction, Can_uBalance_RemoveAction);
            this.uBalance_EditAction = new WpfRelayCommandParameter(Exe_uBalance_EditAction, Can_uBalance_EditAction);
            this.uBalance_UpAction = new WpfRelayCommandParameter(Exe_uBalance_UpAction, Can_uBalance_UpAction);
            this.uBalance_DownAction = new WpfRelayCommandParameter(Exe_uBalance_DownAction, Can_uBalance_DownAction);
            this.uBalance_LoadActions = new WpfRelayCommand(Exe_uBalance_LoadActions, Can_uBalance_LoadActions);
            this.uBalance_SaveActions = new WpfRelayCommand(Exe_uBalance_SaveActions, Can_uBalance_SaveActions);
            this.uBalance_GetStatus = new WpfRelayCommand(Exe_uBalance_GetStatus, Can_uBalance_GetStatus);
            this.uBalance_StartTrans = new WpfRelayCommand(Exe_uBalance_StartTrans, Can_uBalance_StartTrans);
            this.uBalance_StartOper = new WpfRelayCommand(Exe_uBalance_StartOper, Can_uBalance_StartOper);
            this.uBalance_StopOper = new WpfRelayCommand(Exe_uBalance_StopOper, Can_uBalance_StopOper);

            this.RfTrans_GetStatus = new WpfRelayCommand(Exe_RfTrans_GetStatus, Can_RfTrans_GetStatus);
            this.RfTrans_WriteSetting = new WpfRelayCommand(Exe_RfTrans_WriteSetting, Can_RfTrans_WriteSetting);
            this.RfTrans_StartOper = new WpfRelayCommand(Exe_RfTrans_StartOper, Can_RfTrans_StartOper);
            this.RfTrans_Help = new WpfRelayCommand(Exe_RfTrans_Help, Can_RfTrans_Help);

            this.MotorCtr_AddAction = new WpfRelayCommandParameter(Exe_MotorCtr_AddAction, Can_MotorCtr_AddAction);
            this.MotorCtr_EditAction = new WpfRelayCommandParameter(Exe_MotorCtr_EditAction, Can_MotorCtr_EditAction);
            this.MotorCtr_RemoveAction = new WpfRelayCommandParameter(Exe_MotorCtr_RemoveAction, Can_MotorCtr_RemoveAction);
            this.MotorCtr_UpAction = new WpfRelayCommandParameter(Exe_MotorCtr_UpAction, Can_MotorCtr_UpAction);
            this.MotorCtr_DownAction = new WpfRelayCommandParameter(Exe_MotorCtr_DownAction, Can_MotorCtr_DownAction);
            this.MotorCtr_Help = new WpfRelayCommand(Exe_MotorCtr_Help, Can_MotorCtr_Help);
            this.MotorCtr_LoadActions = new WpfRelayCommand(Exe_MotorCtr_LoadActions, Can_MotorCtr_LoadActions);
            this.MotorCtr_SaveActions = new WpfRelayCommand(Exe_MotorCtr_SaveActions, Can_MotorCtr_SaveActions);
            this.MotorCtr_DeviceStatus = new WpfRelayCommand(Exe_MotorCtr_DeviceStatus, Can_MotorCtr_DeviceStatus);
            this.MotorCtr_ToggleSample = new WpfRelayCommand(Exe_MotorCtr_ToggleSample, Can_MotorCtr_ToggleSample);
            this.MotorCtr_SelectDataOption = new WpfRelayCommand(Exe_MotorCtr_SelectDataOption, Can_MotorCtr_SelectDataOption);
            this.MotorCtr_StartOper = new WpfRelayCommand(Exe_MotorCtr_StartOper, Can_MotorCtr_StartOper);
            this.MotorCtr_StopOper = new WpfRelayCommand(Exe_MotorCtr_StopOper, Can_MotorCtr_StopOper);
            this.MotorCtr_TransmitActions = new WpfRelayCommand(Exe_MotorCtr_TransmitActions, Can_MotorCtr_TransmitActions);
            this.MotorCtr_MoveMotor1Left = new WpfRelayCommandParameter(Exe_MotorCtr_MoveMotor1Left, Can_MotorCtr_MoveMotor1Left);
            this.MotorCtr_MoveMotor1Right = new WpfRelayCommandParameter(Exe_MotorCtr_MoveMotor1Right, Can_MotorCtr_MoveMotor1Right);
            this.MotorCtr_MoveMotor2Left = new WpfRelayCommandParameter(Exe_MotorCtr_MoveMotor2Left, Can_MotorCtr_MoveMotor2Left);
            this.MotorCtr_MoveMotor2Right = new WpfRelayCommandParameter(Exe_MotorCtr_MoveMotor2Right, Can_MotorCtr_MoveMotor2Right);
            this.MotorCtr_MoveBothMotor = new WpfRelayCommandParameter(Exe_MotorCtr_MoveBothMotor, Can_MotorCtr_MoveBothMotor);
            this.MotorCtr_WritePwmPeriod = new WpfRelayCommand(Exe_MotorCtr_WritePwmPeriod, Can_MotorCtr_WritePwmPeriod);
            this.MotorCtr_StartAutoBalance = new WpfRelayCommand(Exe_MotorCtr_StartAutoBalance, Can_MotorCtr_StartAutoBalance);
            this.MotorCtr_StopAutoBalance = new WpfRelayCommand(Exe_MotorCtr_StopAutoBalance, Can_MotorCtr_StopAutoBalance);            

            this.CommandInfoContainer = new CommandInfoContainer(this);
            this.ValidationRuleContainer = new ValidationRuleContainer(this);
        }

        private void Initialize_ViewProperties()
        {
            SetViewProperties(this.InputParameter);
            SetViewProperties(this.OutputParameter);
        }
        #endregion

        #region Public Method
        public void SetView(MainWindow view)
        {
            this.View = view;
        }
        #endregion

        #region Dispose
        public async override void Dispose()
        {
            //Dispose in UI Thread
            if (this.StopMatHandler.CanExecute(null))
            {
                this.StopMatHandler.Execute(null);
            }

            if (this.StopOper.CanExecute(null))
            {
                this.StopOper.Execute(null);
                await Task.Delay(500);
            }
            
            SingleWorker worker = new SingleWorker();
            worker.Stoped += (_sender, _e) =>
            {
                this.IsDisposed = true;
                this.View.Close();
            };
            worker.SetAction(() =>
            {
                //make sure UI Thread call return
                Task.Delay(100).Wait();

                //Dispose in Background Thread
            });
            worker.Start();
        }
        #endregion

        #region Private Method
        private void NotifyNoFunction()
        {
            System.Windows.MessageBox.Show("Function is not implemented !!!");
        }

        private void UpdateAppStatusColor(AppStatusTypes type)
        {
            this.View.UpdateAppStatusColor(type);
        }

        private void SetViewProperties(object data)
        {
            if (data == null)
            {
                return;
            }

            #region Configuration
            if (data is Configuration)
            {
                var config = data as Configuration;

                foreach (var item in this.InputParameter.TaskOptions)
                {
                    switch (item.Type)
                    {
                        case MicroTasks.Press:          item.Content = config.PressSetting;             break;
                        case MicroTasks.Capacitor:      item.Content = config.CapSetting;               break;
                        case MicroTasks.Acceleration:   item.Content = config.AccSetting;               break;
                        case MicroTasks.uBalance:       item.Content = config.uBalanceSetting;          break;
                        case MicroTasks.RfTrans:        item.Content = config.RfTransSetting;           break;
                        case MicroTasks.MotorCtr:       item.Content = config.MotorCtrSetting;          break;
                        case MicroTasks.Non:
                        default:                        item.Content = null;                            break;
                    }
                    SetViewProperties(item.Content as SettingBase);
                }
            }
            #endregion            

            #region Other setting
            else if(data is InputParameters)
            {
                var parameters = data as InputParameters;
                
                foreach (var item in parameters.OutPinCollection)
                {
                    item.CommandCollection = parameters.OutPinCommandCollection;
                    item.DurationRule = this.ValidationRuleContainer.NonZeroUshortRule;
                    item.ResetPin = this.CommandInfoContainer.OutPin_ResetPin;
                    item.SetPin = this.CommandInfoContainer.OutPin_SetPin;
                    item.ExeCommand = this.CommandInfoContainer.OutPin_ExeCommand;
                    item.LoadFile = this.CommandInfoContainer.OutPin_LoadFile;
                }

                //MotorCtr
                var moveMotor = parameters.MotorCtr_Motor1Move as MotorMoveSetting;
                if (moveMotor != null)
                {
                    moveMotor.PulseRule = this.ValidationRuleContainer.NonZeroUshortRule;
                }
                moveMotor = parameters.MotorCtr_Motor2Move as MotorMoveSetting;
                if (moveMotor != null)
                {
                    moveMotor.PulseRule = this.ValidationRuleContainer.NonZeroUshortRule;
                }
            }
            else if(data is OutputParameters)
            {

            }
            else if (data is SerialPortSetting)
            {
                var setting = data as SerialPortSetting;
                setting.Refresh = this.RefreshPort;
                setting.PortCollection = this.InputParameter.PortCollection;
                setting.BaudRateCollection = this.InputParameter.BaudRateCollection;
                setting.DataBitCollection = this.InputParameter.DataBitCollection;
                setting.ParityCollection = this.InputParameter.ParityCollection;
                setting.StopBitsCollection = this.InputParameter.StopBitsCollection;
                setting.ThresholdAmountRule = this.ValidationRuleContainer.ThresholdAmountRule;             
            }
            else if(data is UsbSetting)
            {
                var setting = data as UsbSetting;
                setting.IdRule = this.ValidationRuleContainer.UsbIdRule;
                setting.EnableDeviceSignature = this.EnableDeviceSignature;
                setting.DisableDeviceSignature = this.DisableDeviceSignature;
            }
            else if (data is RemoteSetting)
            {
                var setting = data as RemoteSetting;
                setting.CheckServer = this.CheckServer;
                setting.TimeRule = this.ValidationRuleContainer.PosIntRule;
                setting.PortRule = this.ValidationRuleContainer.UshortRule;
                setting.NameRule = this.ValidationRuleContainer.NameRule;
                setting.AddressRule = this.ValidationRuleContainer.AddressRule;
            }
            else if (data is ServerSetting)
            {
                var setting = data as ServerSetting;
                setting.GetIpAddress = this.GetIpAddress;
                setting.RegisterPort = this.RegisterPort;
                setting.RegisterPrefix = this.RegisterPrefix;
                setting.PortRule = this.ValidationRuleContainer.UshortRule;
            }
            else if (data is GraphicSetting)
            {
                var setting = data as GraphicSetting;
                setting.PointAmountRule = this.ValidationRuleContainer.PointAmountRule;
                foreach (var item in setting.CurveCollection)
                {
                    item.LineStyleCollection = this.InputParameter.LineStyleCollection;
                    item.OpenSetting = this.OpenCurveSetting;
                }

                foreach (var item in setting.AxisCollection)
                {
                    item.LineStyleCollection = this.InputParameter.LineStyleCollection;
                    item.OpenSetting = this.OpenAxisSetting;
                }
            }            
            else if(data is PressFileSetting)
            {
                var setting = data as PressFileSetting;
                setting.SelectDirectory = this.SelectDirectory;
            }
            else if (data is FileSingleSetting)
            {
                var setting = data as FileSingleSetting;
                setting.SelectDirectory = this.SelectDirectory;
                setting.FileNameRule = this.ValidationRuleContainer.FileNameRule;
                setting.SampleAmountRule = this.ValidationRuleContainer.SampleAmountRule;
                setting.IndexRule = this.ValidationRuleContainer.PosIntRule;
                setting.DurationRule = this.ValidationRuleContainer.DurationRule;
            }
            else if(data is PressFilterInfo)
            {
                var info = data as PressFilterInfo;
                info.IdRule = this.ValidationRuleContainer.NonZeroByteRule;
                info.DurationRule = this.ValidationRuleContainer.Press_Duration;
            }
            else if(data is uBalanceChannelAction)
            {
                var action = data as uBalanceChannelAction;
                action.ActionCollection = this.InputParameter.uBalance_ActionCollection;
                action.WidthRule = this.ValidationRuleContainer.ByteRule;
                action.IndexRule = this.ValidationRuleContainer.uBalance_IndexRule;
                action.AmountRule = this.ValidationRuleContainer.NonZeroUshortRule;
            }
            else if(data is RfTransOperSetting)
            {
                var setting = data as RfTransOperSetting;
                setting.DurationRule = this.ValidationRuleContainer.RfTrans_Duration;
            }
            else if (data is MasterSettingPackage)
            {
                var package = data as MasterSettingPackage;
                package.ByteRule = this.ValidationRuleContainer.ByteRule;
                package.RfAddressRule = this.ValidationRuleContainer.RfAddressRule;
            }
            else if(data is PressSlaveSettingPackage)
            {
                var package = data as PressSlaveSettingPackage;
                package.ByteRule = this.ValidationRuleContainer.ByteRule;
                package.UshortRule = this.ValidationRuleContainer.UshortRule;
                package.RfAddressRule = this.ValidationRuleContainer.RfAddressRule;
            }
            else if(data is MotorCtrMotorAction)
            {
                var action = data as MotorCtrMotorAction;
                action.ActionCollection = this.InputParameter.MotorCtr_ActionCollection;
                action.BackwardCollection = this.InputParameter.MotorCtr_BackwardCollection;
                action.ShortRule = this.ValidationRuleContainer.ShortRule;
                action.UshortRule = this.ValidationRuleContainer.UshortRule;
                action.NonZeroUshortRule = this.ValidationRuleContainer.NonZeroUshortRule;
            }
            else if(data is MotorCtrDataOption)
            {
                var option = data as MotorCtrDataOption;
                option.FrequencyRule = this.ValidationRuleContainer.NonZeroByteRule;
            }
            else if(data is FileChangedSetting)
            {
                var setting = data as FileChangedSetting;
                setting.ResponseCollection = this.InputParameter.FileChangedResponseCollection;
                setting.LengthRule = this.ValidationRuleContainer.PosIntRule;
                setting.SelectFile = this.SelectFile;
            }
            else if(data is MotorUnbalanceSetting)
            {
                var setting = data as MotorUnbalanceSetting;
                setting.PosDoubleRule = this.ValidationRuleContainer.PosDoubleRule;
                setting.NonZeroUshortRule = this.ValidationRuleContainer.NonZeroUshortRule;
            }
            #endregion
        }

        private void SetViewProperties(SettingBase data)
        {
            if (data == null)
            {
                return;
            }

            #region Common
            SetViewProperties(data.SerialPortSetting);
            SetViewProperties(data.UsbSetting);
            SetViewProperties(data.ServerSetting);
            SetViewProperties(data.RemoteSetting);
            #endregion

            #region Task specific
            if (data is PressSetting)
            {
                var setting = data as PressSetting;               
                SetViewProperties(setting.GraphicSetting);
                SetViewProperties(setting.PressFileSetting);
                SetViewProperties(setting.MatFileSetting);                
            }
            else if(data is CapSetting)
            {
                var setting = data as CapSetting;
                SetViewProperties(setting.GraphicSetting);
                SetViewProperties(setting.MatFileSetting);
                setting.ChargeResistorRule = this.ValidationRuleContainer.Cap_ResistorRule;
            }
            else if (data is AccSetting)
            {
                var setting = data as AccSetting;
                SetViewProperties(setting.GraphicSetting);
                SetViewProperties(setting.MatFileSetting);                
            }
            else if(data is uBalanceSetting)
            {
                var setting = data as uBalanceSetting;
            }
            else if(data is RfTransSetting)
            {
                var setting = data as RfTransSetting;
                SetViewProperties(setting.OperSetting);
                SetViewProperties(setting.GraphicSetting);
                SetViewProperties(setting.MatFileSetting);
            }
            else if(data is MotorCtrSetting)
            {
                var setting = data as MotorCtrSetting;
                SetViewProperties(setting.GraphicSetting);
                SetViewProperties(setting.MatFileSetting);
                SetViewProperties(setting.FileChangedSetting);
                SetViewProperties(setting.UnbalanceSetting);
            }
            #endregion
        }
        #endregion
    }
}
