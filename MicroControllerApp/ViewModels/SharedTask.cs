﻿using LongInterface.Models;
using LongModel.ViewModels;
using LongWpfUI.Models.Commands;
using LongWpfUI.SystemHelper;
using MicroControllerApp.Interfaces;
using MicroControllerApp.Models;
using MicroControllerApp.Models.Base;
using MicroControllerApp.Models.DataClasses;
using MicroControllerApp.Models.DataClasses.Packages;
using MicroControllerApp.Models.DataClasses.Settings;
using MicroControllerApp.Models.IO;
using MicroControllerApp.Models.IO.Handlers;
using MicroControllerApp.Models.ViewClasses.Graphics;
using MicroControllerApp.ViewModels.Helpers;
using MicroControllerApp.Views.Dialogs;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace MicroControllerApp.ViewModels
{
    public partial class MVController : ViewModel<MainWindow, InputParameters, OutputParameters, CommandInfoContainer, ValidationRuleContainer>
    {
        #region IO

        private void ProcessIn()
        {
            try
            {
                var collection = new List<byte[]>();
                byte tmpValue = 0;

                #region Parse Buffer
                var parser = this.headerParser;
                while (parser.Queue.Count > 0 || parser.HasLength)
                {
                    if (parser.HasLength)
                    {
                        //wait Length
                        if (parser.Queue.Count >= parser.Length)
                        {
                            //set
                            var data = new byte[parser.Length + 2];
                            data[0] = parser.Type;
                            data[1] = parser.Length;
                            for (int i = 0; i < parser.Length; i++)
                            {
                                data[i + 2] = parser.Queue.Dequeue();
                            }
                            collection.Add(data);

                            //reset
                            parser.HasLength = false;
                        }
                        else
                        {
                            //wait for more buffer
                            break;
                        }
                    }
                    else if (parser.HasHeader)
                    {   //Wait Type, Length
                        if (parser.Queue.Count > 1)
                        {
                            //set
                            parser.Type = parser.Queue.Dequeue();
                            parser.Length = parser.Queue.Dequeue();
                            parser.HasLength = true;
                            //reset
                            parser.HasHeader = false;
                        }
                        else
                        {
                            //wait for more buffer
                            break;
                        }
                    }
                    //Wait Header
                    else
                    {
                        tmpValue = parser.Queue.Dequeue();
                        if (tmpValue == HeaderParser.Indicator)
                        {
                            parser.HeaderCount++;
                            if (parser.HeaderCount >= HeaderParser.IndicatorLength)
                            {
                                //set
                                parser.HasHeader = true;
                                //reset
                                parser.HeaderCount = 0;
                            }
                        }
                        else
                        {
                            //reset
                            parser.HeaderCount = 0;
                        }
                    }
                }
                #endregion

                #region Parse Package
                if (collection.Count > 0)
                {
                    var setting = this.InputParameter.CurrentConfig.CurrentSetting;
                    foreach (var item in collection)
                    {
                        //try to get package
                        var package = setting.ParseSharedBuffer(item);
                        if (package == null)
                        {
                            package = setting.ParseBuffer(item);
                        }

                        //add package when it is valid
                        if (package != null)
                        {
                            this.packageQueue.Enqueue(package);
                        }
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                App.Current.Dispatcher.Invoke(() =>
                {
                    //Stop operation
                    this.StopOper.TryToExecute();
                    //Notify User
                    MessageBox.Show(ex.Message, "Parse Input stream from device error", MessageBoxButton.OK, MessageBoxImage.Information);
                });
            }            
        }

        private void WriteOut(IBuffer buffer)
        {
            byte[] data = buffer.GetBytes();
            WriteOut(data, 0, data.Length);
        }

        private void WriteOut(byte[] data)
        {
            WriteOut(data, 0, data.Length);
        }

        private void WriteOut(byte[] data, int offset, int length)
        {
            var setting = this.InputParameter.CurrentConfig.CurrentSetting;
            if (setting != null)
            {
                switch (setting.Port)
                {
                    case PhysicalPorts.USB:
                        this.usbPort.Write(data, 0, data.Length);
                        break;

                    case PhysicalPorts.WebServer:
                        
                        break;
                        
                    case PhysicalPorts.Serial:
                    default:
                        this.serialPort.Write(data, 0, data.Length);
                        break;
                }
            }

        }

        private void UsbPort_DataReceived(object sender, UsbDataReceivedEventArgs e)
        {
            var port = sender as UsbPort;
            if (port != null)
            {
                var data = new byte[port.BytesToRead];
                port.Read(data, 0, data.Length);

                foreach (var item in data)
                {
                    this.headerParser.Queue.Enqueue(item);
                }

                ProcessIn();
            }
        }

        private void SerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            var port = sender as SerialPort;
            if (port != null)
            {
                var data = new byte[port.BytesToRead];
                port.Read(data, 0, data.Length);
                foreach (var item in data)
                {
                    this.headerParser.Queue.Enqueue(item);
                }

                ProcessIn();
            }
        }
        #endregion

        #region Setting Initialize/Dispose
        private void Setting_InitializeSW(SettingBase setting)
        {
            if (setting is IGraphic)
            {
                Setting_InitializeGraphic(setting as IGraphic);
            }
            if (setting is IMatFile)
            {
                Setting_InitializeMatFile(setting as IMatFile);
            }

            this.updateTimer = new DispatcherTimer();
            this.updateTimer.Interval = TimeSpan.FromMilliseconds(UpdateInterval);
            this.updateTimer.Tick += UpdateTimer_Tick;
            this.updateTimer.Start();
        }
        
        private void Setting_InitializeHW(SettingBase setting)
        {
            if (setting != null && setting.Position == AppPositions.Local)
            {
                switch (setting.Port)
                {
                    case PhysicalPorts.USB:
                        this.usbPort = new UsbPort(setting.UsbSetting.VendorID, setting.UsbSetting.ProductID);
                        this.usbPort.Open();
                        this.usbPort.DataReceived += UsbPort_DataReceived;
                        break;

                    case PhysicalPorts.WebServer:
                        string prefix = GetServerPrefix(setting.ServerSetting);
                        bool isOk = NetworkHelper.CheckHttpPrefix(prefix);
                        if (!isOk)  //NOT
                        {
                            throw new Exception("Prefix address of Server hasn't been registered");
                        }
                        this.webServer = new LongWpfUI.Models.Net.HttpServer();
                        this.webServer.Start(prefix, setting.OnRequested);
                        break;

                    case PhysicalPorts.Serial:
                    default:
                        this.serialPort = new SerialPort()
                        {
                            PortName = setting.SerialPortSetting.Port,
                            BaudRate = setting.SerialPortSetting.BaudRate,
                            Parity = (Parity)setting.SerialPortSetting.ParityIndex,
                            StopBits = (StopBits)setting.SerialPortSetting.StopBitsIndex,
                            DtrEnable = setting.SerialPortSetting.EnableDTR,
                            RtsEnable = setting.SerialPortSetting.EnableRTS
                        };

                        if (setting.SerialPortSetting.UseThreshold)
                        {
                            this.serialPort.ReceivedBytesThreshold = setting.SerialPortSetting.ThresholdAmount;
                        }
                        this.serialPort.Open();
                        this.serialPort.DataReceived += SerialPort_DataReceived;                        
                        break;
                }
            }
        }        

        private void Setting_Update(SettingBase setting, ISharedPackage sharedPackage)
        {
            if (sharedPackage is MasterSettingPackage)
            {
                this.hasResponse = true;
                this.lastResponse = sharedPackage as DataPackage;
            }
            else if(sharedPackage is CommandDonePackage)
            {
                this.hasResponse = true;
                this.lastResponse = sharedPackage as DataPackage;
            }
            else if(sharedPackage is SubSlaveSettingPackage)
            {
                this.hasResponse = true;
                this.lastResponse = sharedPackage as DataPackage;
            }
        }

        private void Setting_DisposeSW(SettingBase setting)
        {
            if (setting is IGraphic)
            {
                Setting_DisposeGraphic();
            }
            if (setting is IMatFile)
            {
                Setting_DiposeMatFile();
            }

            if (this.updateTimer != null && this.updateTimer.IsEnabled)
            {
                this.updateTimer.Stop();
                this.updateTimer.Tick -= UpdateTimer_Tick;
                this.updateTimer = null;
            }
        }

        private void Setting_DisposeHW(SettingBase setting)
        {
            if (setting != null && setting.Position == AppPositions.Local)
            {
                switch (setting.Port)
                {
                    case PhysicalPorts.USB:
                        if (this.usbPort != null)
                        {
                            this.usbPort.Close();
                            this.usbPort.DataReceived -= UsbPort_DataReceived;
                            this.usbPort = null;
                        }
                        break;

                    case PhysicalPorts.WebServer:
                        if (this.webServer != null && this.webServer.IsEnabled)
                        {
                            this.webServer.Dispose();
                            this.webServer = null;
                        }
                        break;

                    case PhysicalPorts.Serial:
                    default:
                        if (this.serialPort != null)
                        {
                            this.serialPort.Close();
                            this.serialPort.DataReceived -= SerialPort_DataReceived;
                            this.serialPort = null;
                        }
                        break;
                }
            }
        }

        #region Sub Method
        private void Setting_InitializeGraphic(IGraphic setting)
        {
            //reset
            foreach (var item in this.OutputParameter.PlotModel.Series)
            {
                var dataCurve = item as DataCurve;
                if (dataCurve != null)
                {
                    dataCurve.Points.Clear();
                }
            }
            //Update
            if (this.OutputParameter.HasView(AppViews.Graphic))
            {
                this.OutputParameter.UpdateGraphic();
            }
        }

        private void Setting_InitializeMatFile(IMatFile setting)
        {
            if (setting != null)
            {
                //Set parameter
                this.currentTime = 0;                            
                this.samplePeriod = (setting.SampleInfo.Frequecy > 0) ? 1 / setting.SampleInfo.Frequecy : 1;

                //create Instance
                this.matFileHandler = new MatFileHandler(setting.MatFileSetting, OnMatWriterStoped, OnMatWriterHasError);                
            }            
        }

        private void Setting_DisposeGraphic()
        {
            this.OutputParameter.ClearGraphic();
        }

        private void Setting_DiposeMatFile()
        {
            if (this.StopMatHandler.CanExecute(null))
            {
                this.StopMatHandler.Execute(null);
            }
            //Important => only in this Method
            this.matFileHandler = null;
        }
        #endregion
        #endregion

        #region Mat Writer       
        private bool Can_ShowSampleInfo()
        {
            return (this.InputParameter.CurrentConfig != null && this.InputParameter.CurrentConfig.CurrentSetting is IMatFile);
        }

        private void Exe_ShowSampleInfo()
        {
            try
            {
                var setting = this.InputParameter.CurrentConfig.CurrentSetting as IMatFile;
                var window = new SampleInfoWindow(this.View, setting.SampleInfo);
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message, "Show Sample Info error", 
                                               System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Information);
            }
        }
        
        private bool Can_StartMatHandler()
        {
            return this.matFileHandler != null && !this.matFileHandler.IsEnabled;    //NOT
        }

        private void Exe_StartMatHandler()
        {
            this.matFileHandler.Start();
            this.matFileHandler.ClearSampleAmount();
            this.View.ShowSamplePanel(true);
            this.OutputParameter.UpdateAppStatus("Start collecting sample to write mat file");
            this.startTick = DateTime.Now.Ticks;
            WpfRelayCommand.UpdateCanExecuteStatus();
        }

        private bool Can_StopMatHandler()
        {
            return this.matFileHandler != null && this.matFileHandler.IsEnabled;
        }

        private void Exe_StopMatHandler()
        {
            this.matFileHandler.Stop();            
            this.View.ShowSamplePanel(false);
            this.OutputParameter.UpdateAppStatus("Stop collecting sample to write mat file");
            WpfRelayCommand.UpdateCanExecuteStatus();
        }

        private void OnMatWriterHasError(Exception ex)
        {
            this.OutputParameter.UpdateAppStatus("Write MatFile Error: " + ex.Message);
        }

        private void OnMatWriterStoped(string filePath)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                this.OutputParameter.UpdateAppStatus("MatFile was written: " + filePath);
            });           
        }
        #endregion
    }
}
