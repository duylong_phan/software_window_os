﻿using LongModel.ViewModels;
using MicroControllerApp.Models;
using MicroControllerApp.ViewModels.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.ViewModels
{
    public partial class MVController : ViewModel<MainWindow, InputParameters, OutputParameters, CommandInfoContainer, ValidationRuleContainer>
    {
        //setting.SetLocalAction(XXX_InitializeSW, XXX_InitializeHW, XXX_Update, XXX_DisposeSW, XXX_DisposeHW);
        //setting.SetRemoteAction(XXX_OnRequested, XXX_OnResponsed);

        #region Local
        private void XXX_InitializeSW()
        {
            
        }

        private void XXX_InitializeHW()
        {
            
        }


        private void XXX_Update()
        {
            
        }

        private void XXX_DisposeSW()
        {
            
        }

        private void XXX_DisposeHW()
        {
            
        }
        #endregion


        #region Remote
        private void XXX_OnRequested(HttpListenerRequest request, HttpListenerResponse respone)
        {
            
        }

        private void XXX_OnResponsed(string suffix, byte[] buffer)
        {
            
        }
        #endregion
    }
}
