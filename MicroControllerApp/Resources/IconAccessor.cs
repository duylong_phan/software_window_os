﻿using LongInterface.Views.ControlModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroControllerApp.Resources
{
    public class IconAccessor : IBasicIcon
    {
        #region Instance
        public bool HasIcon
        {
            get
            {
                return !string.IsNullOrEmpty(this.Icon);    //NOT
            }
        }

        public string Icon { get; private set; }

        private IconAccessor(string icon)
        {
            this.Icon = icon;
        }
        #endregion

        private static IconAccessor bime;
        private static IconAccessor appIcon;
        private static IconAccessor help_Basic1;
        private static IconAccessor help_Basic2;
        private static IconAccessor help_Basic3;
        private static IconAccessor help_Basic4;
        private static IconAccessor help_Basic5;
        private static IconAccessor help_CapPara1;
        private static IconAccessor help_Communication1;
        private static IconAccessor help_Communication2;
        private static IconAccessor help_Communication3;
        private static IconAccessor help_Communication4;
        private static IconAccessor help_Graphic1;
        private static IconAccessor help_Graphic2;
        private static IconAccessor help_Graphic3;
        private static IconAccessor help_MatFile1;
        private static IconAccessor help_Position1;
        private static IconAccessor help_PressFile1;
        private static IconAccessor help_DeviceManager;
        private static IconAccessor help_PSOC4_Driver_1;
        private static IconAccessor help_PSOC4_Driver_2;
        private static IconAccessor help_PSOC4_Driver_3;
        private static IconAccessor help_PSOC5_Driver_1;
        private static IconAccessor help_PSOC5_Driver_2;
        private static IconAccessor help_PSOC5_Driver_3;
        private static IconAccessor help_Press1;
        private static IconAccessor help_Press2;
        private static IconAccessor help_Press3;
        private static IconAccessor help_Press4;
        private static IconAccessor help_Press5;
        private static IconAccessor help_Press6;
        private static IconAccessor help_Press7;
        private static IconAccessor help_Press8;
        private static IconAccessor help_Press9;


        /// <summary>
        /// Get source for Bime Icon
        /// </summary>
        public static IconAccessor Bime
        {
            get
            {
                if (bime == null)
                {
                    bime = new IconAccessor("/MicroControllerApp;component/Resources/Images/Bime.ico");
                }
                return bime;
            }
        }
        /// <summary>
        /// Get source for AppIcon Icon
        /// </summary>
        public static IconAccessor AppIcon
        {
            get
            {
                if (appIcon == null)
                {
                    appIcon = new IconAccessor("/MicroControllerApp;component/Resources/Images/AppIcon.ico");
                }
                return appIcon;
            }
        }
        /// <summary>
        /// Get source for Help_Basic1 Icon
        /// </summary>
        public static IconAccessor Help_Basic1
        {
            get
            {
                if (help_Basic1 == null)
                {
                    help_Basic1 = new IconAccessor("/MicroControllerApp;component/Resources/Images/Helps/Basic1.png");
                }
                return help_Basic1;
            }
        }
        /// <summary>
        /// Get source for Help_Basic2 Icon
        /// </summary>
        public static IconAccessor Help_Basic2
        {
            get
            {
                if (help_Basic2 == null)
                {
                    help_Basic2 = new IconAccessor("/MicroControllerApp;component/Resources/Images/Helps/Basic2.png");
                }
                return help_Basic2;
            }
        }
        /// <summary>
        /// Get source for Help_Basic3 Icon
        /// </summary>
        public static IconAccessor Help_Basic3
        {
            get
            {
                if (help_Basic3 == null)
                {
                    help_Basic3 = new IconAccessor("/MicroControllerApp;component/Resources/Images/Helps/Basic3.png");
                }
                return help_Basic3;
            }
        }
        /// <summary>
        /// Get source for Help_Basic4 Icon
        /// </summary>
        public static IconAccessor Help_Basic4
        {
            get
            {
                if (help_Basic4 == null)
                {
                    help_Basic4 = new IconAccessor("/MicroControllerApp;component/Resources/Images/Helps/Basic4.png");
                }
                return help_Basic4;
            }
        }
        /// <summary>
        /// Get source for Help_Basic5 Icon
        /// </summary>
        public static IconAccessor Help_Basic5
        {
            get
            {
                if (help_Basic5 == null)
                {
                    help_Basic5 = new IconAccessor("/MicroControllerApp;component/Resources/Images/Helps/Basic5.png");
                }
                return help_Basic5;
            }
        }        
        /// <summary>
        /// Get source for Help_CapPara1 Icon
        /// </summary>
        public static IconAccessor Help_CapPara1
        {
            get
            {
                if (help_CapPara1 == null)
                {
                    help_CapPara1 = new IconAccessor("/MicroControllerApp;component/Resources/Images/Helps/CapPara1.png");
                }
                return help_CapPara1;
            }
        }       
        /// <summary>
        /// Get source for Help_Communication1 Icon
        /// </summary>
        public static IconAccessor Help_Communication1
        {
            get
            {
                if (help_Communication1 == null)
                {
                    help_Communication1 = new IconAccessor("/MicroControllerApp;component/Resources/Images/Helps/Communication1.png");
                }
                return help_Communication1;
            }
        }        
        /// <summary>
        /// Get source for Help_Communication2 Icon
        /// </summary>
        public static IconAccessor Help_Communication2
        {
            get
            {
                if (help_Communication2 == null)
                {
                    help_Communication2 = new IconAccessor("/MicroControllerApp;component/Resources/Images/Helps/Communication2.png");
                }
                return help_Communication2;
            }
        }       
        /// <summary>
        /// Get source for Help_Communication3 Icon
        /// </summary>
        public static IconAccessor Help_Communication3
        {
            get
            {
                if (help_Communication3 == null)
                {
                    help_Communication3 = new IconAccessor("/MicroControllerApp;component/Resources/Images/Helps/Communication3.png");
                }
                return help_Communication3;
            }
        }       
        /// <summary>
        /// Get source for Help_Communication4 Icon
        /// </summary>
        public static IconAccessor Help_Communication4
        {
            get
            {
                if (help_Communication4 == null)
                {
                    help_Communication4 = new IconAccessor("/MicroControllerApp;component/Resources/Images/Helps/Communication4.png");
                }
                return help_Communication4;
            }
        }        
        /// <summary>
        /// Get source for Help_Graphic1 Icon
        /// </summary>
        public static IconAccessor Help_Graphic1
        {
            get
            {
                if (help_Graphic1 == null)
                {
                    help_Graphic1 = new IconAccessor("/MicroControllerApp;component/Resources/Images/Helps/Graphic1.png");
                }
                return help_Graphic1;
            }
        }       
        /// <summary>
        /// Get source for Help_Graphic2 Icon
        /// </summary>
        public static IconAccessor Help_Graphic2
        {
            get
            {
                if (help_Graphic2 == null)
                {
                    help_Graphic2 = new IconAccessor("/MicroControllerApp;component/Resources/Images/Helps/Graphic2.png");
                }
                return help_Graphic2;
            }
        }        
        /// <summary>
        /// Get source for Help_Graphic3 Icon
        /// </summary>
        public static IconAccessor Help_Graphic3
        {
            get
            {
                if (help_Graphic3 == null)
                {
                    help_Graphic3 = new IconAccessor("/MicroControllerApp;component/Resources/Images/Helps/Graphic3.png");
                }
                return help_Graphic3;
            }
        }        
        /// <summary>
        /// Get source for Help_MatFile1 Icon
        /// </summary>
        public static IconAccessor Help_MatFile1
        {
            get
            {
                if (help_MatFile1 == null)
                {
                    help_MatFile1 = new IconAccessor("/MicroControllerApp;component/Resources/Images/Helps/MatFile1.png");
                }
                return help_MatFile1;
            }
        }        
        /// <summary>
        /// Get source for Help_Position1 Icon
        /// </summary>
        public static IconAccessor Help_Position1
        {
            get
            {
                if (help_Position1 == null)
                {
                    help_Position1 = new IconAccessor("/MicroControllerApp;component/Resources/Images/Helps/Position1.png");
                }
                return help_Position1;
            }
        }     
        /// <summary>
        /// Get source for Help_PressFile1 Icon
        /// </summary>
        public static IconAccessor Help_PressFile1
        {
            get
            {
                if (help_PressFile1 == null)
                {
                    help_PressFile1 = new IconAccessor("/MicroControllerApp;component/Resources/Images/Helps/PressFile1.png");
                }
                return help_PressFile1;
            }
        }
        /// <summary>
        /// Get source for Help_DeviceManager Icon
        /// </summary>
        public static IconAccessor Help_DeviceManager
        {
            get
            {
                if (help_DeviceManager == null)
                {
                    help_DeviceManager = new IconAccessor("/MicroControllerApp;component/Resources/Images/Helps/DeviceManager.png");
                }
                return help_DeviceManager;
            }
        }
        /// <summary>
        /// Get source for Help_PSOC4_1 Icon
        /// </summary>
        public static IconAccessor Help_PSOC4_Driver_1
        {
            get
            {
                if (help_PSOC4_Driver_1 == null)
                {
                    help_PSOC4_Driver_1 = new IconAccessor("/MicroControllerApp;component/Resources/Images/Helps/PSOC4_Driver_1.png");
                }
                return help_PSOC4_Driver_1;
            }
        }
        /// <summary>
        /// Get source for Help_PSOC4_2 Icon
        /// </summary>
        public static IconAccessor Help_PSOC4_Driver_2
        {
            get
            {
                if (help_PSOC4_Driver_2 == null)
                {
                    help_PSOC4_Driver_2 = new IconAccessor("/MicroControllerApp;component/Resources/Images/Helps/PSOC4_Driver_2.png");
                }
                return help_PSOC4_Driver_2;
            }
        }
        /// <summary>
        /// Get source for Help_PSOC4_3 Icon
        /// </summary>
        public static IconAccessor Help_PSOC4_Driver_3
        {
            get
            {
                if (help_PSOC4_Driver_3 == null)
                {
                    help_PSOC4_Driver_3 = new IconAccessor("/MicroControllerApp;component/Resources/Images/Helps/PSOC4_Driver_3.png");
                }
                return help_PSOC4_Driver_3;
            }
        }

        
        /// <summary>
        /// Get source for Help_PSOC5_Driver_1 Icon
        /// </summary>
        public static IconAccessor Help_PSOC5_Driver_1
        {
            get
            {
                if (help_PSOC5_Driver_1 == null)
                {
                    help_PSOC5_Driver_1 = new IconAccessor("/MicroControllerApp;component/Resources/Images/Helps/PSOC5_Driver_1.png");
                }
                return help_PSOC5_Driver_1;
            }
        }

        
        /// <summary>
        /// Get source for Help_PSOC5_Driver_2 Icon
        /// </summary>
        public static IconAccessor Help_PSOC5_Driver_2
        {
            get
            {
                if (help_PSOC5_Driver_2 == null)
                {
                    help_PSOC5_Driver_2 = new IconAccessor("/MicroControllerApp;component/Resources/Images/Helps/PSOC5_Driver_2.png");
                }
                return help_PSOC5_Driver_2;
            }
        }

        

        /// <summary>
        /// Get source for Help_PSOC5_Driver_3 Icon
        /// </summary>
        public static IconAccessor Help_PSOC5_Driver_3
        {
            get
            {
                if (help_PSOC5_Driver_3 == null)
                {
                    help_PSOC5_Driver_3 = new IconAccessor("/MicroControllerApp;component/Resources/Images/Helps/PSOC5_Driver_3.png");
                }
                return help_PSOC5_Driver_3;
            }
        }


        /// <summary>
        /// Get source for Help_Press1 Icon
        /// </summary>
        public static IconAccessor Help_Press1
        {
            get
            {
                if (help_Press1 == null)
                {
                    help_Press1 = new IconAccessor("/MicroControllerApp;component/Resources/Images/Helps/Press1.png");
                }
                return help_Press1;
            }
        }        
        /// <summary>
        /// Get source for Help_Press2 Icon
        /// </summary>
        public static IconAccessor Help_Press2
        {
            get
            {
                if (help_Press2 == null)
                {
                    help_Press2 = new IconAccessor("/MicroControllerApp;component/Resources/Images/Helps/Press2.png");
                }
                return help_Press2;
            }
        }        
        /// <summary>
        /// Get source for Help_Press3 Icon
        /// </summary>
        public static IconAccessor Help_Press3
        {
            get
            {
                if (help_Press3 == null)
                {
                    help_Press3 = new IconAccessor("/MicroControllerApp;component/Resources/Images/Helps/Press3.png");
                }
                return help_Press3;
            }
        }        
        /// <summary>
        /// Get source for Help_Press4 Icon
        /// </summary>
        public static IconAccessor Help_Press4
        {
            get
            {
                if (help_Press4 == null)
                {
                    help_Press4 = new IconAccessor("/MicroControllerApp;component/Resources/Images/Helps/Press4.png");
                }
                return help_Press4;
            }
        }       
        /// <summary>
        /// Get source for Help_Press5 Icon
        /// </summary>
        public static IconAccessor Help_Press5
        {
            get
            {
                if (help_Press5 == null)
                {
                    help_Press5 = new IconAccessor("/MicroControllerApp;component/Resources/Images/Helps/Press5.png");
                }
                return help_Press5;
            }
        }        
        /// <summary>
        /// Get source for Help_Press6 Icon
        /// </summary>
        public static IconAccessor Help_Press6
        {
            get
            {
                if (help_Press6 == null)
                {
                    help_Press6 = new IconAccessor("/MicroControllerApp;component/Resources/Images/Helps/Press6.png");
                }
                return help_Press6;
            }
        }       
        /// <summary>
        /// Get source for Help_Press7 Icon
        /// </summary>
        public static IconAccessor Help_Press7
        {
            get
            {
                if (help_Press7 == null)
                {
                    help_Press7 = new IconAccessor("/MicroControllerApp;component/Resources/Images/Helps/Press7.png");
                }
                return help_Press7;
            }
        }    
        /// <summary>
        /// Get source for Help_Press8 Icon
        /// </summary>
        public static IconAccessor Help_Press8
        {
            get
            {
                if (help_Press8 == null)
                {
                    help_Press8 = new IconAccessor("/MicroControllerApp;component/Resources/Images/Helps/Press8.png");
                }
                return help_Press8;
            }
        }
        /// <summary>
        /// Get source for Help_Press9 Icon
        /// </summary>
        public static IconAccessor Help_Press9
        {
            get
            {
                if (help_Press9 == null)
                {
                    help_Press9 = new IconAccessor("/MicroControllerApp;component/Resources/Images/Helps/Press9.png");
                }
                return help_Press9;
            }
        }

    }
}
