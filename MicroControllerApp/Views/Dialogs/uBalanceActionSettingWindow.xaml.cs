﻿using LongWpfUI.Models.Commands;
using LongWpfUI.Windows;
using MicroControllerApp.Models.Base;
using MicroControllerApp.Models.DataClasses.Settings.Others;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using LongWpfUI.Helpers;

namespace MicroControllerApp.Views.Dialogs
{    
    public partial class uBalanceActionSettingWindow : DialogBase
    {
        #region Field
        private uBalanceChannelAction action;
        private bool isNew;
        #endregion

        #region Constructor        
        public uBalanceActionSettingWindow(Window owner, uBalanceChannelAction action, bool isNew, ICommand helpCommand)
        {
            //Set Model Field, Property
            this.DataContext = action;
            this.Owner = owner;
            this.action = action;
            this.isNew = isNew;

            //Initialize          
            InitializeComponent();
            
            //Set View Property
            this.helpBtn.Command = helpCommand;
            this.dialogPanel.Visibility = isNew ? Visibility.Visible : Visibility.Collapsed;
            this.triggerPanel.Visibility = (this.action.Action == uBalanceChannelInfoBits.Trigger) ? Visibility.Visible : Visibility.Collapsed;
            this.SaveCommand = new WpfRelayCommand(Exe_Save);
            this.CancelCommand = new WpfRelayCommand(Exe_Cancel);

            //subscribe Event
            this.Closing += UBalanceActionSettingWindow_Closing;
            this.actionComboBox.SelectionChanged += ActionComboBox_SelectionChanged;
        }        
        #endregion

        #region Private Method
        protected override void Exe_Save()
        {
            this.DialogResult = true;
            this.Close();
        }

        protected override void Exe_Cancel()
        {
            this.DialogResult = false;
            this.Close();
        }
        #endregion

        #region Event Handler
        private void UBalanceActionSettingWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //Edit Mode or Save in New Mode
            if (!this.isNew || (this.DialogResult != null && this.DialogResult.Value))  //NOT
            {
                try
                {
                    string message = string.Empty;
                    //Check UI Input
                    if (HasInputError(this.MainPanel, out message))
                    {
                        throw new Exception(message);
                    }
                    //Compare Parameter
                    else 
                    {
                        if (this.action.Index_5V >= this.action.Length)
                        {
                            throw new Exception("Index of 5V muss be smaller than Length");
                        }
                        if (this.action.Index_12V >= this.action.Length)
                        {
                            throw new Exception("Index of 12V muss be smaller than Length");
                        }
                        if ((this.action.Index_5V + this.action.Width) >= this.action.Length)
                        {
                            throw new Exception("Sum of Index 5V and Width muss be smaller than Length");
                        }
                        if ((this.action.Index_12V + this.action.Width) >= this.action.Length)
                        {
                            throw new Exception("Sum of Index 12V and Width muss be smaller than Length");
                        }
                        //In Edit Mode
                        this.DialogResult = true;
                    }
                }
                catch (Exception ex)
                {
                    e.Cancel = true;
                    this.DialogResult = false;
                    MessageBox.Show(ex.Message, "Invalid Input", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
        }

        private void ActionComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.action != null && this.triggerPanel != null)
            {
                this.triggerPanel.Visibility = (this.action.Action == uBalanceChannelInfoBits.Trigger) ? Visibility.Visible : Visibility.Collapsed;
            }
        }
        #endregion
    }
}
