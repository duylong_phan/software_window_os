﻿using LongWpfUI.Models.Commands;
using LongWpfUI.Windows;
using MicroControllerApp.Models.DataClasses.Settings.Others;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;

namespace MicroControllerApp.Views.Dialogs
{
    public partial class MotorCtrMotorActionSettingWindow : DialogBase
    {
        #region Field
        private MotorCtrMotorAction action;
        private bool isNew;
        #endregion

        #region Constructor
        public MotorCtrMotorActionSettingWindow(Window owner, MotorCtrMotorAction action, bool isNew, ICommand helpCommand)
        {
            //Set Model Field, Property
            this.DataContext = action;
            this.Owner = owner;
            this.action = action;
            this.isNew = isNew;
            InitializeComponent();

            //Set View Property
            this.helpBtn.Command = helpCommand;
            this.dialogPanel.Visibility = isNew ? Visibility.Visible : Visibility.Collapsed;            
            this.SaveCommand = new WpfRelayCommand(Exe_Save);
            this.CancelCommand = new WpfRelayCommand(Exe_Cancel);

            UpdateActionPanel();
            //subscribe Event
            this.Closing += MotorCtrMotorActionSettingWindow_Closing;
            this.actionComboBox.SelectionChanged += ActionComboBox_SelectionChanged;
        }
        #endregion

        #region Event Handler
        private void ActionComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateActionPanel();
        }

        private void MotorCtrMotorActionSettingWindow_Closing(object sender, CancelEventArgs e)
        {
            //Edit Mode or Save in New Mode
            if (!this.isNew || (this.DialogResult != null && this.DialogResult.Value))
            {
                try
                {
                    string message = string.Empty;
                    //Check UI Input
                    if (HasInputError(this.MainPanel, out message))
                    {
                        throw new Exception(message);
                    }
                    //In Edit Mode
                    this.DialogResult = true;
                }
                catch (Exception ex)
                {
                    e.Cancel = true;
                    this.DialogResult = false;
                    MessageBox.Show(ex.Message, "Invalid Input", MessageBoxButton.OK, MessageBoxImage.Information);
                }                
            }
        }
        #endregion

        #region Private Method
        protected override void Exe_Save()
        {
            this.DialogResult = true;
            this.Close();
        }

        protected override void Exe_Cancel()
        {
            this.DialogResult = false;
            this.Close();
        }

        private void UpdateActionPanel()
        {
            if (this.action.ActionType != (byte)MotorControl1Flags.Trigger)
            {
                this.delayPanel.Visibility = Visibility.Visible;
                this.triggerPanel.Visibility = Visibility.Collapsed;
            }
            else
            {
                this.delayPanel.Visibility = Visibility.Collapsed;
                this.triggerPanel.Visibility = Visibility.Visible;
            }
        }
        #endregion
    }
}
