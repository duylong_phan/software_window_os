﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MicroControllerApp.Models.Base;
using MicroControllerApp.Models.DataClasses.Settings;
using LongWpfUI.Windows;
using LongWpfUI.Models.Commands;
using MicroControllerApp.Models.ViewClasses.Options;
using MicroControllerApp.Models.ViewClasses.Selectors;
using MicroControllerApp.Models.DataClasses.Settings.Sub;

namespace MicroControllerApp.Views.Dialogs
{
    public partial class ConfigWindow : DialogBase
    {
        #region DP
        public MicroTasks Task
        {
            get { return (MicroTasks)GetValue(TaskProperty); }
            set { SetValue(TaskProperty, value); }
        }
        public AppPositions Position
        {
            get { return (AppPositions)GetValue(PositionProperty); }
            set { SetValue(PositionProperty, value); }
        }
        public PhysicalPorts Port
        {
            get { return (PhysicalPorts)GetValue(PortProperty); }
            set { SetValue(PortProperty, value); }
        }
        
        public static readonly DependencyProperty TaskProperty =
            DependencyProperty.Register("Task", typeof(MicroTasks), typeof(ConfigWindow), new PropertyMetadata(MicroTasks.Non));
        public static readonly DependencyProperty PositionProperty =
            DependencyProperty.Register("Position", typeof(AppPositions), typeof(ConfigWindow), new PropertyMetadata(AppPositions.Local));
        public static readonly DependencyProperty PortProperty =
            DependencyProperty.Register("Port", typeof(PhysicalPorts), typeof(ConfigWindow), new PropertyMetadata(PhysicalPorts.Serial));
        #endregion

        #region Field
        private bool isNew;
        private Configuration config;        
        private ContentControl connectionControl;
        private TabControl settingTabControl;
        private List<TabItem> localTabCollection;
        private List<TabItem> remoteTabCollection;
        #endregion

        #region Constructor
        public ConfigWindow(Window owner, Configuration config, bool isNew)
        {
            #region Initialize
            //Initalize Value => important, at beginning
            this.Task = config.Task;
            foreach (var item in App.ViewModel.InputParameter.TaskOptions)
            {
                if (item.Type == config.Task && item.Content is SettingBase)
                {
                    var setting = item.Content as SettingBase;
                    this.Position = setting.Position;
                    this.Port = setting.Port;
                    break;
                }
            }
                        
            //Support event
            this.Initialized += ConfigWindow_Initialized;
            this.Closing += ConfigWindow_Closing;
            //API
            InitializeComponent();
            #endregion

            //Fields
            this.localTabCollection = new List<TabItem>();
            this.remoteTabCollection = new List<TabItem>();

            //Field
            this.config = config;
            this.isNew = isNew;
            //MVVM
            this.Owner = owner;
            this.DataContext = App.ViewModel;

            if (isNew)
            {                
                this.Title = "Create new Configuration";
                this.SaveCommand = new WpfRelayCommand(Exe_Save);
                this.CancelCommand = new WpfRelayCommand(Exe_Cancel);
            }
            else
            {
                this.Title = "Edit configuration";
                this.btnPanel.Visibility = Visibility.Collapsed;
            }
        }
        #endregion

        #region Protected Method
        protected override void Exe_Save()
        {
            string message = string.Empty;
            bool hasError = HasInputError(this.MainPanel, out message);
            if (hasError)
            {
                MessageBox.Show(message, "Invalid Input value");
                return;
            }
            this.DialogResult = true;
            this.Close();
        }

        protected override void Exe_Cancel()
        {
            this.DialogResult = false;
            this.Close();
        }
        #endregion

        #region Event Handler

        #region Window
        private void ConfigWindow_Initialized(object sender, EventArgs e)
        {
            this.Initialized -= ConfigWindow_Initialized;

            //assign DataTemplate for TemplateSelector
            var selector = this.Resources["configTemplateSelector"] as ConfigTemplateSelector;
            if (selector != null)
            {
                //External Resource
                selector.Serial = App.Current.Resources["xDialogSerialTemplate"] as DataTemplate;
                selector.File = App.Current.Resources["xDialogSingleFileTemplate"] as DataTemplate;
                //Local Resource
                selector.Usb = this.Resources["appExternUsbTemplate"] as DataTemplate;
                selector.PressSetting = this.Resources["appPressSettingTemplate"] as DataTemplate;
                selector.CapacitorSetting = this.Resources["appCapacitorSettingTemplate"] as DataTemplate;
                selector.AccelerometerSetting = this.Resources["appAccelerometerSettingTemplate"] as DataTemplate;
                selector.uBalanceSetting = this.Resources["appuBalaceSettingTemplate"] as DataTemplate;
                selector.RfTransSetting = this.Resources["appRfTransSettingTemplate"] as DataTemplate;
                selector.MotorCtrSetting = this.Resources["appMotorCtrSettingTemplate"] as DataTemplate;
                selector.Graphic = this.Resources["appGraphicTemplate"] as DataTemplate;
                selector.Server = this.Resources["appServerTemplate"] as DataTemplate;
                selector.Remote = this.Resources["appRemoteTemplate"] as DataTemplate;
                selector.Local = this.Resources["appLocalFileTemplate"] as DataTemplate;
                selector.PressFile = this.Resources["appPressFileTemplate"] as DataTemplate;
                selector.FileChanged = this.Resources["appFileChangedTemplate"] as DataTemplate;
                selector.MotorBalanceSetting = this.Resources["appMotorBalanceTemplate"] as DataTemplate;
            }
        }

        private void ConfigWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //Save is enable, or in Edit Mode
            if (this.DialogResult == true || !this.isNew)       //NOT
            {
                //assign Task
                this.config.Task = this.Task;

                //assign Task Position, Port
                var option = this.taskComboBox.SelectedItem as MicroTaskOption;
                if (option != null && option.Content is SettingBase)
                {
                    var setting = option.Content as SettingBase;
                    setting.Position = this.Position;
                    setting.Port = this.Port;
                }
            }
        }
        #endregion

        #region Option Changed
        private void TaskComboBox_Initialized(object sender, EventArgs e)
        {
            //Initialize Value
            this.taskComboBox.SelectedValue = this.Task;
        }

        private void TaskComboBox_Changed(object sender, SelectionChangedEventArgs e)
        {          
            if (this.taskComboBox.SelectedItem is MicroTaskOption)
            {
                var option = (MicroTaskOption)this.taskComboBox.SelectedItem;
                var setting = option.Content as SettingBase;

                //Update Task
                this.Task = option.Type;

                //Update Position Content
                if (setting != null)
                {
                    foreach (var item in App.ViewModel.InputParameter.PositionOptions)
                    {
                        switch (item.Type)
                        {
                            case AppPositions.Remote:
                                item.Content = setting.RemoteSetting;
                                break;

                            case AppPositions.Local:
                            default:
                                //Update LocalSetting Content
                                foreach (var subItem in App.ViewModel.InputParameter.LocalSetting)
                                {
                                    switch (subItem.Type)
                                    {                                        
                                        case PhysicalPorts.USB:
                                            subItem.Content = setting.UsbSetting;
                                            break;

                                        case PhysicalPorts.WebServer:                                            
                                            subItem.Content = null;
                                            break;

                                        case PhysicalPorts.Serial:
                                        default:
                                            subItem.Content = setting.SerialPortSetting;
                                            break;
                                    }
                                }
                                App.ViewModel.InputParameter.LocalSetting.Server = setting.ServerSetting;
                                break;
                        }
                    }
                }

                //Panel, Control Visibility base on Task
                if (this.Task == MicroTasks.Non)
                {
                    this.postionPanel.Visibility = Visibility.Hidden;
                    this.settingControl.Visibility = Visibility.Hidden;
                }
                else
                {
                    this.postionPanel.Visibility = Visibility.Visible;
                    this.settingControl.Visibility = Visibility.Visible;
                }
            }
        }

        private void PostionComboBox_Initialized(object sender, EventArgs e)
        {
            //initialize Value
            this.positionComboBox.SelectedValue = this.Position;
        }

        private void PostionComboBox_Changed(object sender, SelectionChangedEventArgs e)
        {
            //Check if Position is available
            if (this.positionComboBox.SelectedValue is AppPositions)
            {
                this.Position = (AppPositions)this.positionComboBox.SelectedValue;                

                //update valid Connection
                UpdateConnectionControl();

                //Update valid Tab Setting
                switch (this.Position)
                {
                    case AppPositions.Remote:                        
                        foreach (var item in this.localTabCollection)
                        {
                            item.Visibility = Visibility.Collapsed;
                        }
                        foreach (var item in this.remoteTabCollection)
                        {
                            item.Visibility = Visibility.Visible;
                        }
                        break;

                    case AppPositions.Local:
                    default:                       
                        foreach (var item in this.localTabCollection)
                        {
                            item.Visibility = Visibility.Visible;
                        }
                        foreach (var item in this.remoteTabCollection)
                        {
                            item.Visibility = Visibility.Collapsed;
                        }
                        break;
                }

                //check if selected Tab is visible
                if (this.settingTabControl != null && this.settingTabControl.SelectedIndex > -1)
                {
                    var tabItem = this.settingTabControl.ItemContainerGenerator.ContainerFromIndex(this.settingTabControl.SelectedIndex) as TabItem;
                    //tab is not visible
                    if (tabItem != null && tabItem.Visibility != Visibility.Visible)
                    {
                        //enable Connection Tab => Tab 0
                        this.settingTabControl.SelectedIndex = 0;
                    }
                }
            }
        }
        
        private void PortComboBox_Initialized(object sender, EventArgs e)
        {
            //initialize Port Value
            var portComboBox = sender as ComboBox;
            if (portComboBox != null)
            {
                portComboBox.SelectedValue = this.Port;
            }
        }

        private void PortComboBox_Changed(object sender, SelectionChangedEventArgs e)
        {            
            var portComboBox = sender as ComboBox;
            if (portComboBox == null)
            {
                return;                                
            }

            //Update current Selected Port
            if (portComboBox.SelectedValue is PhysicalPorts)
            {
                this.Port = (PhysicalPorts)portComboBox.SelectedValue;
            }
            //Update Server enabled status
            if (portComboBox.DataContext is LocalSetting)
            {
                var setting = portComboBox.DataContext as LocalSetting;
                var serverSetting = setting.Server as ServerSetting;
                if (serverSetting != null && this.Port == PhysicalPorts.WebServer)
                {
                    serverSetting.IsEnabled = true;
                }
            }
        }

        private void Connection_Control_Loaded(object sender, RoutedEventArgs e)
        {
            this.connectionControl = sender as ContentControl;
            UpdateConnectionControl();
        }

        private void UpdateConnectionControl()
        {
            //update Content
            if (this.connectionControl != null && this.positionComboBox != null && this.positionComboBox.SelectedItem is AppPositionOption)
            {
                var option = this.positionComboBox.SelectedItem as AppPositionOption;
                this.connectionControl.Content = option.Content;
            }
        }
        
        #endregion

        #region Tab Setting
        private void LocalSetting_Tab_Loaded(object sender, RoutedEventArgs e)
        {
            var item = sender as TabItem;
            if (item != null)
            {
                this.localTabCollection.Add(item);
            }
        }
                
        private void LocalSetting_Tab_UnLoaded(object sender, RoutedEventArgs e)
        {
            var item = sender as TabItem;
            if (item != null && this.localTabCollection.Contains(item))
            {
                this.localTabCollection.Remove(item);
            }
        }

        private void RemoteSetting_Tab_Loaded(object sender, RoutedEventArgs e)
        {
            var item = sender as TabItem;
            if (item != null)
            {
                this.remoteTabCollection.Add(item);
            }
        }

        private void RemoteSetting_Tab_Unloaded(object sender, RoutedEventArgs e)
        {
            var item = sender as TabItem;
            if (item != null && this.localTabCollection.Contains(item))
            {
                this.remoteTabCollection.Remove(item);
            }
        }


        #endregion

        #region Else
        private void SettingTabControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.settingTabControl = sender as TabControl;
        }

        private void Help_Clicked(object sender, RoutedEventArgs e)
        {
            if (App.ViewModel != null)
            {
                App.ViewModel.ShowTaskHelp.TryToExecute(this.Task);
            }
        }


        #endregion

        private void WorkAsServer_Unchecked(object sender, RoutedEventArgs e)
        {
            var checkBox = sender as CheckBox;
            if (this.Port == PhysicalPorts.WebServer)
            {
                var exp = BindingOperations.GetBindingExpression(checkBox, CheckBox.IsCheckedProperty);
                if (exp.DataItem is ServerSetting)
                {
                    var setting = exp.DataItem as ServerSetting;
                    setting.IsEnabled = true;
                }
            }
        }

        #endregion
    }
}
