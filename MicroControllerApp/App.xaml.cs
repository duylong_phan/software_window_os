﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.IO;
using System.Windows.Threading;
using System.Xml.Linq;
using MicroControllerApp.ViewModels;
using System.Reflection;

namespace MicroControllerApp
{
    public enum AppEndStatus
    {
        Unknown,
        Normal,
        Error
    }

    public partial class App : Application
    {
        #region Static
        //Basic
        public static CultureInfo CultureFormat { get; private set; }
        public static AppEndStatus EndStatus { get; private set; }
        public static DateTime StartTime { get; private set; }
        public static string SoftwareName { get; private set; }
        public static string NumberFormat { get; private set; }
        public static string DateTimeFormat { get; private set; }
        public static string DateFormat { get; private set; }
        public static string DataPath { get; private set; }
        public static string ErrorFilePath { get; private set; }
        public static string LogFilePath { get; private set; }

        //Advance
        public static MVController ViewModel { get; private set; }
        public static string ConfigExtension { get; private set; }
        public static string ConfigFilter { get; private set; }
        public static string ExecutePath { get; private set; }
        public static string FireWallHelperPath { get; private set; }

        static App()
        {
            //Basic
            CultureFormat = new CultureInfo("de-DE");
            EndStatus = AppEndStatus.Unknown;
            StartTime = DateTime.Now;
            SoftwareName = "Micro Controller App";
            NumberFormat = "0.##";
            DateTimeFormat = "dd.MM.yyyy HH:mm:ss";
            DateFormat = "dd.MM.yyyy";
            DataPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), SoftwareName);
            ErrorFilePath = Path.Combine(DataPath, "Error_File.exml");
            LogFilePath = Path.Combine(DataPath, "Log_File.lxml");

            //Advance
            ConfigExtension = ".configx";
            ConfigFilter = string.Format("MicroControllerApp Configuration File (*{0})|*{0}", ConfigExtension);
            ExecutePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            FireWallHelperPath = Path.Combine(ExecutePath, "Resources", "Softwares", "FireWallHelper.exe");
        }

        #endregion

        #region Start Point
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            //#region Subscribe Event
            ////Exit
            //AppDomain.CurrentDomain.ProcessExit += CurrentDomain_ProcessExit;
            ////exception
            //AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            //this.DispatcherUnhandledException += App_DispatcherUnhandledException;
            //#endregion

            //#region Process Arguments
            //var requestedFile = string.Empty;
            //if (e.Args != null && e.Args.Length > 0)
            //{
            //    requestedFile = e.Args[0];
            //}
            //#endregion

            #region Show Window
            App.ViewModel = MVController.Instance;
            App.ViewModel.SetView(new MainWindow() { DataContext = App.ViewModel});
            App.ViewModel.View.ShowDialog();
            App.EndStatus = AppEndStatus.Normal;
            #endregion
        }
        #endregion

        #region Maintainance Method
        private void SaveLogFile()
        {
            try
            {
                DateTime stopTime = DateTime.Now;
                XElement element = new XElement("LogInfo");
                element.Add(new XAttribute("Start", StartTime.ToString(App.DateTimeFormat)));
                element.Add(new XAttribute("Stop", stopTime.ToString(App.DateTimeFormat)));
                element.Add(new XAttribute("Status", App.EndStatus));

                if (!Directory.Exists(App.DataPath))    //NOT
                {
                    Directory.CreateDirectory(App.DataPath);
                }

                using (FileStream stream = (File.Exists(App.LogFilePath)) ? File.Open(App.LogFilePath, FileMode.Append) : File.Create(App.LogFilePath))
                using (StreamWriter writer = new StreamWriter(stream))
                {
                    writer.WriteLine(element.ToString());
                }
            }
            catch (Exception)
            {
                //Do nothing => assume that it always works 
            }
        }

        private void SaveErrorFile(string type, string message, string trace)
        {
            try
            {
                XElement element = new XElement("ErrorInfo");
                element.Add(new XAttribute("Type", type));
                element.Add(new XAttribute("Time", DateTime.Now.ToString(App.DateTimeFormat)));
                element.Add(new XElement("Message", message));
                element.Add(new XElement("Trace", trace));

                if (!Directory.Exists(App.DataPath))    //NOT
                {
                    Directory.CreateDirectory(App.DataPath);
                }

                using (FileStream stream = (File.Exists(App.ErrorFilePath)) ? File.Open(App.ErrorFilePath, FileMode.Append) : File.Create(App.ErrorFilePath))
                using (StreamWriter writer = new StreamWriter(stream))
                {
                    writer.WriteLine(element.ToString());
                }
            }
            catch (Exception)
            {
                //Do nothing => assume that it always works 
            }
        }

        private void ShowApplicationError(Exception ex)
        {
            string message = string.Empty;
            string trace = string.Empty;
            string type = string.Empty;
            if (ex != null)
            {
                type = ex.GetType().ToString();
                message = ex.Message;
                trace = ex.StackTrace;
            }
            else
            {
                message = trace = type = "No Info";
            }

            SaveErrorFile(type, message, trace);

            message = "There was an unexcepted Error during the operation. The Software will be closed to prevent any further damages." + Environment.NewLine +
                      "Please contact the Programmer for more support." + Environment.NewLine + "Error detail: " + message;
            MessageBox.Show(message, "Software has error", MessageBoxButton.OK, MessageBoxImage.Warning);
        }
        #endregion

        #region Event Handler

        private void CurrentDomain_ProcessExit(object sender, EventArgs e)
        {
            SaveLogFile();
        }

        void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            ShowApplicationError(e.ExceptionObject as Exception);
            App.EndStatus = AppEndStatus.Error;
            Environment.Exit(0);
        }

        void App_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            ShowApplicationError(e.Exception);
            e.Handled = true;
            App.EndStatus = AppEndStatus.Error;
            this.Shutdown();
        }
        #endregion
    }
}
