﻿using LongInterface.Views;
using MicroControllerApp.Models.DataClasses.Settings;
using MicroControllerApp.Models.ViewClasses.Options;
using MicroControllerApp.Models.ViewClasses.Selectors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Ribbon;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LongWpfUI.Helpers;
using MicroControllerApp.Models.DataClasses.Settings.Others;
using LongWpfUI.Models.Commands;
using System.Collections;
using System.Collections.Specialized;
using MicroControllerApp.Models.ViewClasses.Infos;
using MicroControllerApp.Interfaces;
using LongInterface.ViewModels.Logics;
using System.Globalization;

namespace MicroControllerApp
{
    public enum AppStatusTypes
    {
        Normal,
        Error
    }

    public partial class MainWindow : Window, IView
    {
        #region Const
        public const int Index_ConfigTab = 0;
        public const int Index_OperTab = 1;
        public const int Index_FileTab = 2;
        #endregion

        #region Getter
        public string RequestedFile { get; set; }
        #endregion

        #region Field
        //Multi Choice View
        private ContentControl viewControl1;
        private ContentControl viewControl2;

        //ScrollView for multi Data
        private ScrollViewer dataScrollView;

        //Panel Instance
        private Panel matSamplePanel;
        private Panel inputParaPanel;

        //File reader
        private RibbonGroup fileFunctionGroup;
        private TabControl readerTabControl;

        //press File Reader
        private int group_ExpandCount;
        private int filter_ExpandCount;
        private ContentControl group_HeaderControl;
        private ContentControl filter_HeaderControl;

        //Package Editor
        private ListBox packageListBox;

        //OutPin Handler
        private ContentControl outPinFunctionSettingControl;
        private TextBox _manualAngleServoTxt;
        private ComboBox _sendAngleServorCombobox;

        #endregion

        #region Constructor
        public MainWindow()
        {
            this.Initialized += MainWindow_Initialized;
            this.Loaded += MainWindow_Loaded;
            this.ContentRendered += MainWindow_ContentRendered;
            this.Closing += MainWindow_Closing;
            InitializeComponent();

            this.group_ExpandCount = 0;
            this.filter_ExpandCount = 0;
        }
        #endregion

        #region Public Method
        public void CloseView()
        {
            this.Close();
        }

        public void ShowView()
        {
            this.ShowDialog();
        }
        
        public void UpdateAppStatusColor(AppStatusTypes type)
        {
            var brush = Brushes.SteelBlue;
            switch (type)
            {                
                case AppStatusTypes.Error:
                    brush = Brushes.Red;
                    break;

                case AppStatusTypes.Normal:
                default:
                    //as default
                    break;
            }
            this.appStatusPanel.Background = brush;
        }

        public void SetViewPanelStatus(bool isEnabled)
        {
            if (this.viewOptionPanel != null && this.viewOptionPanel.IsEnabled != isEnabled)
            {
                this.viewOptionPanel.IsEnabled = isEnabled;
            }
        }

        public void UpdateRibbonTabIndex(int index)
        {
            if (this.ribbon != null)
            {
                this.ribbon.SelectedIndex = index;
            }
        }

        public void UpdateFileFunction(IEnumerable collection)
        {
            if (this.fileFunctionGroup != null)
            {
                this.fileFunctionGroup.ItemsSource = collection;
                this.fileFunctionGroup.Visibility = (this.fileFunctionGroup.Items.Count > 0) ? Visibility.Visible : Visibility.Collapsed;             
            }
        }

        public void ScrollViewToBottom()
        {
            if (this.dataScrollView != null)
            {                
                this.dataScrollView.ScrollToBottom();
            }
        }

        public void ShowSamplePanel(bool isShown)
        {
            if (this.matSamplePanel != null)
            {
                this.matSamplePanel.Visibility = isShown ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public void UpdatePackageListBoxSelectedIndex(int index)
        {
            if (this.packageListBox != null)
            {
                this.packageListBox.SelectedIndex = index;
            }
        }

        public int GetAngleServoOption()
        {
            return _sendAngleServorCombobox.SelectedIndex;
        }

        public bool GetManualAngleServo(out double angle)
        {            
            var errorMessage = string.Empty;
            angle = 0;
            try
            {
                if (_manualAngleServoTxt.Text.IndexOf(',') > -1)
                {
                    throw new Exception("Please use dot instead of comma");
                }
                angle = double.Parse(_manualAngleServoTxt.Text, CultureInfo.InvariantCulture);               
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
            }

            if (string.IsNullOrEmpty(errorMessage))
            {
                return true;
            }
            else
            {
                App.ViewModel.OutputParameter.UpdateAppStatus("Invalid value: " + errorMessage, AppStatusTypes.Error);
                return false;
            }
        }

        private bool CheckInputParameter(out string message)
        {
            bool isOk = true;
            message = string.Empty;

            if (this.inputParaPanel != null)
            {
                var collection = ControlHelper.GetChildrenControl<TextBox>(this.inputParaPanel);
                foreach (var item in collection)
                {
                    if (item != null && Validation.GetHasError(item))
                    {
                        isOk = false;
                        break;
                    }
                }
            }

            return isOk;
        }


        #endregion

        #region Event Handler

        #region Window
        private void MainWindow_Initialized(object sender, EventArgs e)
        {
            this.Initialized -= MainWindow_Initialized;
            var viewSelector = this.Resources["viewTemplateSelector"] as ViewTemplateSelector;
            if (viewSelector != null)
            {
                viewSelector.Graphic = this.Resources["appGraphicTemplate"] as DataTemplate;
                viewSelector.OutPinCtr = this.Resources["appOutPinControlTemplate"] as DataTemplate;
                viewSelector.InPinCtr = this.Resources["appInPinControlTemplate"] as DataTemplate;
                viewSelector.PressTable = this.Resources["appPressTableTemplate"] as DataTemplate;
                viewSelector.PressLocalControl = this.Resources["appPressLocalControlTemplate"] as DataTemplate;
                viewSelector.PressRemoteControl = this.Resources["appPressRemoteControlTemplate"] as DataTemplate;
                viewSelector.CapControl = this.Resources["appCapControlTemplate"] as DataTemplate;
                viewSelector.AccControl = this.Resources["appAccControlTemplate"] as DataTemplate;
                viewSelector.uBalaceEditor = this.Resources["appuBalanceEditorTemplate"] as DataTemplate;
                viewSelector.uBalanceControl = this.Resources["appuBalaceControlTemplate"] as DataTemplate;
                viewSelector.RfTransControl = this.Resources["appRfTransControlTemplate"] as DataTemplate;
                viewSelector.MotorCtrContol = this.Resources["appMotorCtrControlTemplate"] as DataTemplate;                
                viewSelector.MotorCtrEditor = this.Resources["appMotorCtrEditorTemplate"] as DataTemplate;
            }

            var fileSelector = this.Resources["fileContentTemplateSelector"] as FileContentTemplateSelector;
            if (fileSelector != null)
            {
                fileSelector.Press = this.Resources["appPressExtensionTemplate"] as DataTemplate;
            }
        }       

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= MainWindow_Loaded;
        }

        private void MainWindow_ContentRendered(object sender, EventArgs e)
        {
            this.ContentRendered -= MainWindow_ContentRendered;            

            if (App.ViewModel != null)
            {
                //Support double run Software from Config File
                if (!string.IsNullOrEmpty(this.RequestedFile))  //NOT
                {
                    App.ViewModel.LoadConfig.TryToExecute(this.RequestedFile);
                }
                App.ViewModel.OutputParameter.UpdateAppStatus("Ready", AppStatusTypes.Normal);
            }
        }

        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (App.ViewModel != null)
            {
                if (!App.ViewModel.IsDisposed)  //NOT
                {
                    e.Cancel = true;
                    App.ViewModel.Dispose();
                }
            }
        }

        #endregion

        #region Ribbon
        private void RibbonTab_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.ribbon != null && this.mainViewControl != null)
            {
                DataTemplate template = null;
                switch (this.ribbon.SelectedIndex)
                {
                    case 0:
                    case 1:
                        template = this.Resources["appViewMultiChoiceTemplate"] as DataTemplate;
                        break;

                    case 2:
                        template = this.Resources["appFileReaderTemplate"] as DataTemplate;
                        break;

                    default:
                        //Ignore
                        break;
                }
                this.mainViewControl.ContentTemplate = template;
            }
        }

        private void RibbonGroup_FileFunction_Loaded(object sender, RoutedEventArgs e)
        {
            this.fileFunctionGroup = sender as RibbonGroup;
        }

        #endregion

        #region Get Instance        
        private void ScrollableControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (sender is ScrollViewer)
            {
                this.dataScrollView = sender as ScrollViewer;
            }
            else if (sender is FrameworkElement)
            {
                var parent = sender as FrameworkElement;
                var child = ControlHelper.GetChildControl<ScrollViewer>(parent);
                if (child != null)
                {
                    this.dataScrollView = child;
                }
            }
        }
                
        private void SamplePanel_Loaded(object sender, RoutedEventArgs e)
        {
            this.matSamplePanel = sender as Panel;
        }
                
        private void InputParaPanel_Loaded(object sender, RoutedEventArgs e)
        {
            this.inputParaPanel = sender as Panel;
        }
                
        private void InputParaPanel_Unloaded(object sender, RoutedEventArgs e)
        {
            this.inputParaPanel = null;
        }
                
        private void Group_HeaderPanel_Loaded(object sender, RoutedEventArgs e)
        {
            this.group_HeaderControl = sender as ContentControl;
        }
        
        private void Fileter_HeaderPanel_Loaded(object sender, RoutedEventArgs e)
        {
            this.filter_HeaderControl = sender as ContentControl;
        }

        private void ManualAngleServo_Loaded(object sender, RoutedEventArgs e)
        {
            _manualAngleServoTxt = sender as TextBox;
        }

        private void SendAngleServor_Loaded(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            if (button != null)
            {
                button.Command = App.ViewModel.OutPin_SendAngleServor;
            }
        }

        private void SendAngleServorCombobox_Loaded(object sender, RoutedEventArgs e)
        {
            _sendAngleServorCombobox = sender as ComboBox;
        }
        #endregion

        #region Multi Choice View
        //Get Instance
        private void ViewControl1_Loaded(object sender, RoutedEventArgs e)
        {
            this.viewControl1 = sender as ContentControl;
        }

        //Get Instance
        private void ViewControl2_Loaded(object sender, RoutedEventArgs e)
        {
            this.viewControl2 = sender as ContentControl;
        }

        //Get View
        private void viewComboBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.viewComboBox1.SelectedItem is ViewOption)
            {
                var option = this.viewComboBox1.SelectedItem as ViewOption;
                this.viewControl1.Visibility = (option.Type == AppViews.Non) ? Visibility.Collapsed : Visibility.Visible;
                if (App.ViewModel != null)
                {
                    App.ViewModel.InputParameter.UpdateLeftView(option.Type);
                }
            }
        }

        //Get View
        private void viewComboBox2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.viewComboBox2.SelectedItem is ViewOption)
            {
                var option = this.viewComboBox2.SelectedItem as ViewOption;
                this.viewControl2.Visibility = (option.Type == AppViews.Non) ? Visibility.Collapsed : Visibility.Visible;
                if (App.ViewModel != null)
                {
                    App.ViewModel.InputParameter.UpdateRightView(option.Type);
                }
            }
        }

        #endregion
        
        #region TabControl for File Reader
        //File Viewer selected
        private void FileReader_Loaded(object sender, RoutedEventArgs e)
        {
            //Subscribte collection Event
            this.readerTabControl = sender as TabControl;
            if (readerTabControl != null)
            {
                var collection = readerTabControl.Items as INotifyCollectionChanged;
                if (collection != null)
                {
                    collection.CollectionChanged += Collection_CollectionChanged;
                }
            }
        }

        //File Viewer deselected
        private void FileReader_UnLoaded(object sender, RoutedEventArgs e)
        {
            //dispose Collection event
            this.readerTabControl = sender as TabControl;
            if (this.readerTabControl != null)
            {
                var collection = this.readerTabControl.Items as INotifyCollectionChanged;
                if (collection != null)
                {
                    collection.CollectionChanged -= Collection_CollectionChanged;
                }
            }
        }

        //Page added, or removed
        private void Collection_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {            
            //select last Page
            if (this.readerTabControl != null)
            {
                this.readerTabControl.SelectedIndex = this.readerTabControl.Items.Count - 1;
            }
        }       

        //Assign GroupDescription
        private void Group_ItemsControl_Loaded(object sender, RoutedEventArgs e)
        {
            var control = sender as ItemsControl;
            if (control != null)
            {
                control.Items.GroupDescriptions.Clear();
                control.Items.GroupDescriptions.Add(new PropertyGroupDescription("Group"));
            }
        }

        //update supported Function
        private void FileReader_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            #region Load available Function
            IEnumerable functions = null;
            var tabControl = sender as TabControl;
            if (tabControl != null && tabControl.SelectedItem is FileContentInfo)
            {
                var info = tabControl.SelectedItem as FileContentInfo;
                functions = info.Functions;                
            }
            UpdateFileFunction(functions);
            #endregion

            #region Reset Column Header
            this.filter_ExpandCount = 0;
            this.group_ExpandCount = 0;
            if (this.group_HeaderControl != null)
            {
                this.group_HeaderControl.Visibility = Visibility.Collapsed;
            }
            if (this.filter_HeaderControl != null)
            {
                this.filter_HeaderControl.Visibility = Visibility.Collapsed;
            }
            #endregion
        }
        
        //Update Visibility
        private void Group_Expander_Expanded(object sender, RoutedEventArgs e)
        {
            this.group_ExpandCount++;
            if (this.group_ExpandCount > 0 && this.group_HeaderControl != null)
            {
                this.group_HeaderControl.Visibility = Visibility.Visible;
            }
        }

        //Update Visibility
        private void Group_Expander_Collapsed(object sender, RoutedEventArgs e)
        {
            this.group_ExpandCount--;
            if (this.group_ExpandCount <= 0 && this.group_HeaderControl != null)
            {
                this.group_HeaderControl.Visibility = Visibility.Collapsed;
            }
        }

        //Update Visibility
        private void Filter_Expander_Expanded(object sender, RoutedEventArgs e)
        {
            this.filter_ExpandCount++;
            if (this.filter_ExpandCount > 0 && this.filter_HeaderControl != null)
            {
                this.filter_HeaderControl.Visibility = Visibility.Visible;
            }
        }

        //Update Visibility
        private void Filter_Expander_Collapsed(object sender, RoutedEventArgs e)
        {
            this.filter_ExpandCount--;
            if (this.filter_ExpandCount <= 0 && this.filter_HeaderControl != null)
            {
                this.filter_HeaderControl.Visibility = Visibility.Collapsed;
            }
        }
        #endregion

        #region Press File
        //Update Width
        private void Group_ItemsControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            var control = sender as FrameworkElement;
            if (control != null && this.group_HeaderControl != null)
            {
                this.group_HeaderControl.Width = control.ActualWidth;
            }
        }

        //Update Width
        private void Filter_ItemsControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            var control = sender as FrameworkElement;
            if (control != null && this.filter_HeaderControl != null)
            {
                this.filter_HeaderControl.Width = control.ActualWidth;
            }
        }

        //Assign Background
        private void PressItem_ItemsControl_Loaded(object sender, RoutedEventArgs e)
        {
            var panel = sender as Panel;
            if (panel != null)
            {
                //remove Loaded EventHandler
                panel.Loaded -= PressItem_ItemsControl_Loaded;

                //assign Background
                var container = ControlHelper.GetParent<ContentPresenter>(panel);                           
                if (container != null )
                {
                    var brush = Brushes.Transparent;
                    var altIndex = ItemsControl.GetAlternationIndex(container);                    
                    switch (altIndex)
                    {
                        case 1:     brush = new SolidColorBrush(Color.FromArgb(127, 173, 221, 230));        break;
                        case 2:     brush = Brushes.LightBlue;                                              break;
                        case 0:
                        default:    /*Default Color*/                                                       break;
                    }
                    panel.Background = brush;
                }                
            }
        }        
        #endregion

        #region Action Package Editor
        //double click to edit Action
        private void PackageItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var item = sender as ListBoxItem;
            if (item != null)
            {
                var dataEditor = PackageItem_GetDataEditor(item);
                if (dataEditor != null && dataEditor.EditAction.CanExecute(item.DataContext))
                {
                    dataEditor.EditAction.Execute(item.DataContext);
                }
            }
        }

        //KeyUp when Item is focus
        private void PackageItem_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            var item = sender as ListBoxItem;
            IDataEditor dataEditor = null;
            switch (e.Key)
            {
                case Key.Delete:
                    dataEditor = PackageItem_GetDataEditor(item);
                    if (dataEditor != null && dataEditor.RemoveAction.CanExecute(item.DataContext))
                    {
                        dataEditor.RemoveAction.Execute(item.DataContext);
                    }
                    break;

                default:
                    //ignore
                    break;
            }
        }

        //Update Current selected Channel View => Support multi ListBox
        private void PackageAction_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.packageListBox = sender as ListBox;
        }

        private IDataEditor PackageItem_GetDataEditor(FrameworkElement item)
        {
            IDataEditor dataEditor = null;
            if (item != null)
            {
                var listBox = ControlHelper.GetParent<ListBox>(item);
                if (listBox != null)
                {
                    dataEditor = listBox.DataContext as IDataEditor;
                }
            }
            return dataEditor;
        }
        #endregion

        #region OutPinHandler 

        private void OutPinFunctionSettingControl_Initialized(object sender, EventArgs e)
        {
            this.outPinFunctionSettingControl = sender as ContentControl;
        }

        private void OutPinFunctionComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var comboBox = sender as ComboBox;
            if (comboBox != null && this.outPinFunctionSettingControl != null && comboBox.SelectedItem is OutPinCommandOption)
            {
                DataTemplate template = null;
                var option = comboBox.SelectedItem as OutPinCommandOption;
                switch (option.Type)
                {
                    case OutPinCommands.Reset:
                    case OutPinCommands.Set:
                    case OutPinCommands.Toggle:                     template = this.Resources["appOutPinNoParaTemplate"] as DataTemplate;       break;
                    case OutPinCommands.PosTrigger:                       
                    case OutPinCommands.NegTrigger:
                    case OutPinCommands.Rect:                       template = this.Resources["appOutPinDurationTemplate"] as DataTemplate;     break;
                    case OutPinCommands.PWM:                        template = this.Resources["appOutPinPwmTemplate"] as DataTemplate;          break;
                    case OutPinCommands.PWM_Servo:                  template = this.Resources["appOutPinServoTemplate"] as DataTemplate;        break;                            
                    case OutPinCommands.PWM_Auto:       
                    case OutPinCommands.PWM_Servo_Auto:             template = this.Resources["appOutPinInputAutoTemplate"] as DataTemplate;    break;
                    case OutPinCommands.PWM_Servo_SV1270TG_Auto:    template = this.Resources["appOutPinInputAutoSV1270TGTemplate"] as DataTemplate;    break;
                    case OutPinCommands.PWM_Servo_SV1270TG:         template = this.Resources["appOutPinRealServoTemplate"] as DataTemplate;    break;
                    case OutPinCommands.Non:                    
                    default:                                        template = null;                                                            break;
                }

                this.outPinFunctionSettingControl.ContentTemplate = template;
                this.outPinFunctionSettingControl.Visibility = (template == null) ? Visibility.Collapsed : Visibility.Visible;
            }
        }

        private void SendAngleServorCombobox_SelectionCHanged(object sender, SelectionChangedEventArgs e)
        {
            var comboBox = sender as ComboBox;
            if (comboBox != null && _manualAngleServoTxt != null)
            {
                _manualAngleServoTxt.IsEnabled = (comboBox.SelectedIndex == 1);
            }
        }

        private void ManualAngleServo_KeyDown(object sender, KeyEventArgs e)
        {
            if (App.ViewModel != null && e.Key == Key.Enter)
            {
                App.ViewModel.OutPin_SendAngleServor.TryToExecute();
            }
        }

        #endregion

        #endregion


    }
}
